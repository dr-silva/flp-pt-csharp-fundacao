﻿/*******************************************************************************
 * 
 * Arquivo  : insercao.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Por Inserção da Fundação.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoPorInsercao<T> : OrdenacaoAbstrata<T>
  {
    #region Construtores
    public OrdenacaoPorInsercao() : base()
    {
    }

    public OrdenacaoPorInsercao(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoPorInsercao(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected abstract bool eOrdenavel(int i, int j);
    #endregion

    #region Ordenação
    protected sealed override void ordenar()
    {
      for (int j = 1; j < total; j++)
      {
        int i = j - 1;
        while ((i >= 0) && eOrdenavel(i, i + 1))
        {
          trocar(i, i + 1);
          i--;
        }
      }
    }
    #endregion
  }

  public class OrdenacaoPorInsercaoAscendente<T> : OrdenacaoPorInsercao<T>
  {
    #region Construtores
    public OrdenacaoPorInsercaoAscendente() : base()
    {
    }

    public OrdenacaoPorInsercaoAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoPorInsercaoAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) > 0);
    }
    #endregion
  }

  public class OrdenacaoPorInsercaoDescendente<T> : OrdenacaoPorInsercao<T>
  {
    #region Construtores
    public OrdenacaoPorInsercaoDescendente() : base()
    {
    }

    public OrdenacaoPorInsercaoDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoPorInsercaoDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) < 0);
    }
    #endregion
  }
}