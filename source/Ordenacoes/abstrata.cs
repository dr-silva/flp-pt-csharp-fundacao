﻿/*******************************************************************************
 * 
 * Arquivo  : abstrata.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores abstratos das Ordenações da Fundação.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoAbstrata<T> : IOrdenacao<T>, IDisposable
  {
    #region Atributos
    private T[] fItens;
    #endregion

    #region Construtores
    public OrdenacaoAbstrata() : this
    (
      FabricaComparador.criar<T>()
    )
    {
    }

    public OrdenacaoAbstrata(FuncaoComparacao<T> comparacao) : this
    (
      FabricaComparador.criar<T>(comparacao)
    )
    {
    }

    public OrdenacaoAbstrata(IComparador<T> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Dispose
    public virtual void Dispose()
    {
      fItens     = null;
      comparador = null;
    }
    #endregion

    #region Assistentes
    protected void trocar(int i, int j)
    {
      T temp    = fItens[i];
      fItens[i] = fItens[j];
      fItens[j] = temp;
    }

    protected abstract void ordenar();
    #endregion

    #region Ordenação
    public void ordenar(T[] itens)
    {
      if (itens == null || itens.Length == 0)
        return;

      fItens = itens;
      total  = itens.Length;

      ordenar();
    }
    #endregion

    #region Propriedades
    public int total
    {
      get;
      set;
    }

    public IComparador<T> comparador
    {
      get;
      private set;
    }

    public T this[int indice]
    {
      get
      {
        return fItens[indice];
      }
    }
    #endregion
  }
}