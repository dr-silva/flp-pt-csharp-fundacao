﻿/*******************************************************************************
 * 
 * Arquivo  : merge.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Merge da Fundação.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoMerge<T> : IOrdenacao<T>, IDisposable
  {
    #region Atributos
    private T[] fItens,
                fEsquerda,
                fDireita;
    #endregion

    #region Construtores
    public OrdenacaoMerge() : this
    (
      FabricaComparador.criar<T>()
    )
    {
    }

    public OrdenacaoMerge(FuncaoComparacao<T> comparacao) : this
    (
      FabricaComparador.criar<T>(comparacao)
    )
    {
    }

    public OrdenacaoMerge(IComparador<T> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Dispose
    public void Dispose()
    {
      fItens     = null;
      fEsquerda  = null;
      fDireita   = null;
      comparador = null;
    }
    #endregion

    #region Assistentes
    private int criarEsquerda(int minimo, int maximo)
    {
      int total = maximo - minimo + 1;
      fEsquerda = new T[total];

      for (int i = 0; i < total; i++)
        fEsquerda[i] = fItens[minimo + i];

      return total;
    }

    private int criarDireita(int minimo, int maximo)
    {
      int total = maximo - minimo;
      fDireita  = new T[total];

      for (int i = 0; i < total; i++)
        fDireita[i] = fItens[minimo + i + 1];

      return total;
    }

    private int definirEsquerda(int item, int esquerda)
    {
      fItens[item] = fEsquerda[esquerda];
      
      return esquerda + 1;
    }

    private int definirDireita(int item, int direita)
    {
      fItens[item] = fDireita[direita];
      
      return direita + 1;
    }

    private void fundir(int minimo, int mediana, int maximo)
    {
      int totalEsquerda = criarEsquerda(minimo, mediana),
          totalDireita  = criarDireita (mediana, maximo);

      int e = 0,
          d = 0;

      for (int i = minimo; i <= maximo; i++)
        if (e >= totalEsquerda)
          d = definirDireita(i, d);
        else if (d >= totalDireita)
          e = definirEsquerda(i, e);
        else if (eOrdenavel(fEsquerda[e], fDireita[d]))
          e = definirEsquerda(i, e);
        else
          d = definirDireita(i, d);
    }

    private void ordenar(int minimo, int maximo)
    {
      if (minimo < maximo)
      {
        int mediano = (minimo + maximo) / 2;

        ordenar(minimo, mediano);
        ordenar(mediano + 1, maximo);

        fundir(minimo, mediano, maximo);
      }
    }

    protected abstract bool eOrdenavel(T esquerda, T direita);
    #endregion

    #region Ordenação
    public void ordenar(T[] itens)
    {
      if (itens == null || itens.Length == 0)
        return;

      fItens = itens;

      ordenar(0, itens.Length - 1);

      fEsquerda = null;
      fDireita  = null;
    }
    #endregion

    #region Propriedades
    protected IComparador<T> comparador
    {
      get;
      private set;
    }
    #endregion
  }

  public class OrdenacaoMergeAscendente<T> : OrdenacaoMerge<T>
  {
    #region Construtores
    public OrdenacaoMergeAscendente() : base()
    {
    }

    public OrdenacaoMergeAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoMergeAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(T esquerda, T direita)
    {
      return (comparador.Compare(esquerda, direita) <= 0);
    }
    #endregion
  }

  public class OrdenacaoMergeDescendente<T> : OrdenacaoMerge<T>
  {
    #region Construtores
    public OrdenacaoMergeDescendente() : base()
    {
    }

    public OrdenacaoMergeDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoMergeDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(T esquerda, T direita)
    {
      return (comparador.Compare(esquerda, direita) >= 0);
    }
    #endregion
  }
}