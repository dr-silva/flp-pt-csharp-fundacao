﻿/*******************************************************************************
 * 
 * Arquivo  : selecao.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Por Seleção da Fundação.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoPorSelecao<T> : OrdenacaoAbstrata<T>
  {
    #region Construtores
    public OrdenacaoPorSelecao() : base()
    {
    }

    public OrdenacaoPorSelecao(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoPorSelecao(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected abstract bool eOrdenavel(int i, int j);
    #endregion

    #region Ordenação
    protected sealed override void ordenar()
    {
      for (int i = 0; i < total; i++)
      {
        int minimo = i;

        for (int j = i + 1; j < total; j++)
          if (eOrdenavel(minimo, j))
            minimo = j;

        trocar(i, minimo);
      }
    }
    #endregion
  }

  public class OrdenacaoPorSelecaoAscendente<T> : OrdenacaoPorSelecao<T>
  {
    #region Construtores
    public OrdenacaoPorSelecaoAscendente() : base()
    {
    }

    public OrdenacaoPorSelecaoAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoPorSelecaoAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) > 0);
    }
    #endregion
  }

  public class OrdenacaoPorSelecaoDescendente<T> : OrdenacaoPorSelecao<T>
  {
    #region Construtores
    public OrdenacaoPorSelecaoDescendente() : base()
    {
    }

    public OrdenacaoPorSelecaoDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoPorSelecaoDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) < 0);
    }
    #endregion
  }
}