﻿/*******************************************************************************
 * 
 * Arquivo  : contagem.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Por Contagem da Fundação.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoPorContagem<T> : IOrdenacao<T>, IDisposable
  {
    #region Atributos
    private T[] fItens;
    #endregion

    #region Contrutores
    public OrdenacaoPorContagem(FuncaoProjecao<T, int> projecao)
    {
      if (projecao == null)
        throw Excecoes.ArgumentoNulo(nameof(projecao));

      projetor = FabricaProjetor.criar(projecao);
      minimo   = int.MaxValue;
      maximo   = int.MinValue;
    }

    public OrdenacaoPorContagem(IProjetor<T, int> projetor)
    {
      this.projetor = projetor ?? throw Excecoes.ArgumentoNulo(nameof(projetor));
      this.minimo   = int.MaxValue;
      this.maximo   = int.MinValue;
    }
    #endregion

    #region Dispose
    public void Dispose()
    {
      fItens   = null;
      projetor = null;
    }
    #endregion

    #region Assistentes
    private void definirMinMax()
    {
      minimo = int.MaxValue;
      maximo = int.MinValue;

      for (int i = 0; i < total; i++)
      {
        int valor = projetor.projetar(fItens[i]);

        if (valor < minimo)
          minimo = valor;

        if (valor > maximo)
          maximo = valor;
      }
    }

    private bool preparar()
    {
      if (minimo > maximo)
        definirMinMax();

      return (maximo - minimo) > 1;
    }
    
    private int indexar(int indice)
    {
      return projetor.projetar(fItens[indice]) - minimo;
    }

    protected abstract int indexarSaida(int indice);
    #endregion

    #region Ordenação
    public void ordenar(T[] itens)
    {
      if (itens == null || itens.Length == 0)
        return;

      fItens = itens;
      total  = itens.Length;

      if (!preparar())
        return;

      int diferenca = maximo - minimo;

      T  [] saida    = new T  [total        ];
      int[] contagem = new int[diferenca + 1];

      for (int i = 0; i < total; i++)
        contagem[indexar(i)]++;

      contagem[0]--;
      for (int i = 1; i <= diferenca; i++)
        contagem[i] += contagem[i - 1];

      for (int i = total - 1; i >= 0; i--)
        saida[ contagem[indexar(i)]-- ] = fItens[i];

      for (int i = 0; i < total; i++)
        fItens[i] = saida[ indexarSaida(i) ];

      saida    = null;
      contagem = null;
      minimo   = int.MaxValue;
      maximo   = int.MinValue;
    }
    #endregion

    #region Propriedades
    private IProjetor<T, int> projetor
    {
      get;
      set;
    }

    protected int total
    {
      get;
      private set;
    }

    public int minimo
    {
      get;
      set;
    }

    public int maximo
    {
      get;
      set;
    }
    #endregion
  }

  public class OrdenacaoPorContagemAscendente<T> : OrdenacaoPorContagem<T>
  {
    #region Contrutores
    public OrdenacaoPorContagemAscendente(FuncaoProjecao<T, int> projecao) : base(projecao)
    {
    }

    public OrdenacaoPorContagemAscendente(IProjetor<T, int> projetor) : base(projetor)
    {
    }
    #endregion

    #region Contagem
    protected override int indexarSaida(int indice)
    {
      return indice;
    }
    #endregion
  }

  public class OrdenacaoPorContagemDescendente<T> : OrdenacaoPorContagem<T>
  {
    #region Contrutores
    public OrdenacaoPorContagemDescendente(FuncaoProjecao<T, int> projecao) : base(projecao)
    {
    }

    public OrdenacaoPorContagemDescendente(IProjetor<T, int> projetor) : base(projetor)
    {
    }
    #endregion

    #region Contagem
    protected override int indexarSaida(int indice)
    {
      return total - (indice + 1);
    }
    #endregion
  }
}