﻿/*******************************************************************************
 * 
 * Arquivo  : validacao.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Validação de Ordenações da Fundação.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class ValidadorOrdenacao<T> : IValidadorOrdenacao<T>, IDisposable
  {
    #region Construtores
    public ValidadorOrdenacao()
    {
      comparador = FabricaComparador.criar<T>();
    }

    public ValidadorOrdenacao(FuncaoComparacao<T> comparacao)
    {
      if (comparacao == null)
        throw Excecoes.ArgumentoNulo(nameof(comparacao));

      comparador = FabricaComparador.criar<T>(comparacao);
    }

    public ValidadorOrdenacao(IComparador<T> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }

    public void Dispose()
    {
      comparador = null;
    }
    #endregion

    #region Assistentes
    protected abstract bool estaOrdenado(T esquerda, T direita);
    #endregion

    #region Validação
    public bool validar(T[] itens)
    {
      if (itens == null || itens.Length == 0)
        return true;

      int total = itens.Length,
          i     = 1;

      while ((i < total) && estaOrdenado(itens[i - 1], itens[i]))
        i++;

      return (i == total);
    }
    #endregion

    #region Propriedades
    protected IComparador<T> comparador
    {
      get;
      private set;
    }
    #endregion
  }

  public class ValidadorOrdenacaoAscendente<T> : ValidadorOrdenacao<T>
  {
    #region Construtores
    public ValidadorOrdenacaoAscendente() : base()
    {
    }

    public ValidadorOrdenacaoAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public ValidadorOrdenacaoAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool estaOrdenado(T esquerda, T direita)
    {
      return comparador.Compare(esquerda, direita) <= 0;
    }
    #endregion
  }

  public class ValidadorOrdenacaoDescendente<T> : ValidadorOrdenacao<T>
  {
    #region Construtores
    public ValidadorOrdenacaoDescendente() : base()
    {
    }

    public ValidadorOrdenacaoDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public ValidadorOrdenacaoDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool estaOrdenado(T esquerda, T direita)
    {
      return comparador.Compare(esquerda, direita) >= 0;
    }
    #endregion
  }
}