﻿/*******************************************************************************
 * 
 * Arquivo  : shell.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Shell da Fundação.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoShell<T> : OrdenacaoAbstrata<T>
  {
    #region Construtores
    public OrdenacaoShell() : base()
    {
    }

    public OrdenacaoShell(FuncaoComparacao<T> comparacao) : base()
    {
    }

    public OrdenacaoShell(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected abstract bool eOrdenavel(int i, int j);
    #endregion

    #region Ordenação
    protected sealed override void ordenar()
    {
      int lacuna = 1;
      while (lacuna < total / 3)
        lacuna = 3 * lacuna + 1;

      while (lacuna >= 1)
      {
        for (int i = lacuna; i < total; i++)
        {
          int j = i;

          while ((j >= lacuna) && eOrdenavel(j - lacuna, j))
          {
            trocar(j - lacuna, j);
            j -= lacuna;
          }
        }

        lacuna /= 3;
      }
    }
    #endregion
  }

  public class OrdenacaoShellAscendente<T> : OrdenacaoShell<T>
  {
    #region Construtores
    public OrdenacaoShellAscendente() : base()
    {
    }

    public OrdenacaoShellAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoShellAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) > 0);
    }
    #endregion
  }

  public class OrdenacaoShellDescendente<T> : OrdenacaoShell<T>
  {
    #region Construtores
    public OrdenacaoShellDescendente() : base()
    {
    }

    public OrdenacaoShellDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoShellDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) < 0);
    }
    #endregion
  }
}