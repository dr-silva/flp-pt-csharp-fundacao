﻿/*******************************************************************************
 * 
 * Arquivo  : quick.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Quick da Fundação.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Matematica;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoQuick<T> : OrdenacaoAbstrata<T>
  {
    #region Construtores
    public OrdenacaoQuick() : base()
    {
    }

    public OrdenacaoQuick(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoQuick(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    private int particionar(int minimo, int maximo)
    {
      int i = minimo + TMatematica.uniforme(maximo - minimo);
      trocar(i, maximo);

      i = minimo;
      for (int j = minimo; j < maximo; j++)
        if (eOrdenavel(j, maximo))
          trocar(i++, j);

      trocar(i, maximo);

      return i;
    }

    private void ordenar(int minimo, int maximo)
    {
      if (minimo < maximo)
      {
        int ponto = particionar(minimo, maximo);

        ordenar(minimo, ponto - 1);
        ordenar(ponto + 1, maximo);
      }
    }

    protected abstract bool eOrdenavel(int i, int j);
    #endregion

    #region Ordenação
    protected sealed override void ordenar()
    {
      ordenar(0, total - 1);
    }
    #endregion
  }

  public class OrdenacaoQuickAscendente<T> : OrdenacaoQuick<T>
  {
    #region Construtores
    public OrdenacaoQuickAscendente() : base()
    {
    }

    public OrdenacaoQuickAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoQuickAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) <= 0);
    }
    #endregion
  }

  public class OrdenacaoQuickDescendente<T> : OrdenacaoQuick<T>
  {
    #region Construtores
    public OrdenacaoQuickDescendente() : base()
    {
    }

    public OrdenacaoQuickDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoQuickDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) >= 0);
    }
    #endregion
  }
}