﻿/*******************************************************************************
 * 
 * Arquivo  : heap.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Heap da Fundação.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoHeap<T> : OrdenacaoAbstrata<T>
  {
    #region Construtores
    public OrdenacaoHeap() : base()
    {
    }

    public OrdenacaoHeap(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoHeap(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    private void amontoar(int i)
    {
      int prioritario,
          esquerda = 2 * (i + 1) - 1,
          direita  = 2 * (i + 1);

      prioritario = ((esquerda < total) && eOrdenavel(esquerda, i)) ? esquerda : i;

      if ((direita < total) && eOrdenavel(direita, prioritario))
        prioritario = direita;

      if (prioritario != i)
      {
        trocar(i, prioritario);
        amontoar(prioritario);
      }
    }

    protected abstract bool eOrdenavel(int i, int j);
    #endregion

    #region Ordenação
    protected sealed override void ordenar()
    {
      // criar heap
      for (int i = total / 2; i >= 0; i--)
        amontoar(i);

      // ordenar array
      for (int i = total - 1; i > 0; i--)
      {
        trocar(0, i);
        total--;
        amontoar(0);
      }
    }
    #endregion
  }

  public class OrdenacaoHeapAscendente<T> : OrdenacaoHeap<T>
  {
    #region Construtores
    public OrdenacaoHeapAscendente() : base()
    {
    }

    public OrdenacaoHeapAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoHeapAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) > 0);
    }
    #endregion
  }

  public class OrdenacaoHeapDescendente<T> : OrdenacaoHeap<T>
  {
    #region Construtores
    public OrdenacaoHeapDescendente() : base()
    {
    }

    public OrdenacaoHeapDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoHeapDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override bool eOrdenavel(int i, int j)
    {
      return (comparador.Compare(this[i], this[j]) < 0);
    }
    #endregion
  }
}