﻿/*******************************************************************************
 * 
 * Arquivo  : quick3way.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Quick 3-Way da Fundação.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoQuick3Way<T> : OrdenacaoAbstrata<T>
  {
    #region Construtores
    public OrdenacaoQuick3Way() : base()
    {
    }

    public OrdenacaoQuick3Way(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public OrdenacaoQuick3Way(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    private void ordenar(int minimo, int maximo)
    {
      if (minimo >= maximo)
        return;

      int menor = minimo,
          maior = maximo,
          i     = minimo + 1;
      T   valor = this[minimo];

      while (i <= maior)
      {
        int comparacao = comparar(this[i], valor);

        if (comparacao < 0)
          trocar(menor++, i++);
        else if (comparacao > 0)
          trocar(i, maior--);
        else
          i++;
      }

      ordenar(minimo, menor - 1);
      ordenar(maior + 1, maximo);
    }

    protected abstract int comparar(T esquerda, T direita);
    #endregion

    #region Ordenação
    protected sealed override void ordenar()
    {
      ordenar(0, total - 1);
    }
    #endregion
  }

  public class OrdenacaoQuick3WayAscendente<T> : OrdenacaoQuick3Way<T>
  {
    #region Construtores
    public OrdenacaoQuick3WayAscendente() : base()
    {
    }

    public OrdenacaoQuick3WayAscendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoQuick3WayAscendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override int comparar(T esquerda, T direita)
    {
      return comparador.Compare(esquerda, direita);
    }
    #endregion
  }

  public class OrdenacaoQuick3WayDescendente<T> : OrdenacaoQuick3Way<T>
  {
    #region Construtores
    public OrdenacaoQuick3WayDescendente() : base()
    {
    }

    public OrdenacaoQuick3WayDescendente(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }
    public OrdenacaoQuick3WayDescendente(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Assistentes
    protected override int comparar(T esquerda, T direita)
    {
      return -comparador.Compare(esquerda, direita);
    }
    #endregion
  }
}