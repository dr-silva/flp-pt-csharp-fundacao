﻿/*******************************************************************************
 * 
 * Arquivo  : balde.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Guarda os classificadores da Ordenação Por Balde da Fundação.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Matematica;

namespace FractalLotus.Fundacao.Ordenacoes
{
  public abstract class OrdenacaoPorBalde<T> : IOrdenacao<T>, IDisposable
  {
    #region Classe Interna
    private class Item
    {
      public T      valor    { get; private set; }
      public double projecao { get; private set; }

      public Item anterior { get; set; }
      public Item ulterior { get; set; }

      public Item(T valor, double projecao, Item lista)
      {
        this.valor    = valor;
        this.projecao = projecao;
        this.anterior = null;
        this.ulterior = lista;

        if (lista != null)
          lista.anterior = this;
      }

      public Item inverterUlterior()
      {
        return (ulterior != null) ? trocar(this, ulterior) : null;
      }

      public Item inverterAnterior()
      {
        return (anterior != null) ? trocar(anterior, this) : null;
      }

      public static Item trocar(Item esquerda, Item direita)
      {
        Item extremaEsquerda = esquerda.anterior,
             extremaDireita  = direita .ulterior;

        if (extremaEsquerda != null)
          extremaEsquerda.ulterior = direita;
        if (extremaDireita != null)
          extremaDireita.anterior = esquerda;

        direita .ulterior = esquerda;
        esquerda.ulterior = extremaDireita;
        esquerda.anterior = direita;
        direita .anterior = extremaEsquerda;

        return direita;
      }
    }
    #endregion

    #region Atributos
    private T   [] fItens;
    private Item[] fBaldes;
    #endregion

    #region Construtores
    public OrdenacaoPorBalde(FuncaoProjecao<T, double> projecao)
    {
      if (projecao == null)
        throw Excecoes.ArgumentoNulo(nameof(projecao));

      projetor = FabricaProjetor.criar(projecao);
    }

    public OrdenacaoPorBalde(IProjetor<T, double> projetor)
    {
      this.projetor = projetor ?? throw Excecoes.ArgumentoNulo(nameof(projetor));
    }
    #endregion

    #region Dispose
    public void Dispose()
    {
      fItens   = null;
      fBaldes  = null;
      projetor = null;
    }
    #endregion

    #region Assistentes
    private void criarItem(T valor)
    {
      double projecao = projetor.projetar(valor);
      int    indice   = (int)Math.Floor(total * projecao);

      fBaldes[indice] = new Item(valor, projecao, fBaldes[indice]);
    }

    private void ordenar(int balde)
    {
      Item primeiro = fBaldes[balde];

      if (primeiro == null)
          return;

      Item fixo = primeiro.ulterior;
      while (fixo != null)
      {
        Item movel = fixo.anterior;

        while ((movel != null) && (movel.projecao > movel.ulterior.projecao))
          movel = movel.inverterUlterior().anterior;

        fixo = fixo.ulterior;
      }

      while (primeiro.anterior != null)
        primeiro = primeiro.anterior;

      fBaldes[balde] = primeiro;
    }

    private int transferir(int indice, int balde)
    {
      for (Item item = fBaldes[balde]; item != null; item = item.ulterior)
        fItens[ indexar(indice++) ] = item.valor;

      return indice;
    }

    protected abstract int indexar(int indice);
    #endregion

    #region Ordenação
    public void ordenar(T[] itens)
    {
      if (itens == null || itens.Length == 0)
        return;

      fItens  = itens;
      total   = itens.Length;
      fBaldes = new Item[total];

      for (int i = 0; i < total; i++)
        criarItem(fItens[i]);

      for (int i = 0; i < total; i++)
        ordenar(i);

      int indice = 0;
      for (int i = 0; i < total; i++)
        indice = transferir(indice, i);
    }
    #endregion

    #region Propriedades
    protected int total
    {
      get;
      private set;
    }

    private IProjetor<T, double> projetor
    {
      get;
      set;
    }
    #endregion
  }

  public class OrdenacaoPorBaldeAscendente<T> : OrdenacaoPorBalde<T>
  {
    #region Construtores
    public OrdenacaoPorBaldeAscendente(FuncaoProjecao<T, double> projecao) : base(projecao)
    {
    }

    public OrdenacaoPorBaldeAscendente(IProjetor<T, double> projetor) : base(projetor)
    {
    }
    #endregion

    #region Balde
    protected override int indexar(int indice)
    {
      return indice;
    }
    #endregion
  }

  public class OrdenacaoPorBaldeDescendente<T> : OrdenacaoPorBalde<T>
  {
    #region Construtores
    public OrdenacaoPorBaldeDescendente(FuncaoProjecao<T, double> projecao) : base(projecao)
    {
    }

    public OrdenacaoPorBaldeDescendente(IProjetor<T, double> projetor) : base(projetor)
    {
    }
    #endregion

    #region Balde
    protected override int indexar(int indice)
    {
      return total - (indice + 1);
    }
    #endregion
  }

  public class OrdenacaoAleatoria<T> : OrdenacaoPorBaldeAscendente<T>
  {
    #region Construtores
    public OrdenacaoAleatoria() : base(new ProjetorAleatorio())
    {
    }
    #endregion

    #region Assistentes
    private class ProjetorAleatorio : IProjetor<T, double>
    {
      private readonly IGeradorCongruencial gerador;

      public ProjetorAleatorio()
      {
        gerador = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);
      }

      public double projetar(T valor)
      {
        return gerador.uniforme();
      }
    }
    #endregion
  }
}