﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Extensores
{
  class ExtensorPadrao
  {
    public static T[] toArray<T>(IEnumeravel<T> colecao)
    {
      if (colecao == null)
        return null;

      T[] result = new T[colecao.total];
      int indice = 0;

      foreach (var item in colecao)
        result[indice++] = item;

      return result;
    }

    public static string toString<T>(IEnumerable<T> colecao)
    {
      return toString
      (
        colecao, colecao != null ? FabricaProjetor.criarString<T>() : null
      );
    }

    public static string toString<T>(IEnumerable<T> colecao, IProjetor<T, string> projetor)
    {
      if (colecao == null)
        return string.Empty;

      StringBuilder sb = new StringBuilder();

      sb.Append("[");

      foreach (var item in colecao)
      {
        sb.Append(projetor.projetar(item));
        sb.Append(", ");
      }

      if (sb.Length > 1)
        sb.Remove(sb.Length - 2, 2);

      sb.Append("]");

      return sb.ToString();
    }
  }
}
