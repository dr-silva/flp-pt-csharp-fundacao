﻿/*******************************************************************************
 * 
 * Arquivo  : bolsas.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-04-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as classes de coleções do bolsa (guardáveis).
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes.Abstracoes;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes
{
  #region Bolsas Ordinárias
  public class BolsaVetorial<T> : Vetorizacao<T>, IGuardavel<T>
  {
    #region Atributos
    private int fTotal;
    #endregion

    #region Propriedades
    public override int total
    {
      get { return fTotal; }
    }
    public override bool vazio
    {
      get { return fTotal == 0; }
    }
    public override bool cheio
    {
      get { return fTotal == capacidade; }
    }
    public override bool somenteLeitura
    {
      get { return false; }
    }
    public T primeiro
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return this[0];
      }
    }
    public T ultimo
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return this[fTotal - 1];
      }
    }
    #endregion

    #region Construtores
    public BolsaVetorial(int capacidade) : base(capacidade)
    {
      fTotal = 0;
    }
    #endregion

    #region Enumerável
    public override IEnumerator<T> GetEnumerator()
    {
      for (int i = 0; i < fTotal; i++)
        yield return this[i];
    }
    #endregion

    #region Guardável
    public void guardar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      this[fTotal++] = item;
    }
    public void guardar(IEnumerable<T> itens)
    {
      foreach (T item in itens)
        guardar(item);
    }
    #endregion
  }

  public class BolsaEncadeada<T> : Encadeamento<T>, IGuardavel<T>
  {
    #region Atributos
    private No fPrimeiro,
               fUltimo;
    #endregion

    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    public T primeiro
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return fPrimeiro.valor;
      }
    }
    public T ultimo
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return fUltimo.valor;
      }
    }
    #endregion

    #region Enumerável
    public override IEnumerator<T> GetEnumerator()
    {
      return obterEnumerador(fPrimeiro);
    }
    #endregion

    #region Guardável
    public void guardar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      No no = new No(item);

      if (!vazio)
        fUltimo.proximo = no;

      fUltimo = no;

      if (vazio)
        fPrimeiro = no;

      total++;
    }
    public void guardar(IEnumerable<T> itens)
    {
      foreach (T item in itens)
        guardar(item);
    }
    #endregion
  }
  #endregion

  #region Bolsas Ordenadas
  public class BolsaOrdenadaVetorial<T> : VetorizacaoOrdenada<T, bool?>, IGuardavel<T>
  {
    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    public T primeiro
    {
      get { return minimo; }
    }
    public T ultimo
    {
      get { return maximo; }
    }
    #endregion

    #region Construtores
    public BolsaOrdenadaVetorial(int capacidade) : base(capacidade)
    {
    }

    public BolsaOrdenadaVetorial(int capacidade, FuncaoComparacao<T> comparacao) : base(capacidade, comparacao)
    {
    }

    public BolsaOrdenadaVetorial(int capacidade, IComparador<T> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<T> GetEnumerator()
    {
      return obterEnumeradorChaves(0, total);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Guardável
    public void guardar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();
      else if (vazio)
        inserirValor(0, item, null);
      else
      {
        int i = indexar(item);

        inserirValor(comparar(item, i) < 0 ? i : i + 1, item, null);
      }
    }
    public void guardar(IEnumerable<T> itens)
    {
      foreach (T item in itens)
        guardar(item);
    }
    #endregion
  }

  public class BolsaOrdenadaEncadeada<T> : ArvoreRubronegraOrdenada<T, bool?>, IGuardavel<T>
  {
    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }
    public T primeiro
    {
      get { return minimo; }
    }
    public T ultimo
    {
      get { return maximo; }
    }
    #endregion

    #region Construtores
    public BolsaOrdenadaEncadeada() : base()
    {
    }

    public BolsaOrdenadaEncadeada(FuncaoComparacao<T> comparacao) : base()
    {
    }

    public BolsaOrdenadaEncadeada(IComparador<T> comparador) : base()
    {
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<T> GetEnumerator()
    {
      return obterEnumeradorChaves(buscarNoMinimo(raiz), buscarNoMaximo(raiz));
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Guardável
    public void guardar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      inserirNo( new No(item, null) );
    }
    public void guardar(IEnumerable<T> itens)
    {
      foreach (T item in itens)
        guardar(item);
    }
    #endregion
  }
  #endregion
}