﻿/*******************************************************************************
 * 
 * Arquivo  : listas.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-04-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as classes de coleções indexadas.
 * 
 *******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes.Abstracoes;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes
{
  public class ListaVetorial<T> : VetorizacaoOrdenada<int, T>, IListavel<T>
  {
    #region Atributos
    private int proximo;
    #endregion

    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }

    public T primeiro
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return base[0].Value;;
      }
    }
    public T ultimo
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return base[total - 1].Value;
      }
    }
    public new T this[int indice]
    {
      get
      {
        if ((indice < 0) || (indice >= capacidade))
          throw Excecoes.ChaveDicionario();

        int real = indexar(indice);

        if (comparar(indice, real) != 0)
          throw Excecoes.ChaveDicionario();

        return base[real].Value;
      }
      set
      {
        inserir(indice, value);
      }
    }
    #endregion

    #region Construtores
    public ListaVetorial(int capacidade) : base(capacidade)
    {
      proximo = 0;
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<T> GetEnumerator()
    {
      return obterEnumeradorValores(0, total);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Listável
    public int adicionar(T item)
    {
      int result;

      if (cheio)
        result = -1;
      else
      {
        while (contemChave(proximo))
          if (++proximo < 0)
            proximo = 0;

        result = proximo++;
        inserir(result, item);
      }

      return result;
    }

    public int adicionar(IEnumerable<T> itens)
    {
      int result = -1;

      if (!cheio)
        foreach (var item in itens)
        {
          int i = adicionar(item);

          if (i < 0)
            break;
          if (result < 0)
            result = i;
        }

      return result;
    }

    public void inserir(int indice, T item)
    {
      if ((indice < 0) || (indice >= capacidade))
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);
      else if (vazio)
        inserirValor(0, indice, item);
      else
      {
        int indiceReal = indexar (indice),
            comparacao = comparar(indice, indiceReal);

        if (comparacao < 0)
          inserirValor(indiceReal, indice, item);
        else if (comparacao > 0)
          inserirValor(indiceReal + 1, indice, item);
        else
          base[indiceReal] = new KeyValuePair<int, T>(indice, item);
      }
    }

    public void inserir(int indice, IEnumerable<T> itens)
    {
      foreach (var item in itens)
      {
        if (indice < 0)
          break;

        inserir(indice++, item);
      }
    }

    public bool remover(int indice)
    {
      if ((indice < 0) || (indice >= capacidade))
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

      int indiceReal = indexar (indice),
          comparacao = comparar(indice, indiceReal);

      if (comparacao == 0)
        excluirValor(indiceReal);

      return (comparacao == 0);
    }

    public bool contem (int indice)
    {
      if ((indice < 0) || (indice >= capacidade))
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

      return contemChave(indice);
    }

    public void limpar()
    {
      excluirTodos();
    }

    public IEnumeravel<T> obterItens(int inferior, int superior)
    {
      int idxInferior = teto(inferior),
          idxSuperior = piso(superior);

      return new ColecaoValores(this, idxInferior, idxSuperior);
    }
    #endregion
  }

  public class ListaEncadeada<T> : ArvoreRubronegra<int, T>, IListavel<T>
  {
    #region Atributos
    private int proximo;
    #endregion

    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }

    public T primeiro
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return buscarNoMinimo(raiz).valor;
      }
    }
    public T ultimo
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return buscarNoMaximo(raiz).valor;
      }
    }
    public T this[int indice]
    {
      get
      {
        No no = (indice < 0) ? null : buscarNo(indice);

        if (no == null)
          throw Excecoes.ChaveDicionario();

        return no.valor;
      }
      set
      {
        inserir(indice, value);
      }
    }
    #endregion

    #region Construtores
    public ListaEncadeada() : base()
    {
      proximo = 0;
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<T> GetEnumerator()
    {
      return obterEnumeradorValores(buscarNoMinimo(raiz), buscarNoMaximo(raiz));
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Listável
    public int adicionar(T item)
    {
      if (cheio)
        return -1;
      else
      {
        while (contem(proximo))
          if (++proximo < 0)
            proximo = 0;

        int result = proximo++;
        inserir(result, item);

        return result;
      }
    }

    public int adicionar(IEnumerable<T> itens)
    {
      int result = -1;

      if (!cheio)
        foreach (var item in itens)
        {
          int i = adicionar(item);

          if (i < 0)
            break;
          if (result < 0)
            result = i;
        }

      return result;
    }

    public void inserir(int indice, T item)
    {
      if (indice < 0)
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, int.MaxValue);

      No no = buscarNo(indice);

      if (no != null)
        no.valor = item;
      else
        inserirNo(new No(indice, item));
    }

    public void inserir(int indice, IEnumerable<T> itens)
    {
      foreach (var item in itens)
      {
        if (indice < 0)
          break;

        inserir(indice++, item);
      }
    }

    public bool remover(int indice)
    {
      if (indice < 0)
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, int.MaxValue);

      No no = (vazio) ? null : buscarNo(indice);

      if (no != null)
        excluirNo(no);

      return (no != null);
    }

    public bool contem (int indice)
    {
      if (indice < 0)
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, int.MaxValue);

      return (!vazio) && (buscarNo(indice) != null);
    }

    public void limpar()
    {
      raiz = null;
    }

    public IEnumeravel<T> obterItens(int inferior, int superior)
    {
      No noInferior = buscarNoTeto(inferior),
         noSuperior = buscarNoPiso(superior);

      return new ColecaoValores(this, noInferior, noSuperior);
    }
    #endregion
  }
}
