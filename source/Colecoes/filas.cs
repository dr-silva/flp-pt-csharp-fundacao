﻿/*******************************************************************************
 * 
 * Arquivo  : filas.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-04-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as classes de coleções do tipo fila.
 * 
 *******************************************************************************/
using System;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes.Abstracoes;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Colecoes
{
  #region Filas Básicas
  public class FilaVetorial<T> : Vetorizacao<T>, IEnfileiravel<T>
  {
    #region Atributos
    private int inicio,
                fim;
    #endregion

    #region Propriedades
    public override int total
    {
      get
      {
        return (fim >= inicio) ? fim - inicio : capacidade + fim - inicio;
      }
    }
    public override bool vazio
    {
      get
      {
        return inicio == fim;
      }
    }
    public override bool cheio
    {
      get
      {
        return (inicio == fim + 1) || ((fim == capacidade) && (inicio == 0));
      }
    }
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public FilaVetorial(int capacidade) : base(capacidade, capacidade + 1)
    {
      inicio = fim = 0;
    }
    #endregion

    #region Enumerável
    public override IEnumerator<T> GetEnumerator()
    {
      for (int i = total, j = inicio; i > 0; i--)
      {
        yield return this[j++];

        if (j >= capacidade)
          j = 0;
      }
    }
    #endregion

    #region Enfileirável
    public void enfileirar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      this[fim] = item;

      fim = (fim == capacidade) ? 0 : fim + 1;
    }

    public T desenfileirar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      T item = this[inicio];
      inicio = (inicio == capacidade) ? 0 : inicio + 1;

      return item;
    }

    public T espiar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return this[inicio];
    }

    public void limpar()
    {
      inicio = fim = 0;
    }
    #endregion
  }

  public class FilaEncadeada<T> : Encadeamento<T>, IEnfileiravel<T>
  {
    #region Atributos
    private No primeiro,
               ultimo;
    #endregion

    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Enumerável
    public override IEnumerator<T> GetEnumerator()
    {
      return obterEnumerador(primeiro);
    }
    #endregion

    #region Enfileirável
    public void enfileirar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      No no = new No(item);

      if (!vazio)
        ultimo.proximo = no;

      ultimo = no;

      if (vazio)
        primeiro = no;

      total++;
    }

    public T desenfileirar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      No no      = primeiro;
      primeiro   = no.proximo;
      no.proximo = null;
      total--;

      if (primeiro == null)
        ultimo = null;

      return no.valor;
    }

    public T espiar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return primeiro.valor;
    }

    public void limpar()
    {
      total    = 0;
      primeiro = ultimo = null;
    }
    #endregion
  }
  #endregion

  #region Filas Vetoriais de Prioridade
  public abstract class FilaPrioridadeVetorial<T> : HeapVetorial<T>, IEnfileiravel<T>
  {
    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public FilaPrioridadeVetorial(int capacidade) : base(capacidade)
    {
    }

    public FilaPrioridadeVetorial(int capacidade, FuncaoComparacao<T> comparacao) : base(capacidade, comparacao)
    {
    }

    public FilaPrioridadeVetorial(int capacidade, IComparador<T> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Enfileirável
    public void enfileirar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      inserir(item);
    }

    public T desenfileirar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return excluir(0);
    }

    public T espiar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return this[0];
    }

    public void limpar()
    {
      excluirTodos();
    }
    #endregion
  }

  public class FilaPrioridadeMaximaVetorial<T> : FilaPrioridadeVetorial<T>
  {
    #region Construtores
    public FilaPrioridadeMaximaVetorial(int capacidade) : base(capacidade)
    {
    }

    public FilaPrioridadeMaximaVetorial(int capacidade, FuncaoComparacao<T> comparacao) : base(capacidade, comparacao)
    {
    }

    public FilaPrioridadeMaximaVetorial(int capacidade, IComparador<T> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Heap
    protected override bool eOrdenavel(int comparacao)
    {
      return comparacao < 0;
    }
    #endregion
  }

  public class FilaPrioridadeMinimaVetorial<T> : FilaPrioridadeVetorial<T>
  {
    #region Construtores
    public FilaPrioridadeMinimaVetorial(int capacidade) : base(capacidade)
    {
    }

    public FilaPrioridadeMinimaVetorial(int capacidade, FuncaoComparacao<T> comparacao) : base(capacidade, comparacao)
    {
    }

    public FilaPrioridadeMinimaVetorial(int capacidade, IComparador<T> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Heap
    protected override bool eOrdenavel(int comparacao)
    {
      return comparacao > 0;
    }
    #endregion
  }
  #endregion

  #region Filas Encadeadas de Prioridade
  public abstract class FilaPrioridadeEncadeada<T> : HeapEncadeado<T>, IEnfileiravel<T>
  {
    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public FilaPrioridadeEncadeada() : base()
    {
    }

    public FilaPrioridadeEncadeada(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public FilaPrioridadeEncadeada(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Enfileirável
    public void enfileirar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      inserir(item);
    }

    public T desenfileirar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return excluir(0);
    }

    public T espiar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return primeiro.dado;
    }

    public void limpar()
    {
      excluirTodos();
    }
    #endregion
  }

  public class FilaPrioridadeMaximaEncadeada<T> : FilaPrioridadeEncadeada<T>
  {
    #region Construtores
    public FilaPrioridadeMaximaEncadeada() : base()
    {
    }

    public FilaPrioridadeMaximaEncadeada(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public FilaPrioridadeMaximaEncadeada(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Heap
    protected override bool eOrdenavel(int comparacao)
    {
      return comparacao < 0;
    }
    #endregion
  }

  public class FilaPrioridadeMinimaEncadeada<T> : FilaPrioridadeEncadeada<T>
  {
    #region Construtores
    public FilaPrioridadeMinimaEncadeada() : base()
    {
    }

    public FilaPrioridadeMinimaEncadeada(FuncaoComparacao<T> comparacao) : base(comparacao)
    {
    }

    public FilaPrioridadeMinimaEncadeada(IComparador<T> comparador) : base(comparador)
    {
    }
    #endregion

    #region Heap
    protected override bool eOrdenavel(int comparacao)
    {
      return comparacao > 0;
    }
    #endregion
  }
  #endregion
}
