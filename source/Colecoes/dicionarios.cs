﻿/*******************************************************************************
 * 
 * Arquivo  : dicionarios.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-04-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as classes de coleções do tipo tabela de símbolos.
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes.Abstracoes;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes
{
  #region Dicionários Hashes
  public class DicionarioHashSondado<TChave, TValor> : HashSondado    <TChave, TValor>,
                                                       IDicionarizavel<TChave, TValor>
  {
    #region Atributos
    private readonly ColecaoChaves  fChaves;
    private readonly ColecaoValores fValores;
    #endregion

    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }

    public IEnumeravel<TChave> chaves
    {
      get { return fChaves; }
    }

    public IEnumeravel<TValor> valores
    {
      get { return fValores; }
    }

    public TValor this[TChave chave]
    {
      get
      {
        Item item = buscarItem(chave);

        if (item == null)
          throw Excecoes.ChaveDicionario();

        return item.valor;
      }
      set
      {
        adicionar(chave, value);
      }
    }
    #endregion

    #region Construtores
    public DicionarioHashSondado() : base()
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(FuncaoIgualdade<TChave> igualdade) : base(igualdade)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(FuncaoProjecao<TChave, int> hash) : base(hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(igualdade, hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(IComparadorIgualdade<TChave> comparador) : base(comparador)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(int capacidade) : base(capacidade)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(int capacidade, FuncaoIgualdade<TChave> igualdade) : base(capacidade, igualdade)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(int capacidade, FuncaoProjecao<TChave, int> hash) : base(capacidade, hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(int capacidade, FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(capacidade, igualdade, hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashSondado(int capacidade, IComparadorIgualdade<TChave> comparador) : base(capacidade, comparador)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public KeyValuePair<TChave, TValor>[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
    {
      return obterEnumeradorPares();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Dicionarizável
    public void adicionar(TChave chave, TValor valor)
    {
      Item item = buscarItem(chave);

      if (item != null)
        item.valor = valor;
      else
      {
        if (cheio)
          throw Excecoes.ColecaoCheia();

        inserirItem(chave, valor);
      }
    }

    public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
    {
      if (dicionario != null)
        foreach (var par in dicionario)
          adicionar(par.Key, par.Value);
    }

    public bool remover(TChave chave)
    {
      Item item = (vazio) ? null : buscarItem(chave);

      if (item != null)
        excluirItem(item);

      return (item != null);
    }

    public bool contem(TChave chave)
    {
      return (!vazio) && (buscarItem(chave) != null);
    }

    public void limpar()
    {
      if (!vazio)
      {
        excluirTodos();
        redimensionar(4);
      }
    }

    public bool tentarObter(TChave chave, out TValor valor)
    {
      Item item = buscarItem(chave);

      valor = (item != null) ? item.valor : default(TValor);

      return (item != null);
    }
    #endregion
  }

  public class DicionarioHashEncadeado<TChave, TValor> : HashEncadeado  <TChave, TValor>,
                                                         IDicionarizavel<TChave, TValor>
  {
    #region Atributos
    private readonly ColecaoChaves  fChaves;
    private readonly ColecaoValores fValores;
    #endregion

    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }

    public IEnumeravel<TChave> chaves
    {
      get { return fChaves; }
    }

    public IEnumeravel<TValor> valores
    {
      get { return fValores; }
    }

    public TValor this[TChave chave]
    {
      get
      {
        Item item = buscarItem(chave);

        if (item == null)
          throw Excecoes.ChaveDicionario();

        return item.valor;
      }
      set
      {
        adicionar(chave, value);
      }
    }
    #endregion

    #region Construtores
    public DicionarioHashEncadeado() : base()
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(FuncaoIgualdade<TChave> igualdade) : base(igualdade)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(FuncaoProjecao<TChave, int> hash) : base(hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(igualdade, hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(IComparadorIgualdade<TChave> comparador) : base(comparador)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(int capacidade) : base(capacidade)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(int capacidade, FuncaoIgualdade<TChave> igualdade) : base(capacidade, igualdade)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(int capacidade, FuncaoProjecao<TChave, int> hash) : base(capacidade, hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(int capacidade, FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(capacidade, igualdade, hash)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }

    public DicionarioHashEncadeado(int capacidade, IComparadorIgualdade<TChave> comparador) : base(capacidade, comparador)
    {
      fChaves  = new ColecaoChaves (this);
      fValores = new ColecaoValores(this);
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public KeyValuePair<TChave, TValor>[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
    {
      return obterEnumeradorPares();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Dicionarizável
    public void adicionar(TChave chave, TValor valor)
    {
      Item item = buscarItem(chave);

      if (item != null)
        item.valor = valor;
      else
      {
        if (cheio)
          throw Excecoes.ColecaoCheia();

        inserirItem(chave, valor);
      }
    }

    public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
    {
      if (dicionario != null)
        foreach (var par in dicionario)
          adicionar(par.Key, par.Value);
    }

    public bool remover(TChave chave)
    {
      Item item = (vazio) ? null : buscarItem(chave);

      if (item != null)
        excluirItem(item);

      return (item != null);
    }

    public bool contem(TChave chave)
    {
      return (!vazio) && (buscarItem(chave) != null);
    }

    public void limpar()
    {
      if (!vazio)
      {
        excluirTodos();
        redimensionar(4);
      }
    }

    public bool tentarObter(TChave chave, out TValor valor)
    {
      Item item = buscarItem(chave);

      valor = (item != null) ? item.valor : default(TValor);

      return (item != null);
    }
    #endregion
  }
  #endregion

  #region Dicionários Ordenados
  public class DicionarioOrdenadoVetorial<TChave, TValor> : VetorizacaoOrdenada<TChave, TValor>,
                                                            IDicionarizavel    <TChave, TValor>
  {
    #region Atributos
    private readonly ChavesDicionario  fChaves;
    private readonly ValoresDicionario fValores;
    #endregion

    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }

    public IEnumeravel<TChave> chaves
    {
      get { return fChaves; }
    }

    public IEnumeravel<TValor> valores
    {
      get { return fValores; }
    }

    public TValor this[TChave chave]
    {
      get
      {
        int indice = indexar(chave);

        if (comparar(chave, indice) == 0)
          throw Excecoes.ChaveDicionario();

        return this[indice].Value;
      }
      set
      {
        adicionar(chave, value);
      }
    }
    #endregion

    #region Construtores
    public DicionarioOrdenadoVetorial(int capacidade) : base(capacidade)
    {
      fChaves  = new ChavesDicionario (this);
      fValores = new ValoresDicionario(this);
    }

    public DicionarioOrdenadoVetorial(int capacidade, FuncaoComparacao<TChave> comparacao) : base(capacidade, comparacao)
    {
      fChaves  = new ChavesDicionario (this);
      fValores = new ValoresDicionario(this);
    }

    public DicionarioOrdenadoVetorial(int capacidade, IComparador<TChave> comparador) : base(capacidade, comparador)
    {
      fChaves  = new ChavesDicionario (this);
      fValores = new ValoresDicionario(this);
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public KeyValuePair<TChave, TValor>[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
    {
      return obterEnumeradorPares(0, total);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Dicionarizável
    public void adicionar(TChave chave, TValor valor)
    {
      if (vazio)
        inserirValor(0, chave, valor);
      else
      {
        int indice     = indexar(chave),
            comparacao = comparar(chave, indice);

        if (comparacao == 0)
          this[indice] = new KeyValuePair<TChave, TValor>(chave, valor);
        else
        {
          if (cheio)
            throw Excecoes.ColecaoCheia();

          inserirValor(comparacao < 0 ? indice : indice + 1, chave, valor);
        }
      }
    }

    public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
    {
      if (dicionario != null)
        foreach (var par in dicionario)
          adicionar(par.Key, par.Value);
    }

    public bool remover(TChave chave)
    {
      if (vazio)
        return false;

      int  indice = indexar(chave);
      bool result = (comparar(chave, indice) == 0);

      if (result)
        excluirValor(indice);

      return result;
    }

    public bool contem(TChave chave)
    {
      return contemChave(chave);
    }

    public void limpar()
    {
      excluirTodos();
    }

    public bool tentarObter(TChave chave, out TValor valor)
    {
      int  indice = indexar(chave);
      bool result = comparar(chave, indice) == 0;
           valor  = result ? base[indice].Value : default(TValor);

      return result;
    }
    #endregion

    #region Subcoleções
    protected abstract class ColecaoOrdenada<T> : IEnumeravel<T>
    {
      #region Propriedades
      protected DicionarioOrdenadoVetorial<TChave, TValor> colecao
      {
        get;
        private set;
      }

      public int total
      {
        get { return colecao.total; }
      }
      public bool vazio
      {
        get { return colecao.vazio; }
      }
      public bool cheio
      {
        get { return colecao.cheio; }
      }
      public bool somenteLeitura
      {
        get { return true; }
      }
      #endregion

      #region Construtor
      public ColecaoOrdenada(DicionarioOrdenadoVetorial<TChave, TValor> colecao)
      {
        this.colecao  = colecao ?? throw Excecoes.ArgumentoNulo(nameof(colecao));
      }
      #endregion

      #region Enumerável
      public string toString()
      {
        return ExtensorPadrao.toString(this);
      }

      public T[] toArray()
      {
        return ExtensorPadrao.toArray(this);
      }

      public abstract IEnumerator<T> GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }
        #endregion
    }

    protected class ChavesDicionario : ColecaoOrdenada<TChave>
    {
      #region Construtor
      public ChavesDicionario(DicionarioOrdenadoVetorial<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<TChave> GetEnumerator()
      {
        return colecao.obterEnumeradorChaves(0, colecao.total);
      }
      #endregion
    }

    protected class ValoresDicionario : ColecaoOrdenada<TValor>
    {
      #region Construtor
      public ValoresDicionario(DicionarioOrdenadoVetorial<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<TValor> GetEnumerator()
      {
        return colecao.obterEnumeradorValores(0, colecao.total);
      }
      #endregion
    }
    #endregion
  }

  public class DicionarioOrdenadoEncadeado<TChave, TValor> : ArvoreRubronegraOrdenada<TChave, TValor>,
                                                             IDicionarizavel         <TChave, TValor>
  {
    #region Atributos
    private readonly ChavesDicionario  fChaves;
    private readonly ValoresDicionario fValores;
    #endregion

    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }

    public IEnumeravel<TChave> chaves
    {
      get { return fChaves; }
    }

    public IEnumeravel<TValor> valores
    {
      get { return fValores; }
    }

    public TValor this[TChave chave]
    {
      get
      {
        No no = buscarNo(chave);

        if (no == null)
          throw Excecoes.ChaveDicionario();

        return no.valor;
      }
      set
      {
        adicionar(chave, value);
      }
    }
    #endregion

    #region Construtores
    public DicionarioOrdenadoEncadeado() : base()
    {
      fChaves  = new ChavesDicionario (this);
      fValores = new ValoresDicionario(this);
    }

    public DicionarioOrdenadoEncadeado(FuncaoComparacao<TChave> comparacao) : base(comparacao)
    {
      fChaves  = new ChavesDicionario (this);
      fValores = new ValoresDicionario(this);
    }

    public DicionarioOrdenadoEncadeado(IComparador<TChave> comparador) : base(comparador)
    {
      fChaves  = new ChavesDicionario (this);
      fValores = new ValoresDicionario(this);
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public KeyValuePair<TChave, TValor>[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
    {
      return obterEnumeradorPares(buscarNoMinimo(raiz), buscarNoMaximo(raiz));
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Dicionarizável
    public void adicionar(TChave chave, TValor valor)
    {
      No no = buscarNo(chave);

      if (no != null)
        no.valor = valor;
      else
      {
        if (cheio)
          throw Excecoes.ColecaoCheia();

        inserirNo( new No(chave, valor) );
      }
    }

    public void adicionar(IDicionarizavel<TChave, TValor> dicionario)
    {
      if (dicionario != null)
        foreach (var par in dicionario)
          adicionar(par.Key, par.Value);
    }

    public bool remover(TChave chave)
    {
      No no = vazio ? null : buscarNo(chave);

      if (no != null)
        excluirNo(no);

      return (no != null);
    }

    public bool contem(TChave chave)
    {
      return (!vazio) && (buscarNo(chave) != null);
    }

    public void limpar()
    {
      raiz = null;
    }

    public bool tentarObter(TChave chave, out TValor valor)
    {
      No no    = buscarNo(chave);
         valor = (no != null) ? no.valor : default(TValor);

      return (no != null);
    }
    #endregion

    #region Subcoleções
    protected abstract class ColecaoDicionario<T> : IEnumeravel<T>
    {
      #region Propriedades
      protected DicionarioOrdenadoEncadeado<TChave, TValor> colecao
      {
        get;
        private set;
      }

      public int total
      {
        get { return colecao.total; }
      }
      public bool vazio
      {
        get { return colecao.vazio; }
      }
      public bool cheio
      {
        get { return colecao.cheio; }
      }
      public bool somenteLeitura
      {
        get { return true; }
      }
      #endregion

      #region Construtor
      public ColecaoDicionario(DicionarioOrdenadoEncadeado<TChave, TValor> colecao)
      {
        this.colecao  = colecao ?? throw Excecoes.ArgumentoNulo(nameof(colecao));
      }
      #endregion

      #region Enumerável
      public string toString()
      {
        return ExtensorPadrao.toString(this);
      }

      public T[] toArray()
      {
        return ExtensorPadrao.toArray(this);
      }

      public abstract IEnumerator<T> GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }
        #endregion
    }

    protected class ChavesDicionario : ColecaoDicionario<TChave>
    {
      #region Construtor
      public ChavesDicionario(DicionarioOrdenadoEncadeado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<TChave> GetEnumerator()
      {
        return colecao.obterEnumeradorChaves
        (
          colecao.buscarNoMinimo(colecao.raiz),
          colecao.buscarNoMaximo(colecao.raiz)
        );
      }
      #endregion
    }

    protected class ValoresDicionario : ColecaoDicionario<TValor>
    {
      #region Construtor
      public ValoresDicionario(DicionarioOrdenadoEncadeado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<TValor> GetEnumerator()
      {
        return colecao.obterEnumeradorValores
        (
          colecao.buscarNoMinimo(colecao.raiz),
          colecao.buscarNoMaximo(colecao.raiz)
        );
      }
      #endregion
    }
    #endregion
  }
  #endregion
}