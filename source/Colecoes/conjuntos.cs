﻿/*******************************************************************************
 * 
 * Arquivo  : conjuntos.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-04-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as classes de coleções do tipo conjunto.
 * 
 *******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes.Abstracoes;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes
{
  #region Conjuntos Hashes
  public class ConjuntoHashSondado<TChave> : HashSondado<TChave, bool?>, IDistinguivel<TChave>
  {
    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public ConjuntoHashSondado() : base()
    {
    }

    public ConjuntoHashSondado(FuncaoIgualdade<TChave> igualdade) : base(igualdade)
    {
    }

    public ConjuntoHashSondado(FuncaoProjecao<TChave, int> hash) : base(hash)
    {
    }

    public ConjuntoHashSondado(FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(igualdade, hash)
    {
    }

    public ConjuntoHashSondado(IComparadorIgualdade<TChave> comparador) : base(comparador)
    {
    }

    public ConjuntoHashSondado(int capacidade) : base(capacidade)
    {
    }

    public ConjuntoHashSondado(int capacidade, FuncaoIgualdade<TChave> igualdade) : base(capacidade, igualdade)
    {
    }

    public ConjuntoHashSondado(int capacidade, FuncaoProjecao<TChave, int> hash) : base(capacidade, hash)
    {
    }

    public ConjuntoHashSondado(int capacidade, FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(capacidade, igualdade, hash)
    {
    }

    public ConjuntoHashSondado(int capacidade, IComparadorIgualdade<TChave> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public TChave[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<TChave> GetEnumerator()
    {
      return obterEnumeradorChaves();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Distinguível
    public bool adicionar(TChave chave)
    {
      bool result = (cheio) ? false : (buscarItem(chave) == null);

      if (result)
        inserirItem(chave, null);

      return result;
    }

    public bool remover(TChave chave)
    {
      Item item = (vazio) ? null : buscarItem(chave);

      if (item != null)
        excluirItem(item);

      return (item != null);
    }

    public bool contem(TChave chave)
    {
      return (!vazio) && (buscarItem(chave) != null);
    }

    public void limpar()
    {
      if (!vazio)
      {
        excluirTodos();
        redimensionar(4);
      }
    }

    public void unir(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          adicionar(item);
    }

    public void excetuar(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          remover(item);
    }

    public void interseccionar(IEnumeravel<TChave> colecao)
    {
      Item[] novos  = new Item[Math.Min(total, colecao?.total ?? 0)];
      int    indice = -1;

      if (colecao != null)
        foreach (var valor in colecao)
        {
          Item item = buscarItem(valor);

          if (item != null)
          {
            extrairItem(item);
            novos[++indice] = item;
          }
        }

      excluirTodos();
      redimensionar((byte)Math.Floor(Math.Log(indice + 1, 2) + 1));

      while (indice >= 0)
        inserirItem(novos[indice--]);
    }
    #endregion
  }

  public class ConjuntoHashEncadeado<TChave> : HashEncadeado<TChave, bool?>, IDistinguivel<TChave>
  {
    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public ConjuntoHashEncadeado() : base()
    {
    }

    public ConjuntoHashEncadeado(FuncaoIgualdade<TChave> igualdade) : base(igualdade)
    {
    }

    public ConjuntoHashEncadeado(FuncaoProjecao<TChave, int> hash) : base(hash)
    {
    }

    public ConjuntoHashEncadeado(FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(igualdade, hash)
    {
    }

    public ConjuntoHashEncadeado(IComparadorIgualdade<TChave> comparador) : base(comparador)
    {
    }

    public ConjuntoHashEncadeado(int capacidade) : base(capacidade)
    {
    }

    public ConjuntoHashEncadeado(int capacidade, FuncaoIgualdade<TChave> igualdade) : base(capacidade, igualdade)
    {
    }

    public ConjuntoHashEncadeado(int capacidade, FuncaoProjecao<TChave, int> hash) : base(capacidade, hash)
    {
    }

    public ConjuntoHashEncadeado(int capacidade, FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash) : base(capacidade, igualdade, hash)
    {
    }

    public ConjuntoHashEncadeado(int capacidade, IComparadorIgualdade<TChave> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public TChave[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<TChave> GetEnumerator()
    {
      return obterEnumeradorChaves();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Distinguível
    public bool adicionar(TChave chave)
    {
      bool result = (cheio) ? false : (buscarItem(chave) == null);

      if (result)
        inserirItem(chave, null);

      return result;
    }

    public bool remover(TChave chave)
    {
      Item item = (vazio) ? null : buscarItem(chave);

      if (item != null)
        excluirItem(item);

      return (item != null);
    }

    public bool contem(TChave chave)
    {
      return (!vazio) && (buscarItem(chave) != null);
    }

    public void limpar()
    {
      if (!vazio)
      {
        excluirTodos();
        redimensionar(4);
      }
    }

    public void unir(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          adicionar(item);
    }

    public void excetuar(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          remover(item);
    }

    public void interseccionar(IEnumeravel<TChave> colecao)
    {
      Item[] novos  = new Item[Math.Min(total, colecao?.total ?? 0)];
      int    indice = -1;

      if (colecao != null)
        foreach (var valor in colecao)
        {
          Item item = buscarItem(valor);

          if (item != null)
          {
            extrairItem(item);
            novos[++indice] = item;
          }
        }

      excluirTodos();
      redimensionar((byte)Math.Floor(Math.Log(indice + 1, 2) + 1));

      while (indice >= 0)
        inserirItem(novos[indice--]);
    }
    #endregion
  }
  #endregion

  #region Conjuntos Ordenados
  public class ConjuntoOrdenadoVetorial<TChave> : VetorizacaoOrdenada<TChave, bool?>, IDistinguivel<TChave>
  {
    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public ConjuntoOrdenadoVetorial(int capacidade) : base(capacidade)
    {
    }

    public ConjuntoOrdenadoVetorial(int capacidade, FuncaoComparacao<TChave> comparacao) : base(capacidade, comparacao)
    {
    }

    public ConjuntoOrdenadoVetorial(int capacidade, IComparador<TChave> comparador) : base(capacidade, comparador)
    {
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public TChave[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<TChave> GetEnumerator()
    {
      return obterEnumeradorChaves(0, total);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Distinguível
    public bool adicionar(TChave chave)
    {
      if (cheio)
        return false;
      else if (vazio)
      {
        inserirValor(0, chave, null);
        return true;
      }
      else
      {
        int indice     = indexar (chave),
            comparacao = comparar(chave, indice);

        if (comparacao < 0)
          inserirValor(indice, chave, null);
        else if (comparacao > 0)
          inserirValor(indice + 1, chave, null);

        return (comparacao != 0);
      }
    }

    public bool remover(TChave chave)
    {
      if (vazio)
        return false;

      int  indice = indexar(chave);
      bool result = comparar(chave, indice) == 0;

      if (result)
        excluirValor(indice);

      return result;
    }

    public bool contem(TChave chave)
    {
      return contemChave(chave);
    }

    public void limpar()
    {
      excluirTodos();
    }

    public void unir(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          adicionar(item);
    }

    public void excetuar(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          remover(item);
    }

    public void interseccionar(IEnumeravel<TChave> colecao)
    {
      TChave[] novos  = new TChave[Math.Min(total, colecao?.total ?? 0)];
      int      indice = -1;

      if (colecao != null)
        foreach (var valor in colecao)
          if (contemChave(valor))
            novos[++indice] = valor;

      excluirTodos();

      while (indice >= 0)
        adicionar(novos[indice--]);
    }
    #endregion
  }

  public class ConjuntoOrdenadoEncadeado<TChave> : ArvoreRubronegraOrdenada<TChave, bool?>, IDistinguivel<TChave>
  {
    #region Propriedades
    public bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public ConjuntoOrdenadoEncadeado() : base()
    {
    }

    public ConjuntoOrdenadoEncadeado(FuncaoComparacao<TChave> comparacao) : base()
    {
    }

    public ConjuntoOrdenadoEncadeado(IComparador<TChave> comparador) : base()
    {
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public TChave[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<TChave> GetEnumerator()
    {
      return obterEnumeradorChaves(buscarNoMinimo(raiz), buscarNoMaximo(raiz));
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Distinguível
    public bool adicionar(TChave chave)
    {
      bool result = (cheio) ? false : (buscarNo(chave) == null);

      if (result)
        inserirNo( new No(chave, null) );

      return result;
    }

    public bool remover(TChave chave)
    {
      No no = (vazio) ? null : buscarNo(chave);

      if (no != null)
        excluirNo(no);

      return (no != null);
    }

    public bool contem(TChave chave)
    {
      return (!vazio) && (buscarNo(chave) != null);
    }

    public void limpar()
    {
      raiz = null;
    }

    public void unir(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          adicionar(item);
    }

    public void excetuar(IEnumeravel<TChave> colecao)
    {
      if (colecao != null)
        foreach (var item in colecao)
          remover(item);
    }

    public void interseccionar(IEnumeravel<TChave> colecao)
    {
      TChave[] novos  = new TChave[Math.Min(total, colecao?.total ?? 0)];
      int      indice = -1;

      if (colecao != null)
        foreach (var valor in colecao)
          if (contem(valor))
            novos[++indice] = valor;

      limpar();

      while (indice >= 0)
        inserirNo( new No(novos[indice--], null) );
    }
    #endregion
  }
  #endregion
}