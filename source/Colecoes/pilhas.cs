﻿/*******************************************************************************
 * 
 * Arquivo  : pilhas.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-04-01 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as classes de coleções do tipo pilha.
 * 
 *******************************************************************************/
using System;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes.Abstracoes;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Colecoes
{
  public class PilhaVetorial<T> : Vetorizacao<T>, IEmpilhavel<T>
  {
    #region Atributos
    private int topo;
    #endregion

    #region Propriedades
    public override int total
    {
      get { return topo + 1; }
    }
    public override bool vazio
    {
      get { return topo == -1; }
    }
    public override bool cheio
    {
      get { return topo + 1 == capacidade; }
    }
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Construtores
    public PilhaVetorial(int capacidade) : base(capacidade)
    {
      topo = -1;
    }
    #endregion

    #region Enumerável
    public override IEnumerator<T> GetEnumerator()
    {
      for (int i = 0; i <= topo; i++)
        yield return this[i];
    }
    #endregion

    #region Empilhável
    public void empilhar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      this[++topo] = item;
    }

    public T desempilhar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return this[topo--];
    }

    public T espiar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return this[topo];
    }

    public void limpar()
    {
      topo = -1;
    }
    #endregion
  }

  public class PilhaEncadeada<T> : Encadeamento<T>, IEmpilhavel<T>
  {
    #region Atributos
    private No primeiro;
    #endregion

    #region Propriedades
    public override bool somenteLeitura
    {
      get { return false; }
    }
    #endregion

    #region Enumerável
    public override IEnumerator<T> GetEnumerator()
    {
      return obterEnumerador(primeiro);
    }
    #endregion

    #region Empilhável
    public void empilhar(T item)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      No no = new No(item, primeiro);
      primeiro = no;

      total++;
    }

    public T desempilhar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      No no      = primeiro;
      primeiro   = no.proximo;
      no.proximo = null;
      total--;

      return no.valor;
    }

    public T espiar()
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      return primeiro.valor;
    }

    public void limpar()
    {
      primeiro = null;
    }
    #endregion
  }
}
