﻿/*******************************************************************************
 * 
 * Arquivo  : heap-vetorial.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções de listas ligadas.
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class Encadeamento<T> : IEnumeravel<T>
  {
    #region Propriedades
    public int total
    {
      get;
      protected set;
    }
    public bool vazio
    {
      get { return total == 0; }
    }
    public bool cheio
    {
      get { return total == int.MaxValue; }
    }

    public abstract bool somenteLeitura { get; }
    #endregion

    #region Métodos
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public abstract IEnumerator<T> GetEnumerator();

    protected IEnumerator<T> obterEnumerador(No primeiro)
    {
      for (No proximo = primeiro; proximo != null; proximo = proximo.proximo)
        yield return proximo.valor;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Nó
    protected class No
    {
      #region Propriedades
      public T  valor   { get; set; }
      public No proximo { get; set; }
      #endregion

      #region Construtores
      public No(T valor, No proximo)
      {
        this.valor   = valor;
        this.proximo = proximo;
      }

      public No(T valor) : this(valor, null)
      {
      }
      #endregion
    }
    #endregion
  }
}