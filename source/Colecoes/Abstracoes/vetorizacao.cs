﻿/*******************************************************************************
 * 
 * Arquivo  : vetorizacao.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-29 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções vetoriais.
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class Vetorizacao<T> : IEnumeravel<T>
  {
    #region Atributos
    private readonly T[] fItens;
    #endregion

    #region Propriedades
    protected int capacidade
    {
      get;
      private set;
    }

    protected T this[int indice]
    {
      get
      {
        if ((indice < 0) || (indice >= capacidade))
          throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

        return fItens[indice];
      }
      set
      {
        if ((indice < 0) || (indice >= capacidade))
          throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

        fItens[indice] = value;
      }
    }

    public abstract int  total          { get; }
    public abstract bool vazio          { get; }
    public abstract bool cheio          { get; }
    public abstract bool somenteLeitura { get; }
    #endregion

    #region Construtores
    public Vetorizacao(int capacidade, int comprimento)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));
      if (comprimento < capacidade)
        throw Excecoes.ArgumentoNegativo(nameof(comprimento));

      this.capacidade = capacidade;
      this.fItens     = new T[comprimento];
    }

    public Vetorizacao(int capacidade) : this(capacidade, capacidade)
    {
    }
    #endregion

    #region Métodos
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public abstract IEnumerator<T> GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion
  }
}