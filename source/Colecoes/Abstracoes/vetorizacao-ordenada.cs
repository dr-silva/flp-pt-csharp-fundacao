﻿/*******************************************************************************
 * 
 * Arquivo  : vetorizacao-ordenada.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções vetoriais ordenadas.
 * 
 *******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class VetorizacaoOrdenada<TChave, TValor> : IOrdenavel<TChave>
  {
    #region Atributos
    private readonly KeyValuePair<TChave, TValor>[] itens;
    private readonly IComparador<TChave>            comparador;
    #endregion

    #region Propriedades
    protected int capacidade
    {
      get;
      private set;
    }

    protected KeyValuePair<TChave, TValor> this[int indice]
    {
      get
      {
        if ((indice < 0) || (indice >= capacidade))
          throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

        return itens[indice];
      }
      set
      {
        if ((indice < 0) || (indice >= capacidade))
          throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

        itens[indice] = value;
      }
    }

    public int total
    {
      get;
      private set;
    }

    public bool vazio
    {
      get { return total == 0; }
    }

    public bool cheio
    {
      get { return total == capacidade; }
    }

    public abstract bool somenteLeitura { get; }

    public TChave minimo
    {
      get { return desindexar(0); }
    }

    public TChave maximo
    {
      get { return desindexar(total - 1); }
    }
    #endregion

    #region Construtores
    public VetorizacaoOrdenada(int capacidade)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      this.capacidade = capacidade;
      this.itens      = new KeyValuePair<TChave, TValor>[capacidade];
      this.comparador = FabricaComparador.criar<TChave>();
    }

    public VetorizacaoOrdenada(int capacidade, FuncaoComparacao<TChave> comparacao)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      if (comparacao == null)
        throw Excecoes.ArgumentoNulo(nameof(comparacao));

      this.capacidade = capacidade;
      this.itens      = new KeyValuePair<TChave, TValor>[capacidade];
      this.comparador = FabricaComparador.criar<TChave>(comparacao);
    }

    public VetorizacaoOrdenada(int capacidade, IComparador<TChave> comparador)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      this.capacidade = capacidade;
      this.itens      = new KeyValuePair<TChave, TValor>[capacidade];
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Assistentes
    protected int comparar(TChave chave, int indice)
    {
      return comparador.Compare(chave, itens[indice].Key);
    }

    protected void inserirValor(int indice, TChave chave, TValor valor)
    {
      for (int i = total - 1; i >= indice; i--)
        itens[i + 1] = itens[i];

      itens[indice] = new KeyValuePair<TChave, TValor>(chave, valor);
      total++;
    }

    protected void excluirValor(int indice)
    {
      for (int i = indice + 1; i < total; i++)
        itens[i - 1] = itens[i];

      total--;
    }

    protected void excluirTodos()
    {
      total = 0;
    }

    protected bool contemChave(TChave chave)
    {
      return (!vazio) && (comparar(chave, indexar(chave)) == 0);
    }
    #endregion

    #region Ordenável
    public TChave piso(TChave chave)
    {
      int i = indexar(chave);

      if (comparar(chave, i) < 0)
        i--;

      return desindexar(i);
    }

    public TChave teto(TChave chave)
    {
      int i = indexar(chave);

      return desindexar(i);
    }

    public int indexar(TChave chave)
    {
      int minimo = 0,
          maximo = total - 1,
          result = -1;

      while ((minimo < maximo) && (result < 0))
      {
        int mediana    = (minimo + maximo) / 2,
            comparacao = comparar(chave, mediana);

        if (comparacao < 0)
          maximo = mediana - 1;
        else if (comparacao > 0)
          minimo = mediana + 1;
        else
          result = mediana;
      }

      if (result < 0)
        result = minimo;

      return result;
    }

    public TChave desindexar(int indice)
    {
      if ((indice < 0) || (indice >= total))
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, total - 1);

      return itens[indice].Key;
    }

    public int contar(TChave inferior, TChave superior)
    {
      int idxInf = indexar(inferior),
          idxSup = indexar(superior);

      if ((idxSup > 0) && (comparar(superior, idxSup) == 0))
        idxSup--;

      int result = idxSup - idxInf;

      return (result >= 0) ? result + 1 : result - 1;
    }

    public IEnumeravel<TChave> obterChaves(TChave inferior, TChave superior)
    {
      return null;
    }
    #endregion

    #region Enumeradores
    protected IEnumerable<KeyValuePair<TChave, TValor>> obterPares
    (
      int primeiro, int total
    )
    {
      int contador = (total >= 0) ? +1 : -1;
          total    = (total >= 0) ? +total : -total;

      for (int i = total, j = primeiro; i > 0; i--, j = j + contador)
        yield return itens[j];
    }

    protected IEnumerator<TChave> obterEnumeradorChaves(int primeiro, int total)
    {
      foreach (var item in obterPares(primeiro, total))
        yield return item.Key;
    }

    protected IEnumerator<TValor> obterEnumeradorValores(int primeiro, int total)
    {
      foreach (var item in obterPares(primeiro, total))
        yield return item.Value;
    }

    protected IEnumerator<KeyValuePair<TChave, TValor>> obterEnumeradorPares
    (
      int primeiro, int total
    )
    {
      return obterPares(primeiro, total).GetEnumerator();
    }
    #endregion

    #region Subcoleções
    protected abstract class ColecaoVetorial<T> : IEnumeravel<T>
    {
      #region Propriedades
      protected int primeiro
      {
        get;
        private set;
      }
      protected int ultimo
      {
        get;
        private set;
      }
      protected VetorizacaoOrdenada<TChave, TValor> colecao
      {
        get;
        private set;
      }

      public int total
      {
        get;
        private set;
      }
      public bool vazio
      {
        get { return total == 0; }
      }
      public bool cheio
      {
        get { return total == colecao.total; }
      }
      public bool somenteLeitura
      {
        get { return true; }
      }
      #endregion

      #region Construtor
      public ColecaoVetorial(VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo)
      {
        this.colecao  = colecao ?? throw Excecoes.ArgumentoNulo(nameof(colecao));
        this.primeiro = primeiro;
        this.ultimo   = ultimo;
        this.total    = Math.Abs
        (
          colecao.contar(colecao.itens[primeiro].Key, colecao.itens[ultimo].Key)
        );
      }
      #endregion

      #region Enumerável
      public string toString()
      {
        return ExtensorPadrao.toString(this);
      }

      public T[] toArray()
      {
        return ExtensorPadrao.toArray(this);
      }

      public abstract IEnumerator<T> GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }
        #endregion
    }

    protected class ColecaoChaves : ColecaoVetorial<TChave>
    {
      #region Construtor
      public ColecaoChaves
      (
        VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo
      ) : base(colecao, primeiro, ultimo)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<TChave> GetEnumerator()
      {
        return colecao.obterEnumeradorChaves(primeiro, total);
      }
      #endregion
    }

    protected class ColecaoValores : ColecaoVetorial<TValor>
    {
      #region Construtor
      public ColecaoValores
      (
        VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo
      ) : base(colecao, primeiro, ultimo)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<TValor> GetEnumerator()
      {
        return colecao.obterEnumeradorValores(primeiro, total);
      }
      #endregion
    }

    protected class ColecaoPares : ColecaoVetorial<KeyValuePair<TChave, TValor>>
    {
      #region Construtor
      public ColecaoPares
      (
        VetorizacaoOrdenada<TChave, TValor> colecao, int primeiro, int ultimo
      ) : base(colecao, primeiro, ultimo)
      {
      }
      #endregion

      #region Enumerable
      public override IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
      {
        return colecao.obterEnumeradorPares(primeiro, total);
      }
      #endregion
    }
    #endregion
  }
}
