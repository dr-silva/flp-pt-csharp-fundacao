﻿/*******************************************************************************
 * 
 * Arquivo  : hash-encadeado.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções por hash encadeado.
 * 
 *******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;
using FractalLotus.Fundacao.Matematica;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class HashEncadeado<TChave, TValor>
  {
    #region Atributos
    private readonly IComparadorIgualdade<TChave> comparador;
    private readonly IGeradorCongruencial         aleatorio;

    private Item[] itens;
    private byte   lgCapacidade;
    private int    capacidade,
                   fator,
                   parcela,
                   primo;
    #endregion

    #region Propriedades
    public int total
    {
      get;
      private set;
    }

    public bool vazio
    {
      get { return total == 0; }
    }

    public bool cheio
    {
      get { return total == int.MaxValue; }
    }
    #endregion

    #region Construtores
    public HashEncadeado()
    {
      comparador = FabricaComparadorIgualdade.criar<TChave>();
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar(4);
    }

    public HashEncadeado(FuncaoIgualdade<TChave> igualdade)
    {
      comparador = FabricaComparadorIgualdade.criar(igualdade);
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar(4);
    }

    public HashEncadeado(FuncaoProjecao<TChave, int> hash)
    {
      comparador = FabricaComparadorIgualdade.criar(hash);
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar(4);
    }

    public HashEncadeado(FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash)
    {
      comparador = FabricaComparadorIgualdade.criar(igualdade, hash);
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar(4);
    }

    public HashEncadeado(IComparadorIgualdade<TChave> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
      this.aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar(4);
    }

    public HashEncadeado(int capacidade)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar<TChave>();
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar((byte)Math.Ceiling(Math.Log(capacidade / 3, 2)));
    }

    public HashEncadeado(int capacidade, FuncaoIgualdade<TChave> igualdade)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar(igualdade);
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar((byte)Math.Ceiling(Math.Log(capacidade / 3, 2)));
    }

    public HashEncadeado(int capacidade, FuncaoProjecao<TChave, int> hash)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar(hash);
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar((byte)Math.Ceiling(Math.Log(capacidade / 3, 2)));
    }

    public HashEncadeado(int capacidade, FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar(igualdade, hash);
      aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar((byte)Math.Ceiling(Math.Log(capacidade / 3, 2)));
    }

    public HashEncadeado(int capacidade, IComparadorIgualdade<TChave> comparador)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
      this.aleatorio  = FabricaGeradorCongruencial.criar(TipoGeradorCongruencial.Linear);

      redimensionar((byte)Math.Ceiling(Math.Log(capacidade / 3, 2)));
    }
    #endregion

    #region Assistentes
    private int calcularHash(TChave chave)
    {
      int valor = comparador.GetHashCode(chave) & 0x7FFFFFFF,
          hash  = ((fator * valor + parcela) % primo) % capacidade;

      return (hash < 0) ? -hash : hash;
    }
    #endregion

    #region Interface
    protected void redimensionar(byte dimensao)
    {
      byte valor = (dimensao < 04) ? (byte)04 :
                   (dimensao > 31) ? (byte)31 : dimensao;

      if (valor == lgCapacidade)
        return;

      Item[] antigos          = itens;
      int    capacidadeAntiga = capacidade;
             lgCapacidade     = valor;
             capacidade       = 1 << valor;
             primo            = Primos.valor(valor + 1);
             fator            = aleatorio.uniforme(primo) + 1;
             parcela          = aleatorio.uniforme(primo);
             itens            = new Item[capacidade];

      if (total == 0)
        return;

      total = 0;
      for (int i = 0; i < capacidadeAntiga; i++)
      {
        Item item = antigos[i];

        while (item != null)
        {
          Item temp = item;
               item = item.ulterior;

          inserirItem(temp);
        }
      }
    }

    protected Item buscarItem(TChave chave)
    {
      int  i    = calcularHash(chave);
      Item item = itens[i];

      while (item != null)
        if (comparador.Equals(chave, item.chave))
          break;
        else
          item = item.ulterior;

      return item;
    }

    protected void inserirItem(Item item)
    {
      if (item != null)
      {
        item.hash = calcularHash(item.chave);

        item.anterior = null;
        item.ulterior = itens[item.hash];

        if (itens[item.hash] != null)
          itens[item.hash].anterior = item;

        itens[item.hash] = item;
        total++;
      }
    }

    protected void inserirItem(TChave chave, TValor valor)
    {
      if (total > 3 * capacidade)
        redimensionar((byte)(lgCapacidade + 1));

      inserirItem( new Item(chave, valor) );
    }

    protected void extrairItem(Item item)
    {
      if (item.anterior != null)
        item.anterior.ulterior = item.ulterior;
      if (item.ulterior != null)
        item.ulterior.anterior = item.anterior;
      if (itens[item.hash] == item)
        itens[item.hash] = item.ulterior;

      item.anterior = item.ulterior = null;
      total--;
    }

    protected void excluirItem(Item item)
    {
      if (item != null)
      {
        extrairItem(item);

        if (total < capacidade / 8)
          redimensionar((byte)(lgCapacidade - 1));
      }
    }

    protected void excluirTodos()
    {
      itens = new Item[capacidade];
      total = 0;
    }
    #endregion

    #region Item
    protected class Item
    {
      #region Propriedades
      public int    hash     { get; set; }
      public TChave chave    { get; set; }
      public TValor valor    { get; set; }
      public Item   anterior { get; set; }
      public Item   ulterior { get; set; }
      #endregion

      #region Construtores
      public Item(TChave chave, TValor valor)
      {
        this.hash     = 0;
        this.valor    = valor;
        this.chave    = chave;
        this.anterior = null;
        this.ulterior = null;
      }
      #endregion
    }
    #endregion

    #region Enumeradores
    private IEnumerable<Item> obterItens()
    {
      for (int i = 0; i < capacidade; i++)
      {
        var item = itens[i];

        while (item != null)
        {
          yield return item;

          item = item.ulterior;
        }
      }
    }

    protected IEnumerator<TChave> obterEnumeradorChaves()
    {
      foreach (var item in obterItens())
        yield return item.chave;
    }

    protected IEnumerator<TValor> obterEnumeradorValores()
    {
      foreach (var item in obterItens())
        yield return item.valor;
    }

    protected IEnumerator<KeyValuePair<TChave, TValor>> obterEnumeradorPares()
    {
      foreach (var item in obterItens())
        yield return new KeyValuePair<TChave, TValor>(item.chave, item.valor);
    }
    #endregion

    #region Subcoleções
    protected abstract class ColecaoHash<T> : IEnumeravel<T>
    {
      #region Propriedades
      protected HashEncadeado<TChave, TValor> colecao
      {
        get;
        private set;
      }

      public int  total          { get { return colecao.total; } }
      public bool vazio          { get { return colecao.vazio; } }
      public bool cheio          { get { return colecao.cheio; } }
      public bool somenteLeitura { get { return true; } }
      #endregion

      #region Construtor
      public ColecaoHash(HashEncadeado<TChave, TValor> colecao)
      {
        this.colecao = colecao ?? throw Excecoes.ArgumentoNulo(nameof(colecao));
      }
      #endregion

      #region Métodos
      public string toString()
      {
        return ExtensorPadrao.toString(this);
      }

      public T[] toArray()
      {
        return ExtensorPadrao.toArray(this);
      }

      public abstract IEnumerator<T> GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }
      #endregion
    }

    protected class ColecaoChaves : ColecaoHash<TChave>
    {
      #region Construtor
      public ColecaoChaves(HashEncadeado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<TChave> GetEnumerator()
      {
        return colecao.obterEnumeradorChaves();
      }
      #endregion
    }

    protected class ColecaoValores : ColecaoHash<TValor>
    {
      #region Construtor
      public ColecaoValores(HashEncadeado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<TValor> GetEnumerator()
      {
        return colecao.obterEnumeradorValores();
      }
      #endregion
    }

    protected class ColecaoPares : ColecaoHash<KeyValuePair<TChave, TValor>>
    {
      #region Construtor
      public ColecaoPares(HashEncadeado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
      {
        return colecao.obterEnumeradorPares();
      }
      #endregion
    }
    #endregion
  }
}