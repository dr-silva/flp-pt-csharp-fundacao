﻿/*******************************************************************************
 * 
 * Arquivo  : arvore-rubronegra-ordenada.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-31 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções que implementam uma árvore bi-
 *            nária balanceada ordenadas.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class ArvoreRubronegraOrdenada<TChave, TValor> : ArvoreRubronegra<TChave, TValor>,
                                                                   IOrdenavel<TChave>
  {
    #region Propriedades
    public TChave minimo
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return buscarNoMinimo(raiz).chave;
      }
    }
    public TChave maximo
    {
      get
      {
        if (vazio)
          throw Excecoes.ColecaoVazia();

        return buscarNoMaximo(raiz).chave;
      }
    }
    #endregion

    #region Construtores
    public ArvoreRubronegraOrdenada() : base()
    {
    }

    public ArvoreRubronegraOrdenada(FuncaoComparacao<TChave> comparacao) : base()
    {
    }

    public ArvoreRubronegraOrdenada(IComparador<TChave> comparador) : base()
    {
    }
    #endregion

    #region Ordenável
    public TChave piso(TChave chave)
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      No no = buscarNoPiso(chave);

      if (no == null)
        throw new ArgumentOutOfRangeException
        (
          string.Format("O parâmetro \"{0}\" é menor que a chave mínima", nameof(chave)),
          (Exception)null
        );

      return no.chave;
    }
    public TChave teto(TChave chave)
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      No no = buscarNoTeto(chave);

      if (no == null)
        throw new ArgumentOutOfRangeException
        (
          string.Format("O parâmetro \"{0}\" é maior que a chave máxima", nameof(chave)),
          (Exception)null
        );

      return no.chave;
    }

    public int indexar(TChave chave)
    {
      No no = buscarNoTeto(chave);

      return (no == null) ? total : indexarNo(no) - 1;
    }
    public TChave desindexar(int indice)
    {
      No no = desindexarNo(indice - 1);

      if (no == null)
        throw Excecoes.IndiceForaLimites(nameof(indice), 0, total - 1);

      return no.chave;
    }

    public int contar(TChave inferior, TChave superior)
    {
      No primeiro = buscarNoTeto(inferior),
         ultimo   = buscarNoPiso(superior);

      int total;

      if ((primeiro == null) || (ultimo == null))
        total = 0;
      else if (primeiro == ultimo)
        total = 1;
      else if (comparador.Compare(primeiro.chave, ultimo.chave) <= 0)
        total = indexarNo(ultimo) - indexarNo(primeiro) + 1;
      else
        total = indexarNo(ultimo) - indexarNo(primeiro) - 1;

      return total;
    }
    public IEnumeravel<TChave> obterChaves(TChave inferior, TChave superior)
    {
      No primeiro = buscarNoTeto(inferior),
         ultimo   = buscarNoPiso(superior);

      return new ColecaoChaves(this, primeiro, ultimo);
    }
    #endregion
  }
}