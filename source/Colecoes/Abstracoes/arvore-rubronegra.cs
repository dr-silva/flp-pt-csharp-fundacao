﻿/*******************************************************************************
 * 
 * Arquivo  : arvore-rubronegra.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-31 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções que implementam uma árvore bi-
 *            nária balanceada.
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class ArvoreRubronegra<TChave, TValor>
  {
    #region Constantes
    private static readonly bool VERMELHO = false;
    private static readonly bool PRETO    = true;
    #endregion

    #region Propriedades
    protected IComparador<TChave> comparador
    {
      get;
      private set;
    }
    protected No raiz
    {
      get;
      set;
    }

    public int total
    {
      get { return (raiz != null) ? raiz.tamanho : 0; }
    }
    public bool vazio
    {
      get { return raiz == null; }
    }
    public bool cheio
    {
      get { return total == int.MaxValue; }
    }
    #endregion

    #region Construtores
    public ArvoreRubronegra()
    {
      comparador = FabricaComparador.criar<TChave>();
    }

    public ArvoreRubronegra(FuncaoComparacao<TChave> comparacao)
    {
      comparador = FabricaComparador.criar(comparacao);
    }

    public ArvoreRubronegra(IComparador<TChave> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Assistentes
    private bool eVermelho(No no)
    {
      return (no != null) && (no.cor == VERMELHO);
    }

    private bool ePreto(No no)
    {
      return (no == null) || (no.cor == PRETO);
    }

    private int contar(No no)
    {
      int result = 0;

      if (no != null)
      {
        result += 1;

        if (no.esquerda != null)
          result += no.esquerda.tamanho;
        if (no.direita != null)
          result += no.direita.tamanho;
      }

      return result;
    }

    private void girarParaEsquerda(No no)
    {
      if ((no == null) && (no.direita == null))
        return;

      No pivo           = no.direita;
         no  .direita   = pivo.esquerda;
         pivo.ancestral = no.ancestral;

      if (pivo.esquerda != null)
        pivo.esquerda.ancestral = no;

      if (no.ancestral == null)
        raiz = pivo;
      else if (no == no.ancestral.esquerda)
        no.ancestral.esquerda = pivo;
      else
        no.ancestral.direita = pivo;

      pivo.esquerda  = no;
      no  .ancestral = pivo;
      pivo.tamanho   = no.tamanho;
      no  .tamanho   = contar(no);
    }

    private void girarParaDireita(No no)
    {
      if ((no == null) && (no.esquerda == null))
        return;

      No pivo           = no.esquerda;
         no  .esquerda  = pivo.direita;
         pivo.ancestral = no.ancestral;

      if (pivo.direita != null)
        pivo.direita.ancestral = no;

      if (no.ancestral == null)
        raiz = pivo;
      else if (no == no.ancestral.esquerda)
        no.ancestral.esquerda = pivo;
      else
        no.ancestral.direita = pivo;

      pivo.direita   = no;
      no  .ancestral = pivo;
      pivo.tamanho   = no.tamanho;
      no  .tamanho   = contar(no);
    }

    private void consertarInsercao(No no)
    {
      No avo = null,
         pai = no.ancestral;

      while (eVermelho(pai))
      {
        avo = pai.ancestral;

        if (avo == null)
          break;

        if (pai == avo.esquerda)
          no = consertarInsercaoEsquerda(no);
        else
          no = consertarInsercaoDireita(no);

        pai = no.ancestral;
      }

      raiz.cor = PRETO;
    }

    private No consertarInsercaoEsquerda(No no)
    {
      No result = null,
         tio    = no.ancestral.ancestral.direita;

      if (eVermelho(tio))
      {
        no.ancestral          .cor = PRETO;
        tio                   .cor = PRETO;
        no.ancestral.ancestral.cor = VERMELHO;
        result                     = no.ancestral.ancestral;
      }
      else
      {
        if (no == no.ancestral.direita)
        {
          no = no.ancestral;
          girarParaEsquerda(no);
        }

        no.ancestral          .cor = PRETO;
        no.ancestral.ancestral.cor = VERMELHO;

        girarParaDireita(no.ancestral.ancestral);

        result = no;
      }

      return result;
    }

    private No consertarInsercaoDireita(No no)
    {
      No result = null,
         tio    = no.ancestral.ancestral.esquerda;

      if (eVermelho(tio))
      {
        no.ancestral          .cor = PRETO;
        tio                   .cor = PRETO;
        no.ancestral.ancestral.cor = VERMELHO;
        result                     = no.ancestral.ancestral;
      }
      else
      {
        if (no == no.ancestral.esquerda)
        {
          no = no.ancestral;
          girarParaDireita(no);
        }

        no.ancestral          .cor = PRETO;
        no.ancestral.ancestral.cor = VERMELHO;

        girarParaEsquerda(no.ancestral.ancestral);

        result = no;
      }

      return result;
    }

    private void consertarExclusao(No no, No pai)
    {
      while ((no != raiz) && ePreto(no))
      {
        No neoPai = (no == null) ? pai : no.ancestral;

        if (no == neoPai.esquerda)
          no = consertarExclusaoEsquerda(no, neoPai);
        else
          no = consertarExclusaoDireita(no, neoPai);
      }

      if (no != null)
        no.cor = PRETO;
    }

    private No consertarExclusaoEsquerda(No no, No pai)
    {
      No result = null,
         irmao  = pai.direita;

      if (eVermelho(irmao))
      {
        irmao.cor = PRETO;
        pai  .cor = VERMELHO;

        girarParaEsquerda(pai);

        irmao = pai.direita;
      }

      if (ePreto(irmao?.esquerda) && ePreto(irmao?.direita))
      {
        if (irmao != null)
          irmao.cor = VERMELHO;

        result = pai;
      }
      else
      {
        if (ePreto(irmao.direita))
        {
          if (irmao.esquerda != null)
            irmao.esquerda.cor = PRETO;

          irmao.cor = VERMELHO;

          girarParaDireita(irmao);

          irmao = pai.direita;
        }

        irmao        .cor = pai.cor;
        pai          .cor = PRETO;
        irmao.direita.cor = PRETO;

        girarParaEsquerda(pai);

        result = raiz;
      }

      return result;
    }

    private No consertarExclusaoDireita(No no, No pai)
    {
      No result = null,
         irmao  = pai.esquerda;

      if (eVermelho(irmao))
      {
        irmao.cor = PRETO;
        pai  .cor = VERMELHO;

        girarParaDireita(pai);

        irmao = pai.esquerda;
      }

      if (ePreto(irmao?.esquerda) && ePreto(irmao?.direita))
      {
        if (irmao != null)
          irmao.cor = VERMELHO;

        result = pai;
      }
      else
      {
        if (ePreto(irmao.esquerda))
        {
          if (irmao.direita != null)
            irmao.direita.cor = PRETO;

          irmao.cor = VERMELHO;

          girarParaEsquerda(irmao);

          irmao = pai.esquerda;
        }

        irmao         .cor = pai.cor;
        pai           .cor = PRETO;
        irmao.esquerda.cor = PRETO;

        girarParaDireita(pai);

        result = raiz;
      }

      return result;
    }
    #endregion

    #region Buscadores
    protected No buscarNoMinimo(No no)
    {
      while ((no != null) && (no.esquerda != null))
        no = no.esquerda;

      return no;
    }

    protected No buscarNoMaximo(No no)
    {
      while ((no != null) && (no.direita != null))
        no = no.direita;

      return no;
    }

    protected No buscarNoPredecessor(No no)
    {
      No result = null;

      if (no == null)
        result = null;
      else if (no.esquerda != null)
        result = buscarNoMaximo(no.esquerda);
      else
      {
        result = no.ancestral;

        while ((result != null) && (no == result.esquerda))
        {
          no     = result;
          result = no.ancestral;
        }
      }

      return result;
    }

    protected No buscarNoSucessor(No no)
    {
      No result = null;

      if (no == null)
        result = null;
      else if (no.direita != null)
        result = buscarNoMinimo(no.direita);
      else
      {
        result = no.ancestral;

        while ((result != null) && (no == result.direita))
        {
          no     = result;
          result = no.ancestral;
        }
      }

      return result;
    }

    protected No buscarNo(TChave chave)
    {
      No result = null,
         no     = raiz;

      while ((no != null) && (result == null))
      {
        int comparacao = comparador.Compare(chave, no.chave);

        if (comparacao < 0)
          no = no.esquerda;
        else if (comparacao > 0)
          no = no.direita;
        else
          result = no;
      }

      return result;
    }

    protected No buscarNoPiso(TChave chave)
    {
      No result = null,
         no     = raiz;

      while (no != null)
      {
        int comparacao = comparador.Compare(chave, no.chave);

        if (comparacao < 0)
          no = no.esquerda;
        else if (comparacao > 0)
        {
          result = no;
          no     = no.direita;
        }
        else
        {
          result = no;
          no     = null;
        }
      }

      return result;
    }

    protected No buscarNoTeto(TChave chave)
    {
      No result = null,
         no     = raiz;

      while (no != null)
      {
        int comparacao = comparador.Compare(chave, no.chave);

        if (comparacao < 0)
        {
          result = no;
          no     = no.esquerda;
        }
        else if (comparacao > 0)
          no = no.direita;
        else
        {
          result = no;
          no     = null;
        }
      }

      return result;
    }
    #endregion

    #region Indexadores
    protected int indexarNo(No no)
    {
      int indice;

      if (no == null)
        indice = 0;
      else if (no.esquerda == null)
        indice = 1;
      else
        indice = no.esquerda.tamanho + 1;

      while ((no != null) && (no != raiz))
      {
        if (no == no.ancestral.direita)
          indice += (no.ancestral.esquerda == null) ? 1 : no.ancestral.esquerda.tamanho + 1;

        no = no.ancestral;
      }

      return indice;
    }

    protected No desindexarNo(int indice)
    {
      No result = null,
         no     = raiz;

      if ((indice < 1) || (indice > total))
        return result;

      while (indice > 0)
      {
        int i = (no.esquerda == null) ? 1 : no.esquerda.tamanho + 1;

        if (indice == i)
        {
          result = no;
          break;
        }
        else if (indice < i)
          no = no.esquerda;
        else
        {
          indice -= i;
          no      = no.direita;
        }
      }

      return result;
    }
    #endregion

    #region Estruturação
    protected void inserirNo(No no)
    {
      if (no == null)
        return;

      No pai   = null,
         traco = raiz;

      while (traco != null)
      {
        traco.tamanho = traco.tamanho + 1;
        pai           = traco;
        traco         = (comparador.Compare(no.chave, traco.chave) < 0) ? traco.esquerda : traco.direita;
      }

      no.ancestral = pai;

      if (pai == null)
        raiz = no;
      else if (comparador.Compare(no.chave, pai.chave) < 0)
        pai.esquerda = no;
      else
        pai.direita = no;

      consertarInsercao(no);
    }

    protected void excluirNo(No no)
    {
      if (no == null)
        return;

      No extraido = (no.esquerda == null) || (no.direita == null) ? no : buscarNoSucessor(no),
         x        = extraido.esquerda ?? extraido.direita;

      if (x != null)
        x.ancestral = extraido.ancestral;
      if (extraido.ancestral == null)
        raiz = x;
      else if (extraido == extraido.ancestral.esquerda)
        extraido.ancestral.esquerda = x;
      else
        extraido.ancestral.direita = x;

      if (extraido != no)
      {
        no.chave = extraido.chave;
        no.valor = extraido.valor;
      }

      no = extraido.ancestral;
      while (no != null)
      {
        no.tamanho--;
        no = no.ancestral;
      }

      if (extraido.cor == PRETO)
        consertarExclusao(x, extraido.ancestral);

      extraido.ancestral = extraido.esquerda = extraido.direita = null;
    }
    #endregion

    #region Nó
    protected class No
    {
      #region Propriedades
      public TChave chave   { get; set; }
      public TValor valor   { get; set; }
      public bool   cor     { get; set; }
      public int    tamanho { get; set; }

      public No ancestral { get; set; }
      public No esquerda  { get; set; }
      public No direita   { get; set; }
      #endregion

      #region Construtor
      public No(TChave chave, TValor valor)
      {
        this.chave     = chave;
        this.valor     = valor;
        this.cor       = VERMELHO;
        this.tamanho   = 1;
        this.ancestral = null;
        this.esquerda  = null;
        this.direita   = null;
      }
      #endregion
    }
    #endregion

    #region Enumeradores
    private IEnumerable<No> obterNos(No primeiro, No ultimo)
    {
      if ((primeiro == null) || (ultimo == null))
        yield break;

      bool ascendente = comparador.Compare(primeiro.chave, ultimo.chave) <= 0;

      for (No no = primeiro; no != ultimo; no = ascendente ? buscarNoSucessor(no) : buscarNoPredecessor(no))
        yield return no;

      yield return ultimo;
    }

    protected IEnumerator<TChave> obterEnumeradorChaves(No primeiro, No ultimo)
    {
      foreach (No no in obterNos(primeiro, ultimo))
        yield return no.chave;
    }

    protected IEnumerator<TValor> obterEnumeradorValores(No primeiro, No ultimo)
    {
      foreach (No no in obterNos(primeiro, ultimo))
        yield return no.valor;
    }

    protected IEnumerator<KeyValuePair<TChave, TValor>> obterEnumeradorPares(No primeiro, No ultimo)
    {
      foreach (No no in obterNos(primeiro, ultimo))
        yield return new KeyValuePair<TChave, TValor>(no.chave, no.valor);
    }
    #endregion

    #region Subcoleções
    protected abstract class ColecaoArborea<T> : IEnumeravel<T>
    {
      #region Propriedades
      protected ArvoreRubronegra<TChave, TValor> colecao
      {
        get;
        private set;
      }
      protected No primeiro
      {
        get;
        private set;
      }
      protected No ultimo
      {
        get;
        private set;
      }

      public int total
      {
        get;
        private set;
      }
      public bool vazio
      {
        get { return total == 0; }
      }
      public bool cheio
      {
        get { return total == colecao.total; }
      }
      public bool somenteLeitura
      {
        get { return true; }
      }
      #endregion

      #region Construtor
      public ColecaoArborea(ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo)
      {
        this.colecao  = colecao;
        this.primeiro = primeiro;
        this.ultimo   = ultimo;

        if ((primeiro == null) || (ultimo == null))
          total = 0;
        else if (primeiro == ultimo)
          total = 1;
        else if (colecao.comparador.Compare(primeiro.chave, ultimo.chave) <= 0)
          total = colecao.indexarNo(ultimo) - colecao.indexarNo(primeiro) + 1;
        else
          total = colecao.indexarNo(primeiro) - colecao.indexarNo(ultimo) + 1;
      }
      #endregion

      #region Métodos
      public string toString()
      {
        return ExtensorPadrao.toString(this);
      }

      public T[] toArray()
      {
        return ExtensorPadrao.toArray(this);
      }

      public abstract IEnumerator<T> GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }
      #endregion
    }

    protected class ColecaoChaves : ColecaoArborea<TChave>
    {
      #region Construtor
      public ColecaoChaves(ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo) : base(colecao, primeiro, ultimo)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<TChave> GetEnumerator()
      {
        return colecao.obterEnumeradorChaves(primeiro, ultimo);
      }
      #endregion
    }

    protected class ColecaoValores : ColecaoArborea<TValor>
    {
      #region Construtor
      public ColecaoValores(ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo) : base(colecao, primeiro, ultimo)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<TValor> GetEnumerator()
      {
        return colecao.obterEnumeradorValores(primeiro, ultimo);
      }
      #endregion
    }

    protected class ColecaoPares : ColecaoArborea<KeyValuePair<TChave, TValor>>
    {
      #region Construtor
      public ColecaoPares(ArvoreRubronegra<TChave, TValor> colecao, No primeiro, No ultimo) : base(colecao, primeiro, ultimo)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
      {
        return colecao.obterEnumeradorPares(primeiro, ultimo);
      }
      #endregion
    }
    #endregion
  }
}