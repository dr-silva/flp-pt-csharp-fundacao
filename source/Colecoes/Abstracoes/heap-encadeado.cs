﻿/*******************************************************************************
 * 
 * Arquivo  : heap-vetorial.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-30 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções heap de árvore binária.
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class HeapEncadeado<T> : IEnumeravel<T>
  {
    #region Atributos
    private readonly IComparador<T> comparador;
    #endregion

    #region Propriedades
    protected No primeiro
    {
      get;
      private set;
    }
    protected No ultimo
    {
      get;
      private set;
    }
    public int  total
    {
      get;
      private set;
    }
    public bool vazio
    {
      get { return primeiro == null; }
    }
    public bool cheio
    {
      get { return total == int.MaxValue; }
    }

    public abstract bool somenteLeitura { get; } 
    #endregion

    #region Construtores
    public HeapEncadeado()
    {
      comparador = FabricaComparador.criar<T>();
    }

    public HeapEncadeado(FuncaoComparacao<T> comparacao)
    {
      if (comparacao == null)
        throw Excecoes.ArgumentoNulo(nameof(comparacao));

      comparador = FabricaComparador.criar(comparacao);
    }

    public HeapEncadeado(IComparador<T> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Assistentes
    private void trocar(No a, No b)
    {
      if ((a != null) && (b != null))
      {
        T temp = a.dado;
        a.dado = b.dado;
        b.dado = temp;
      }
    }

    private void emergir(No no)
    {
      while (no != primeiro)
        if (eOrdenavel(no.ancestral, no))
        {
          trocar(no.ancestral, no);
          no = no.ancestral;
        }
        else
          break;
    }

    private void imergir(No no)
    {
      No prioritario = null;

      while (no != ultimo)
      {
        prioritario = ((no.esquerda != null) && eOrdenavel(no, no.esquerda)) ? no.esquerda : no;

        if ((no.direita != null) && eOrdenavel(prioritario, no.direita))
          prioritario = no.direita;

        if (prioritario != no)
        {
          trocar(prioritario, no);
          no = prioritario;
        }
        else
          break;
      }
    }

    protected abstract bool eOrdenavel(int comparacao);

    protected bool eOrdenavel(No esquerda, No direita)
    {
      return eOrdenavel(comparador.Compare(esquerda.dado, direita.dado));
    }

    protected No buscarNo(int indice)
    {
      No no = null;

      if (indice <= 1)
        no = primeiro;
      else
      {
        no = buscarNo(indice / 2);
        no = (indice % 2 == 0) ? no.esquerda : no.direita;
      }

      return no;
    }

    protected void inserir(T valor)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      No no  = new No(valor),
         pai = null;
      ultimo = no;
      total  = total + 1;

      if (primeiro == null)
        primeiro = no;
      else
      {
        pai = buscarNo(total / 2);

        if (total % 2 == 0)
          pai.esquerda = no;
        else
          pai.direita = no;

        no.ancestral = pai;
        emergir(no);
      }
    }

    protected T excluir(int indice)
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      No no    = buscarNo(indice);
      T  valor = no.dado;

      trocar(no, ultimo);
      if (ultimo.ancestral != null)
      {
        if (ultimo.ancestral.esquerda == ultimo)
          ultimo.ancestral.esquerda = null;
        else
          ultimo.ancestral.direita = null;
      }
      else
        primeiro = null;

      ultimo = buscarNo(--total);
      imergir(no);

      return valor;
    }

    protected void excluirTodos()
    {
      primeiro = ultimo = null;
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<T> GetEnumerator()
    {
      if (primeiro == null)
        yield break;

      No atual = obterMinimo(primeiro);

      while (atual != null)
      {
        yield return atual.dado;

        No proximo = atual.ancestral;

        if ((proximo?.esquerda == atual) && (proximo?.direita != null))
          atual = obterMinimo(proximo.direita);
        else
          atual = proximo;
      }
    }

    private No obterMinimo(No no)
    {
      while (no.esquerda != null)
        no = no.esquerda;

      return no;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Nó
    protected class No
    {
      #region Propriedades
      public T  dado      { get; set; }
      public No direita   { get; set; }
      public No esquerda  { get; set; }
      public No ancestral { get; set; }
      #endregion

      #region Construtores
      public No(T dado, No ancestral, No esquerda, No direita)
      {
        this.dado      = dado;
        this.direita   = direita;
        this.esquerda  = esquerda;
        this.ancestral = ancestral;
      }

      public No(T dado) : this(dado, null, null, null)
      {
      }
      #endregion
    }
    #endregion
  }
}