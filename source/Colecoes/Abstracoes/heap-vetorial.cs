﻿/*******************************************************************************
 * 
 * Arquivo  : heap-vetorial.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-29 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções heap binários vetoriais.
 * 
 *******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class HeapVetorial<T> : IEnumeravel<T>
  {
    #region Atributos
    private readonly int            capacidade;
    private readonly T[]            itens;
    private readonly IComparador<T> comparador;
    #endregion

    #region Propriedades
    protected T this[int indice]
    {
      get
      {
        if ((indice < 0) || (indice >= capacidade))
          throw Excecoes.IndiceForaLimites(nameof(indice), 0, capacidade - 1);

        return itens[indice];
      }
    }

    public int  total
    {
      get;
      private set;
    }
    public bool vazio
    {
      get { return total == 0; }
    }
    public bool cheio
    {
      get { return total == capacidade; }
    }

    public abstract bool somenteLeitura { get; } 
    #endregion

    #region Construtores
    public HeapVetorial(int capacidade)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      this.capacidade = capacidade;
      this.itens      = new T[capacidade];
      this.comparador = FabricaComparador.criar<T>();
    }

    public HeapVetorial(int capacidade, FuncaoComparacao<T> comparacao)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      if (comparacao == null)
        throw Excecoes.ArgumentoNulo(nameof(comparacao));

      this.capacidade = capacidade;
      this.itens      = new T[capacidade];
      this.comparador = FabricaComparador.criar<T>(comparacao);
    }

    public HeapVetorial(int capacidade, IComparador<T> comparador)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      this.capacidade = capacidade;
      this.itens      = new T[capacidade];
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Enumerável
    public string toString()
    {
      return ExtensorPadrao.toString(this);
    }

    public T[] toArray()
    {
      return ExtensorPadrao.toArray(this);
    }

    public IEnumerator<T> GetEnumerator()
    {
      for (int i = 0; i < total; i++)
        yield return itens[i];
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region Assistentes
    private void trocar(int i, int j)
    {
      if (i != j)
      {
        T temp   = itens[i];
        itens[i] = itens[j];
        itens[j] = temp;
      }
    }

    private void emergir(int indice)
    {
      int ancestral;

      while (indice > 0)
      {
        ancestral  = (indice + 1) / 2 - 1;
        if (eOrdenavel(ancestral, indice))
        {
          trocar(ancestral, indice);
          indice = ancestral;
        }
        else
          break;
      }
    }

    private void imergir(int indice)
    {
      int direita,
          esquerda,
          prioridade;

      while (indice < total)
      {
        direita    = 2 * (indice + 1);
        esquerda   = direita - 1;
        prioridade = ((esquerda < total) && eOrdenavel(indice, esquerda)) ? esquerda : indice;

        if ((direita < total) && eOrdenavel(prioridade, direita))
          prioridade = direita;

        if (prioridade != indice)
        {
          trocar(prioridade, indice);
          indice = prioridade;
        }
        else
          break;
      }
    }

    protected abstract bool eOrdenavel(int comparacao);

    protected bool eOrdenavel(int esquerda, int direita)
    {
      return eOrdenavel(comparador.Compare(itens[esquerda], itens[direita]));
    }

    protected void inserir(T valor)
    {
      if (cheio)
        throw Excecoes.ColecaoCheia();

      itens[total] = valor;
      emergir(total++);
    }

    protected T excluir(int indice)
    {
      if (vazio)
        throw Excecoes.ColecaoVazia();

      trocar(indice, --total);

      T valor = itens[total];

      imergir(indice);

      return valor;
    }

    protected void excluirTodos()
    {
      total = 0;
    }
    #endregion
  }
}