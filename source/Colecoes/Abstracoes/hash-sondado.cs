﻿/*******************************************************************************
 * 
 * Arquivo  : hash-sondado.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-31 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define a classe base das coleções por hash sondado.
 * 
 *******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;
using FractalLotus.Fundacao.Extensores;

namespace FractalLotus.Fundacao.Colecoes.Abstracoes
{
  public abstract class HashSondado<TChave, TValor>
  {
    #region Atributos
    private static readonly int MAX_VALUE = 3 * 2 << 29;

    private readonly IComparadorIgualdade<TChave> comparador;

    private Item[] itens;
    private byte   lgCapacidade;
    private int    capacidade,
                   primo;
    #endregion

    #region Propriedades
    protected Item excluido
    {
      get;
      private set;
    }

    public int total
    {
      get;
      private set;
    }

    public bool vazio
    {
      get { return total == 0; }
    }

    public bool cheio
    {
      get { return total == MAX_VALUE; }
    }
    #endregion

    #region Construtores
    public HashSondado()
    {
      comparador = FabricaComparadorIgualdade.criar<TChave>();
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar(4);
    }

    public HashSondado(FuncaoIgualdade<TChave> igualdade)
    {
      comparador = FabricaComparadorIgualdade.criar(igualdade);
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar(4);
    }

    public HashSondado(FuncaoProjecao<TChave, int> hash)
    {
      comparador = FabricaComparadorIgualdade.criar(hash);
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar(4);
    }

    public HashSondado(FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash)
    {
      comparador = FabricaComparadorIgualdade.criar(igualdade, hash);
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar(4);
    }

    public HashSondado(IComparadorIgualdade<TChave> comparador)
    {
      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
      this.excluido   = new Item(default(TChave), default(TValor));

      redimensionar(4);
    }

    public HashSondado(int capacidade)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar<TChave>();
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar((byte)Math.Ceiling(Math.Log(4 * capacidade / 3, 2)));
    }

    public HashSondado(int capacidade, FuncaoIgualdade<TChave> igualdade)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar(igualdade);
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar((byte)Math.Ceiling(Math.Log(4 * capacidade / 3, 2)));
    }

    public HashSondado(int capacidade, FuncaoProjecao<TChave, int> hash)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar(hash);
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar((byte)Math.Ceiling(Math.Log(4 * capacidade / 3, 2)));
    }

    public HashSondado(int capacidade, FuncaoIgualdade<TChave> igualdade, FuncaoProjecao<TChave, int> hash)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      comparador = FabricaComparadorIgualdade.criar(igualdade, hash);
      excluido   = new Item(default(TChave), default(TValor));

      redimensionar((byte)Math.Ceiling(Math.Log(4 * capacidade / 3, 2)));
    }

    public HashSondado(int capacidade, IComparadorIgualdade<TChave> comparador)
    {
      if (capacidade <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(capacidade));

      this.comparador = comparador ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
      this.excluido   = new Item(default(TChave), default(TValor));

      redimensionar((byte)Math.Ceiling(Math.Log(4 * capacidade / 3, 2)));
    }
    #endregion

    #region Assistentes
    private int calcularHash(int chave, int indice)
    {
      int novaChave = chave & 0x7FFFFFFF,
          parcela   = novaChave % capacidade,
          fator     = 1 + (novaChave % primo);

      return (parcela + indice * (fator % 2 == 0 ? fator + 1 : fator)) % capacidade;
    }
    #endregion

    #region Interface
    protected void redimensionar(byte dimensao)
    {
      byte valor = (dimensao < 04) ? (byte)04 :
                   (dimensao > 31) ? (byte)31 : dimensao;

      if (valor == lgCapacidade)
        return;

      Item[] antigos          = itens;
      int    capacidadeAntiga = capacidade;
             lgCapacidade     = valor;
             capacidade       = 1 << valor;
             primo            = Primos.valor(valor);
             itens            = new Item[capacidade];

      if (total == 0)
        return;

      total = 0;
      for (int i = 0; i < capacidadeAntiga; i++)
        if ((antigos[i] != null) && (antigos[i] != excluido))
          inserirItem(antigos[i]);
    }

    protected Item buscarItem(TChave chave)
    {
      int  novaChave = comparador.GetHashCode(chave),
           indice    = 0,
           hash      = calcularHash(novaChave, indice);
      Item item      = null;

      while ((itens[hash] != null) && (indice < capacidade))
      {
        if ((itens[hash] != excluido) && comparador.Equals(chave, itens[hash].chave))
        {
          item = itens[hash];
          break;
        }

        hash = calcularHash(novaChave, ++indice);
      }
      return item;
    }

    protected void inserirItem(Item item)
    {
      int novaChave = comparador.GetHashCode(item.chave),
          indice    = 0;

      do
      {
        int hash = calcularHash(novaChave, indice++);

        if ((itens[hash] == null) || (itens[hash] == excluido))
        {
          item.hash   = hash;
          itens[hash] = item;
          total++;
          break;
        }
      } while (indice < capacidade);
    }

    protected void inserirItem(TChave chave, TValor valor)
    {
      if (total > 3 * capacidade / 4)
        redimensionar((byte)(lgCapacidade + 1));

      inserirItem( new Item(chave, valor) );
    }

    protected void extrairItem(Item item)
    {
      if ((item != null) && (item != excluido))
      {
        itens[item.hash] = excluido;
        total--;
      }
    }

    protected void excluirItem(Item item)
    {
      extrairItem(item);

      if (total < capacidade / 8)
          redimensionar((byte)(lgCapacidade - 1));
    }

    protected void excluirTodos()
    {
      itens = new Item[capacidade];
      total = 0;
    }
    #endregion

    #region Item
    protected class Item
    {
      #region Propriedades
      public TChave chave { get; set; }
      public TValor valor { get; set; }
      public int    hash  { get; set; }
      #endregion

      #region Construtores
      public Item(TChave chave, TValor valor, int hash)
      {
        this.valor = valor;
        this.chave = chave;
        this.hash  = hash;
      }

      public Item(TChave chave, TValor valor) : this(chave, valor, -1)
      {
      }
      #endregion
    }
    #endregion

    #region Enumeradores
    private IEnumerable<Item> obterItens()
    {
      for (int i = 0; i < capacidade; i++)
        if ((itens[i] != null) && (itens[i] != excluido))
          yield return itens[i];
    }

    protected IEnumerator<TChave> obterEnumeradorChaves()
    {
      foreach (var item in obterItens())
        yield return item.chave;
    }

    protected IEnumerator<TValor> obterEnumeradorValores()
    {
      foreach (var item in obterItens())
        yield return item.valor;
    }

    protected IEnumerator<KeyValuePair<TChave, TValor>> obterEnumeradorPares()
    {
      foreach (var item in obterItens())
        yield return new KeyValuePair<TChave, TValor>(item.chave, item.valor);
    }
    #endregion

    #region Subcoleções
    protected abstract class ColecaoHash<T> : IEnumeravel<T>
    {
      #region Propriedades
      protected HashSondado<TChave, TValor> colecao
      {
        get;
        private set;
      }

      public int  total          { get { return colecao.total; } }
      public bool vazio          { get { return colecao.vazio; } }
      public bool cheio          { get { return colecao.cheio; } }
      public bool somenteLeitura { get { return true; } }
      #endregion

      #region Construtor
      public ColecaoHash(HashSondado<TChave, TValor> colecao)
      {
        this.colecao = colecao ?? throw Excecoes.ArgumentoNulo(nameof(colecao));
      }
      #endregion

      #region Métodos
      public string toString()
      {
        return ExtensorPadrao.toString(this);
      }

      public T[] toArray()
      {
        return ExtensorPadrao.toArray(this);
      }

      public abstract IEnumerator<T> GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }
      #endregion
    }

    protected class ColecaoChaves : ColecaoHash<TChave>
    {
      #region Construtor
      public ColecaoChaves(HashSondado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<TChave> GetEnumerator()
      {
        return colecao.obterEnumeradorChaves();
      }
      #endregion
    }

    protected class ColecaoValores : ColecaoHash<TValor>
    {
      #region Construtor
      public ColecaoValores(HashSondado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<TValor> GetEnumerator()
      {
        return colecao.obterEnumeradorValores();
      }
      #endregion
    }

    protected class ColecaoPares : ColecaoHash<KeyValuePair<TChave, TValor>>
    {
      #region Construtor
      public ColecaoPares(HashSondado<TChave, TValor> colecao) : base(colecao)
      {
      }
      #endregion

      #region Métodos
      public override IEnumerator<KeyValuePair<TChave, TValor>> GetEnumerator()
      {
        return colecao.obterEnumeradorPares();
      }
      #endregion
    }
    #endregion
  }
}