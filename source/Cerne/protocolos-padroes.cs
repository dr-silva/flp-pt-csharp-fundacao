﻿/*******************************************************************************
 * 
 * Arquivo  : protocolos-padroes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as implementações mais comuns para os protocolos de funções
 *            definidos no arquivo protocolos-funcoes.cs. São estas implementa-
 *            ções que são instanciadas pelas fábricas do arqruivo fabricas-
 *            -funcoes.cs.
 * 
 *******************************************************************************/
using System;
using System.Collections.Generic;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Cerne
{
  /* Classe para ser usada como instancia da interface IComparadorIgualdade.
   */
  class ComparadorIgualdadePadrao<T> : IComparadorIgualdade<T>
  {
    #region Atributos
    private IEqualityComparer<T>   fPadrao;
    private FuncaoIgualdade<T>     fIgualdade;
    private FuncaoProjecao<T, int> fHash;
    #endregion

    #region Construtores
    public ComparadorIgualdadePadrao
    (
      FuncaoIgualdade<T>     igualdade,
      FuncaoProjecao<T, int> hash
    )
    {
      fPadrao    = EqualityComparer<T>.Default;
      fIgualdade = igualdade;
      fHash      = hash;
    }
    #endregion

    #region Interface
    public bool Equals(T x, T y)
    {
      return (fIgualdade == null) ? fPadrao.Equals(x, y) : fIgualdade(x, y);
    }

    public int GetHashCode(T obj)
    {
      return (fHash == null) ? fPadrao.GetHashCode(obj) : fHash(obj);
    }
    #endregion
  }

  /* Classe para ser usada como instancia da interface IComparador.
   */
  class ComparadorPadrao<T> : IComparador<T>
  {
    #region Atributos
    private IComparer<T>        fPadrao;
    private FuncaoComparacao<T> fComparacao;
    #endregion

    #region Construtores
    public ComparadorPadrao(FuncaoComparacao<T> comparacao)
    {
      fPadrao     = Comparer<T>.Default;
      fComparacao = comparacao;
    }
    #endregion

    #region Interface
    public int Compare(T x, T y)
    {
      return (fComparacao == null) ? fPadrao.Compare(x, y) : fComparacao(x, y);
    }
    #endregion
  }

  /* Classe para ser usada como instancia da interface IPredicado.
   */
  class PredicadoPadrao<T> : IPredicado<T>
  {
    #region Atributos
    private FuncaoPredicado<T> fFuncao;
    #endregion

    #region Construtores
    public PredicadoPadrao(FuncaoPredicado<T> funcao)
    {
      fFuncao = funcao ?? throw Excecoes.ArgumentoNulo(nameof(funcao));
    }
    #endregion

    #region Interface
    public bool validar(T valor)
    {
      return fFuncao(valor);
    }
    #endregion
  }

  /* Classe para ser usada como instancia da interface IRedutor.
   */
  class RedutorPadrao<TAcumulador, TFonte> : IRedutor<TAcumulador, TFonte>
  {
    #region Atributos
    private FuncaoReducao<TAcumulador, TFonte> fFuncao;
    #endregion

    #region Construtores
    public RedutorPadrao(FuncaoReducao<TAcumulador, TFonte> funcao)
    {
      fFuncao = funcao ?? throw Excecoes.ArgumentoNulo(nameof(funcao));
    }
    #endregion

    #region Interface
    public TAcumulador reduzir(TAcumulador valor, TFonte item)
    {
      return fFuncao(valor, item);
    }
    #endregion
  }

  /* Classe para ser usada como instancia da interface IProjetor.
   */
  class ProjetorPadrao<T, TResult> : IProjetor<T, TResult>
  {
    #region Atributos
    private FuncaoProjecao<T, TResult> fFuncao;
    #endregion

    #region Construtores
    public ProjetorPadrao(FuncaoProjecao<T, TResult> funcao)
    {
      fFuncao = funcao ?? throw Excecoes.ArgumentoNulo(nameof(funcao));
    }
    #endregion

    #region Interface
    public TResult projetar(T valor)
    {
      return fFuncao(valor);
    }
    #endregion
  }

  /* Classe para ser usada como instancia da interface ICorrelacionamento.
   */
  class CorrelacionamentoPadrao<TEsquerda, TDireita, TChave, TResult> : ICorrelacionamento<TEsquerda, TDireita, TResult>
  {
    #region Atributos
    private FuncaoProjecao<TEsquerda, TChave>  fProjecaoEsquerda;
    private FuncaoProjecao<TDireita , TChave>  fProjecaoDireita;
    private Func<TEsquerda, TDireita, TResult> fFuncaoResultante;
    private IComparadorIgualdade<TChave>       fComparador;
    private TResult                            fCorrelacao;
    #endregion

    #region Construtores
    public CorrelacionamentoPadrao
    (
      FuncaoProjecao<TEsquerda, TChave>  projecaoEsquerda,
      FuncaoProjecao<TDireita , TChave>  projecaoDireita,
      Func<TEsquerda, TDireita, TResult> funcaoResultante,
      IComparadorIgualdade<TChave>       comparador
    )
    {
      fProjecaoEsquerda = projecaoEsquerda ?? throw Excecoes.ArgumentoNulo(nameof(projecaoEsquerda));
      fProjecaoDireita  = projecaoDireita  ?? throw Excecoes.ArgumentoNulo(nameof(projecaoDireita));
      fFuncaoResultante = funcaoResultante ?? throw Excecoes.ArgumentoNulo(nameof(funcaoResultante));
      fComparador       = comparador       ?? throw Excecoes.ArgumentoNulo(nameof(comparador));
    }
    #endregion

    #region Interface
    public bool correlacionar(TEsquerda esquerda, TDireita direita)
    {
      TChave chaveEsquerda = fProjecaoEsquerda(esquerda),
             chaveDireita  = fProjecaoDireita (direita);

      bool result = fComparador.Equals(chaveEsquerda, chaveDireita);
      fCorrelacao = (result) ? fFuncaoResultante(esquerda, direita) : default(TResult);

      return result;
    }

    public TResult correlacao
    {
      get { return fCorrelacao; }
    }
    #endregion
  }
}
