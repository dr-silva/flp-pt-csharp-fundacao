﻿/*******************************************************************************
 * 
 * Arquivo  : protocolos-funcoes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Os protocolos de funções são interfaces para cumprir objetivos
 *            similares aos objetivos das funções anônimas do arquivo funcoes.cs.
 * 
 *******************************************************************************/
using System.Collections.Generic;

namespace FractalLotus.Fundacao.Cerne
{
  /* Define um protocolo que deve ser implementado para classes que necessitem
   * de suporte à comparação de igualde e/ou precisem do código hash.
   */
  public interface IComparadorIgualdade<T> : IEqualityComparer<T>
  {
  }

  /* Define um protocolo que deve ser implementado para classes que necessitem
   * de uma relação de ordem.
   */
  public interface IComparador<T> : IComparer<T>
  {
  }

  /* Define uma interface que deve ser implementada quando for necessário algum
   * um predicado para uma classe.
   */
  public interface IPredicado<T>
  {
    bool validar(T valor);
  }

  /* Define uma interface que deve ser implementada quando for preciso fazer
   * uma agregação dalguma coleção do tipo genérico TFonte. O tipo genérico
   * TAcumulador representa o tipo de retorno da agregação.
   */
  public interface IRedutor<TAcumulador, TFonte>
  {
    TAcumulador reduzir(TAcumulador valor, TFonte item);
  }

  /* Define um protocolo que deve ser implementado para classes que queiram
   * realizar algum tipo de "conversão" feita do tipo genérico T para o tipo
   * genérico TResult.
   */
  public interface IProjetor<T, TResult>
  {
    TResult projetar(T valor);
  }

  /* Define uma interface que deve ser implementada para classes que precisem
   * estabelecer um uma relação entre dois valores de tipos genéricos distintos
   * (TEsquerda e TDireita). Caso esta relação entre os dois valores se confir-
   * me, eles servirão de base para criar um valor resultante.
  */
  public interface ICorrelacionamento<TEsquerda, TDireita, TResult>
  {
    bool correlacionar(TEsquerda esquerda, TDireita direita);

    TResult correlacao { get; }
  }

  /* Define um protocolo que deve ser implementado por classes que queiram re-
   * organizar um array.
   */
  public interface IOrdenacao<T>
  {
    void ordenar(T[] itens);
  }

  /* Define um protocolo que deve ser implementado por classes que queiram ave
   * riguar se um array está em uma determinada ordem.
   */
  public interface IValidadorOrdenacao<T>
  {
    bool validar(T[] itens);
  }

  /* Define um protocolo que deve ser implementado em coleções que tenham itens
   * que se relacionem ordenadamente entre si.
   */
  public interface IOrdenavel<TChave>
  {
    TChave minimo { get; }
    TChave maximo { get; }

    TChave piso(TChave chave);
    TChave teto(TChave chave);

    int    indexar   (TChave chave);
    TChave desindexar(int indice);

    int                 contar     (TChave inferior, TChave superior);
    IEnumeravel<TChave> obterChaves(TChave inferior, TChave superior);
  }
}
