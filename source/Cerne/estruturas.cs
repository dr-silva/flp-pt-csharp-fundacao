﻿/*******************************************************************************
 * 
 * Arquivo  : estruturas.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Declara as estruturas básicas da Fundação.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Cerne
{
  /* Esta é uma estrutura para armazenar as informações dum intervalo de tempo.
   */
  public struct Periodo : IComparable, IComparable<Periodo>, IEquatable<Periodo>
  {
    #region Atributos
    private TimeSpan fTempo;
    #endregion

    #region Construtores
    private Periodo(TimeSpan tempo)
    {
      fTempo = tempo;
    }

    public Periodo
    (
      int dias, short horas, short minutos, short segundos, short milissegundos
    )
    {
      fTempo = new TimeSpan(dias, horas, minutos, segundos, milissegundos);
    }

    public Periodo(DateTime inicio, DateTime fim)
    {
      fTempo = fim.Subtract(inicio);
    }
    #endregion

    #region Consultas
    public override bool Equals(object obj)
    {
      return (obj is Periodo) ? CompareTo((Periodo)obj) == 0 : false;
    }

    public bool Equals(Periodo valor)
    {
      return CompareTo(valor) == 0;
    }

    public int CompareTo(object obj)
    {
      return (obj is Periodo) ? CompareTo((Periodo)obj) : -1;
    }

    public int CompareTo(Periodo valor)
    {
      return fTempo.CompareTo(valor.fTempo);
    }

    public override int GetHashCode()
    {
      return fTempo.GetHashCode();
    }

    public override string ToString()
    {
      return string.Format
      (
        "{0}:{1:00}:{2:00}:{3:00}.{4:000}",
        dias, horas, minutos, segundos, milissegundos
      );
    }
    #endregion

    #region Assistentes
    public static bool eIgual(Periodo esquerda, Periodo direita)
    {
      return esquerda.Equals(direita);
    }

    public static int obterHash(Periodo valor)
    {
      return valor.GetHashCode();
    }

    public static int comparar(Periodo esquerda, Periodo direita)
    {
      return esquerda.CompareTo(direita);
    }

    public static string toString(Periodo valor)
    {
      return valor.ToString();
    }

    public static Periodo parse(string str)
    {
      Periodo periodo;

      if (!tryParse(str, out periodo))
        throw Excecoes.PeriodoInvalido(nameof(str));

      return periodo;
    }

    public static bool tryParse(string str, out Periodo valor)
    {
      bool result = false;
           valor  = default;

      try
      {
        string[] valores = str.Split(':', ':', ':', '.');

        if (valores.Length == 5)
        {
          valor = new Periodo
          (
            int  .Parse(valores[0]),
            short.Parse(valores[1]),
            short.Parse(valores[2]),
            short.Parse(valores[3]),
            short.Parse(valores[4])
          );

          result = true;
        }
      }
      catch
      {
        result = false;
      }

      return result;
    }

    public static long valorar(Periodo periodo)
    {
      return periodo.fTempo.Ticks;
    }
    #endregion

    #region Operadores
    public static Periodo operator +(Periodo periodo)
    {
      return new Periodo(+periodo.fTempo);
    }

    public static Periodo operator -(Periodo periodo)
    {
      return new Periodo(-periodo.fTempo);
    }

    public static Periodo operator +(Periodo esquerda, Periodo direita)
    {
      return new Periodo(esquerda.fTempo + direita.fTempo);
    }

    public static Periodo operator -(Periodo esquerda, Periodo direita)
    {
      return new Periodo(esquerda.fTempo - direita.fTempo);
    }

    public static bool operator ==(Periodo esquerda, Periodo direita)
    {
      return esquerda.fTempo == direita.fTempo;
    }

    public static bool operator !=(Periodo esquerda, Periodo direita)
    {
      return esquerda.fTempo != direita.fTempo;
    }

    public static bool operator >(Periodo esquerda, Periodo direita)
    {
      return esquerda.fTempo > direita.fTempo;
    }

    public static bool operator <(Periodo esquerda, Periodo direita)
    {
      return esquerda.fTempo < direita.fTempo;
    }

    public static bool operator >=(Periodo esquerda, Periodo direita)
    {
      return esquerda.fTempo >= direita.fTempo;
    }

    public static bool operator <=(Periodo esquerda, Periodo direita)
    {
      return esquerda.fTempo <= direita.fTempo;
    }
    #endregion

    #region Propriedades
    public int dias
    {
      get { return fTempo.Days; }
    }
    public short horas
    {
      get { return (short)fTempo.Hours; }
    }
    public short minutos
    {
      get { return (short)fTempo.Minutes; }
    }
    public short segundos
    {
      get { return (short)fTempo.Seconds; }
    }
    public short milissegundos
    {
      get { return (short)fTempo.Milliseconds; }
    }
    #endregion
  }

  /* Esta classe tem o objetivo de contar o tempo decorrido entre o "agora" e a
   * inicialização, que pode ser o momento de sua instanciação ou a última vez
   * que foi reiniciado.
   */
  public class Temporizador
  {
    #region Atributos
    private DateTime fInicio;
    #endregion

    #region Construtores
    public Temporizador()
    {
      reiniciar();
    }
    #endregion

    #region Regras
    public void reiniciar()
    {
      fInicio = DateTime.Now;
    }

    public Periodo decorrer()
    {
      return new Periodo(fInicio, DateTime.Now);
    }

    public Periodo decorrer(bool reiniciar)
    {
      Periodo p = decorrer();

      if (reiniciar)
        this.reiniciar();

      return p;
    }
    #endregion
  }
}
