﻿/*******************************************************************************
 * 
 * Arquivo  : fabricas-funcoes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define algumas fábricas para instancias de maneira rápida os
 *            protocolos de funções definidos no arquivo protocolos-funcoes.cs.
 * 
 *******************************************************************************/
using System;

namespace FractalLotus.Fundacao.Cerne
{
  /// <summary>
  ///   Classe para criar instâncias da interface IComparadorIgualdade.
  /// </summary>
  public sealed class FabricaComparadorIgualdade
  {
    public static IComparadorIgualdade<T> criar<T>()
    {
      return new ComparadorIgualdadePadrao<T>(null, null);
    }

    public static IComparadorIgualdade<T> criar<T>
    (
      FuncaoIgualdade<T> igualdade
    )
    {
      return criar(igualdade, null);
    }

    public static IComparadorIgualdade<T> criar<T>
    (
      FuncaoProjecao<T, int> hash
    )
    {
      return criar(null, hash);
    }

    public static IComparadorIgualdade<T> criar<T>
    (
      FuncaoIgualdade<T>     igualdade,
      FuncaoProjecao<T, int> hash
    )
    {
      return new ComparadorIgualdadePadrao<T>(igualdade, hash);
    }
  }

  /* Classe para criar instancias da interface IComparador.
   */
  public sealed class FabricaComparador
  {
    public static IComparador<T> criar<T>()
    {
      return new ComparadorPadrao<T>(null);
    }

    public static IComparador<T> criar<T>(FuncaoComparacao<T> comparacao)
    {
      return new ComparadorPadrao<T>(comparacao);
    }
  }

  /* Classe para criar instancias da interface IPredicado.
   */
  public sealed class FabricaPredicado
  {
    public static IPredicado<T> criar<T>()
    {
      return null;
    }

    public static IPredicado<T> criar<T>(FuncaoPredicado<T> predicado)
    {
      return new PredicadoPadrao<T>(predicado);
    }
  }

  /* Classe para criar instancias da interface IRedutor.
   */
  public sealed class FabricaRedutor
  {
    public static IRedutor<TAcumulador, TFonte> criar<TAcumulador, TFonte>
    (
      FuncaoReducao<TAcumulador, TFonte> reducao
    )
    {
      return new RedutorPadrao<TAcumulador, TFonte>(reducao);
    }

    public static IRedutor<TFonte, TFonte> criar<TFonte>
    (
      FuncaoReducao<TFonte, TFonte> reducao
    )
    {
      return new RedutorPadrao<TFonte, TFonte>(reducao);
    }

    public static IRedutor<TFonte, TFonte> criarSoma<TFonte>()
    {
      return null;
    }

    public static IRedutor<TFonte, TFonte> criarMultiplicacao<TFonte>()
    {
      return null;
    }
  }

  /* Classe para criar instancias da interface IProjetor.
   */
  public sealed class FabricaProjetor
  {
    public static IProjetor<T, T> criar<T>()
    {
      return new ProjetorPadrao<T, T>(x => x);
    }

    public static IProjetor<T, TResult> criar<T, TResult>
    (
      FuncaoProjecao<T, TResult> projecao
    )
    {
      return new ProjetorPadrao<T, TResult>(projecao);
    }

    public static IProjetor<T, string> criarString<T>()
    {
      FuncaoProjecao<T, string> projecao = (x) =>
      {
        return string.Empty + x;
      };

      return criarString(projecao);
    }

    public static IProjetor<T, string> criarString<T>
    (
      FuncaoProjecao<T, string> projecao
    )
    {
      return new ProjetorPadrao<T, string>(projecao);
    }
  }

  /* Classe para criar instancias da interface ICorrelacionamento.
   */
  public sealed class FabricaCorrelacionamento
  {
    public static ICorrelacionamento<TFonte, TFonte, TResult> criar<TFonte, TChave, TResult>
    (
      FuncaoProjecao<TFonte, TChave> projecao,
      Func<TFonte, TFonte, TResult>  resultante
    )
    {
      return criar
      (
        projecao,
        resultante,
        FabricaComparadorIgualdade.criar<TChave>()
      );
    }

    public static ICorrelacionamento<TFonte, TFonte, TResult> criar<TFonte, TChave, TResult>
    (
      FuncaoProjecao<TFonte, TChave> projecao,
      Func<TFonte, TFonte, TResult>  resultante,
      FuncaoIgualdade<TChave>       igualdade
    )
    {
      return criar
      (
        projecao,
        resultante,
        FabricaComparadorIgualdade.criar(igualdade)
      );
    }

    public static ICorrelacionamento<TFonte, TFonte, TResult> criar<TFonte, TChave, TResult>
    (
      FuncaoProjecao<TFonte, TChave> projecao,
      Func<TFonte, TFonte, TResult>  resultante,
      IComparadorIgualdade<TChave>   comparador
    )
    {
      return new CorrelacionamentoPadrao<TFonte, TFonte, TChave, TResult>
      (
        projecao,
        projecao,
        resultante,
        comparador
      );
    }

    public static ICorrelacionamento<TEsquerda, TDireita, TResult> criar<TEsquerda, TDireita, TChave, TResult>
    (
      FuncaoProjecao<TEsquerda, TChave>  projecaoEsquerda,
      FuncaoProjecao<TDireita , TChave>  projecaoDireita,
      Func<TEsquerda, TDireita, TResult> resultante
    )
    {
      return criar
      (
        projecaoEsquerda,
        projecaoDireita,
        resultante,
        FabricaComparadorIgualdade.criar<TChave>()
      );
    }

    public static ICorrelacionamento<TEsquerda, TDireita, TResult> criar<TEsquerda, TDireita, TChave, TResult>
    (
      FuncaoProjecao<TEsquerda, TChave>  projecaoEsquerda,
      FuncaoProjecao<TDireita , TChave>  projecaoDireita,
      Func<TEsquerda, TDireita, TResult> resultante,
      FuncaoIgualdade<TChave>            igualdade
    )
    {
      return criar
      (
        projecaoEsquerda,
        projecaoDireita,
        resultante,
        FabricaComparadorIgualdade.criar(igualdade)
      );
    }

    public static ICorrelacionamento<TEsquerda, TDireita, TResult> criar<TEsquerda, TDireita, TChave, TResult>
    (
      FuncaoProjecao<TEsquerda, TChave>  projecaoEsquerda,
      FuncaoProjecao<TDireita , TChave>  projecaoDireita,
      Func<TEsquerda, TDireita, TResult> resultante,
      IComparadorIgualdade<TChave>       comparador
    )
    {
      return criar
      (
        projecaoEsquerda,
        projecaoDireita,
        resultante,
        comparador
      );
    }
  }
}
