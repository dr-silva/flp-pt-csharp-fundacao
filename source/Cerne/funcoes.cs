﻿/*******************************************************************************
 * 
 * Arquivo  : funcoes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Aqui são definidas todas as funções anônimas do projeto.
 * 
 *******************************************************************************/

namespace FractalLotus.Fundacao.Cerne
{
  #region Comparativas
  /* Define uma função anônima para verificar a igualdade entre dois valores do
   * tipo genérico T. Espera-se desta função que ela contenha as mesmas propri-
   * edades da igualdade matemática, ou seja, ela deve ser reflexiva, simetrica
   * e transitiva.
   */
  public delegate bool FuncaoIgualdade<T>(T esquerda, T direita);

  /* Define um tipo de função anônima usada para criar uma relação de ordem em
   * um tipo genérico T. O retorno da função é um número inteiro que pode ter a
   * seguinte interpretação, para a, b: T:
   *   - FuncaoComparacao(a, b) = 0 se, e somente se, a = b;
   *   - FuncaoComparacao(a, b) < 0 se, e somente se, a < b;
   *   - FuncaoComparacao(a, b) > 0 se, e somente se, a > b.
   */
  public delegate int FuncaoComparacao<T>(T esquerda, T direita);

  /* Define um tipo de função anônima sobre um tipo genérico T que deve validar
   * se um valor de entrada respeita a um conjunto de critérios.
   */
  public delegate bool FuncaoPredicado<T>(T valor);
  #endregion

  #region Redutivas
  /* Define uma função anônima usada como uma função agregadora que será apli-
   * cada àlguma coleção. O tipo genérico TAcumulador deve corresponder ao re-
   * sultado agregado e o tipo genérico TFonte, ao tipo do elemento da coleção.
   */
  public delegate TAcumulador FuncaoReducao<TAcumulador, TFonte>
  (
    TAcumulador valor, TFonte item
  );
  #endregion

  #region Projetivas
  /* Define um tipo de função anônima usada para dar uma perspectiva diferente,
   * TResult, ao valor do tipo genérico T.
   */
  public delegate TResult FuncaoProjecao<T, TResult>(T valor);
  #endregion
}
