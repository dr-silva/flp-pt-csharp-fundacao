﻿/*******************************************************************************
 * 
 * Arquivo  : protocolos-enumeraveis.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Neste arquivo estão definidas as interfaces usadas por coleções
 *            em todos os projetos da Fractal Lotus.
 * 
 *******************************************************************************/
using System.Collections.Generic;

namespace FractalLotus.Fundacao.Cerne
{
  /* Define uma interface padrão ao projeto para ser alvo da declaração do laço
   * for in.
   */
  public interface IEnumeravel<out T> : IEnumerable<T>
  {
    string toString();
    T[]    toArray();

    int  total          { get; }
    bool vazio          { get; }
    bool cheio          { get; }
    bool somenteLeitura { get; }
  }

  /* Define uma interface padrão ao projeto para criar um protocolo de agrupa-
   * mento, usado principalmente pela extensão TExtensorEnumeravelAgrupamento.
   */
  public interface IAgrupavel<TChave, TValor> : IEnumeravel<TValor>
  {
    TChave chave { get; }
  }

  /* Define uma interface para coleções que se baseiam no princípio último a
   * entrar deve ser o primeiro a sair (Last In, First Out: LIFO).
   */
  public interface IEmpilhavel<T> : IEnumeravel<T>
  {
    void empilhar(T item);
    T    desempilhar();
    T    espiar();
    void limpar();
  }

  /* Define uma interface para coleções que se baseiam no princípio primeiro a
   * entrar deve ser o primeiro a sair (First In, First Out: FIFO).
   */
  public interface IEnfileiravel<T> : IEnumeravel<T>
  {
    void enfileirar(T item);
    T    desenfileirar();
    T    espiar();
    void limpar();
  }

  /* Define uma interface para coleções em que não seja necessário suporte à
   * remoção de itens. Seu propósito é fornecer uma coleção que possa guardar
   * itens e, então, iterá-los, somente, sem se preocupar com outras regras.
   */
  public interface IGuardavel<T> : IEnumeravel<T>
  {
    void guardar(T item);
    void guardar(IEnumerable<T> itens);
  }

  /* Define uma interface para coleções que possam ser acessadas por índices.
   */
  public interface IListavel<T> : IEnumeravel<T>
  {
    T primeiro { get; }
    T ultimo   { get; }

    T this[int indice] { get; set; }

    int adicionar(T item);
    int adicionar(IEnumerable<T> itens);

    void inserir(int indice, T item);
    void inserir(int indice, IEnumerable<T> itens);

    bool remover(int indice);
    bool contem (int indice);

    void limpar();

    IEnumeravel<T> obterItens(int inferior, int superior);
  }

  /* Define uma interface para coleções cujos itens não possam ser duplicados.
   * A interface é interpretada também como um conjunto no sentido matemático.
   */
  public interface IDistinguivel<TChave> : IEnumeravel<TChave>
  {
    bool adicionar(TChave chave);
    bool remover  (TChave chave);
    bool contem   (TChave chave);

    void limpar();

    void unir          (IEnumeravel<TChave> colecao);
    void excetuar      (IEnumeravel<TChave> colecao);
    void interseccionar(IEnumeravel<TChave> colecao);
  }

  /* Define uma interface para uma estrutura de dados de pares chaves-valores
   * que suporte três operações: inserir um novo par na estrutura; buscar por
   * um valor associado a uma determinada chave; e remover uma chave e seu va-
   * lor associado.
   */
  public interface IDicionarizavel<TChave, TValor> : IEnumeravel<KeyValuePair<TChave, TValor>>
  {
    IEnumeravel<TChave> chaves  { get; }
    IEnumeravel<TValor> valores { get; }

    TValor this[TChave chave] { get; set; }

    void adicionar(TChave chave, TValor valor);
    void adicionar(IDicionarizavel<TChave, TValor> dicionario);

    bool remover(TChave chave);
    bool contem (TChave chave);

    void limpar();

    bool tentarObter(TChave chave, out TValor valor);
  }
}
