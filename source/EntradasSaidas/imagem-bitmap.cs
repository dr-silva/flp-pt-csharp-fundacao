﻿/*******************************************************************************
 *
 * Arquivo  : imagem-bitmap.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-06 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Classe para geração de imagens matriciais.
 *
 ******************************************************************************/
using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace FractalLotus.Fundacao.EntradasSaidas
{
  public class ImagemBitmap
  {
    #region Atributos
    private readonly Bitmap fBitmap;
    private          bool   fSuperior;
    #endregion

    #region Construtores & Destruidores
    private ImagemBitmap(Bitmap bitmap, bool superior)
    {
      fBitmap   = bitmap;
      fSuperior = superior;
    }

    public ImagemBitmap(int altura, int largura)
    {
      fBitmap   = new Bitmap(largura, altura, PixelFormat.Format32bppArgb);
      fSuperior = true;
    }

    public ImagemBitmap(string arquivo)
    {
      fBitmap   = new Bitmap(arquivo);
      fSuperior = true;
    }

    public ImagemBitmap(Stream stream)
    {
      fBitmap   = new Bitmap(stream);
      fSuperior = true;
    }
    #endregion

    #region Interface
    public void definir(int linha, int coluna, float r, float g, float b)
    {
      definir(linha, coluna, 1, r, g, b);
    }

    public void definir(int linha, int coluna, float alfa, float r, float g, float b)
    {
      if (alfa < 0 || alfa > 1)
        throw new ArgumentOutOfRangeException(nameof(alfa));
      if (r < 0 || r > 1)
        throw new ArgumentOutOfRangeException(nameof(r));
      if (g < 0 || g > 1)
        throw new ArgumentOutOfRangeException(nameof(g));
      if (b < 0 || b > 1)
        throw new ArgumentOutOfRangeException(nameof(b));

      byte A = (byte)Math.Min(255, alfa * 256),
           R = (byte)Math.Min(255, r    * 256),
           G = (byte)Math.Min(255, g    * 256),
           B = (byte)Math.Min(255, b    * 256);

      pixel(linha, coluna, Color.FromArgb(A, R, G, B));
    }

    public void originarDoSuperiorEsquerdo()
    {
      fSuperior = true;
    }

    public void originarDoInferiorEsquerdo()
    {
      fSuperior = false;
    }

    public void negativar()
    {
      for (int linha = 0; linha < fBitmap.Height; linha++)
        for (int coluna = 0; coluna < fBitmap.Width; coluna++)
        {
          var cor = fBitmap.GetPixel(coluna, linha);

          byte R = (byte)(255 - cor.R),
               G = (byte)(255 - cor.G),
               B = (byte)(255 - cor.B);

          fBitmap.SetPixel(coluna, linha, Color.FromArgb(cor.A, R, G, B));
        }
    }

    public void espelharHorizontalmente()
    {
      fBitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
    }

    public void espelharVerticalmente()
    {
      fBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
    }

    public void rotarParaEsquerda()
    {
      // PORRA, MICROSOFT!!!
      // O SENTIDO CORRETO DE ROTAÇÃO É O ANTI-HORÁRIO!!!
      fBitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
    }

    public void rotarParaDireita()
    {
      // PORRA, MICROSOFT!!!
      // O SENTIDO CORRETO DE ROTAÇÃO É O ANTI-HORÁRIO!!!
      fBitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
    }

    public ImagemBitmap subimagem(int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal)
    {
      if (linhaInicial < 0 || linhaInicial >= fBitmap.Height)
        throw new ArgumentOutOfRangeException(nameof(linhaInicial));
      if (colunaInicial < 0 || colunaInicial >= fBitmap.Width)
        throw new ArgumentOutOfRangeException(nameof(colunaInicial));
      if (linhaFinal < 0 || linhaFinal >= fBitmap.Height)
        throw new ArgumentOutOfRangeException(nameof(linhaFinal));
      if (colunaFinal < 0 || colunaFinal >= fBitmap.Width)
        throw new ArgumentOutOfRangeException(nameof(colunaFinal));

      linhaInicial = obterLinha(linhaInicial);
      linhaFinal   = obterLinha(linhaFinal);

      int linha   = Math.Min(linhaInicial,  linhaFinal),
          coluna  = Math.Min(colunaInicial, colunaFinal),
          altura  = Math.Abs(linhaFinal    - linhaInicial) + 1,
          largura = Math.Abs(colunaInicial - colunaFinal)  + 1;

      Bitmap bitmap = fBitmap.Clone
      (
        new Rectangle(coluna, linha, largura, altura), PixelFormat.Format32bppArgb
      );

      return new ImagemBitmap(bitmap, fSuperior);
    }

    public void salvar(string arquivo)
    {
      fBitmap.Save(arquivo);
    }

    public void salvar(Stream stream, string formato)
    {
      fBitmap.Save(stream, formatoPorExtensao(formato));
    }
    #endregion

    #region Assistentes
    private ImageFormat formatoPorExtensao(string extensao)
    {
      switch (extensao.ToLowerInvariant())
      {
        case "bmp" : return ImageFormat.Bmp;
        case "git" : return ImageFormat.Gif;
        case "ico" : return ImageFormat.Icon;
        case "jpg" :
        case "jpeg": return ImageFormat.Jpeg;
        case "png" : return ImageFormat.Png;
        case "tif" :
        case "tiff": return ImageFormat.Tiff;
        default: throw new ArgumentOutOfRangeException(nameof(extensao));
      }
    }

    private int obterLinha(int linha)
    {
      return fSuperior ? linha : fBitmap.Height - linha - 1;
    }
    #endregion

    #region TO DELETE
    private bool isEquals(ImagemBitmap other)
    {
      bool result = (this.altura  == other.altura) &&
                    (this.largura == other.largura);

      for (int linha = 0; result && linha < this.altura; linha++)
        for (int coluna = 0; result && coluna < this.largura; coluna++)
          result = this.pixel(linha, coluna) == other.pixel(linha, coluna);

      return result;
    }

    public override bool Equals(object obj)
    {
      return (obj != null) &&
             ((this == obj) ||
             ((obj is ImagemBitmap) && isEquals((ImagemBitmap)obj)));
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion

    #region Propriedades
    public Color pixel(int linha, int coluna)
    {
      if (linha < 0 || linha >= fBitmap.Height)
        throw new ArgumentOutOfRangeException("", (Exception)null);
      if (coluna < 0 || coluna >= fBitmap.Width)
        throw new ArgumentOutOfRangeException("", (Exception)null);

      return fBitmap.GetPixel(coluna, obterLinha(linha));
    }

    public void pixel(int linha, int coluna, Color cor)
    {
      if (linha < 0 || linha >= fBitmap.Height)
        throw new ArgumentOutOfRangeException("", (Exception)null);
      if (coluna < 0 || coluna >= fBitmap.Width)
        throw new ArgumentOutOfRangeException("", (Exception)null);

      fBitmap.SetPixel(coluna, obterLinha(linha), cor);
    }

    public int altura
    {
      get { return fBitmap.Height; }
    }

    public int largura
    {
      get { return fBitmap.Width; }
    }
    #endregion
  }
}