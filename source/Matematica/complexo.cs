﻿/*******************************************************************************
 * 
 * Arquivo  : complexo.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-09-13 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define o tipo de dado que representa um número complexo, um corpo
 *            escrito na forma a + bi, para a, b que pertencem aos reais.
 * 
 *******************************************************************************/
using System;
using System.Globalization;
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Matematica
{
  public struct Complexo : IFormattable, IEquatable<Complexo>
  {
    #region Constantes
    public static readonly Complexo ZERO = new Complexo(0, 0);
    public static readonly Complexo NaN  = new Complexo(double.NaN, double.NaN);
    #endregion

    #region Atributos & Propriedades
    public double re,
                  im;

    public double modulo
    {
      get { return Math.Sqrt(re * re + im * im); }
    }

    public double argumento
    {
      get { return Math.Atan2(im, re); }
    }

    public Complexo conjugado
    {
      get { return new Complexo(re, -im); }
    }
    #endregion

    #region Construtores
    public Complexo(double re, double im)
    {
      this.re = re;
      this.im = im;
    }

    public static Complexo FormaPolar(double raio, double theta)
    {
      return new Complexo(raio * Math.Cos(theta), raio * Math.Sin(theta));
    }
    #endregion

    #region Métodos
    public override bool Equals(object obj)
    {
      return (obj is Complexo) && Equals((Complexo)obj);
    }

    public bool Equals(Complexo other)
    {
      return (re == other.re) &&
             (im == other.im);
    }

    public override int GetHashCode()
    {
      return re.GetHashCode() ^ im.GetHashCode();
    }

    public override string ToString()
    {
      return ToString("G", CultureInfo.CurrentCulture);
    }

    public string ToString(IFormatProvider provider)
    {
      return ToString("G", provider);
    }

    public string ToString(string format)
    {
      return ToString(format, CultureInfo.CurrentCulture);
    }

    public string ToString(string format, IFormatProvider provider)
    {
      if (im == 0)
        return re.ToString(format, provider);
      else if (re == 0)
        return im.ToString(format, provider) + "i";
      else if (im > 0)
        return re.ToString(format, provider) + " + " + im.ToString(format, provider) + "i";
      else
        return re.ToString(format, provider) + " - " + Math.Abs(im).ToString(format, provider) + "i";
    }
    #endregion

    #region Operadores
    public static bool operator ==(Complexo esquerda, Complexo direita)
    {
      return esquerda.re == direita.re &&
             esquerda.im == direita.im;
    }

    public static bool operator !=(Complexo esquerda, Complexo direita)
    {
      return esquerda.re != direita.re ||
             esquerda.im != direita.im;
    }

    public static Complexo operator +(Complexo valor)
    {
      return valor;
    }

    public static Complexo operator -(Complexo valor)
    {
      return new Complexo(-valor.re, -valor.im);
    }

    #region ADIÇÃO
    public static Complexo operator +(Complexo esquerda, Complexo direita)
    {
      return new Complexo(esquerda.re + direita.re, esquerda.im + direita.im);
    }

    public static Complexo operator +(double esquerda, Complexo direita)
    {
      return new Complexo(esquerda + direita.re, direita.im);
    }

    public static Complexo operator +(Complexo esquerda, double direita)
    {
      return new Complexo(esquerda.re + direita, esquerda.im);
    }

    public static Complexo operator +(int esquerda, Complexo direita)
    {
      return new Complexo(esquerda + direita.re, direita.im);
    }

    public static Complexo operator +(Complexo esquerda, int direita)
    {
      return new Complexo(esquerda.re + direita, esquerda.im);
    }
    #endregion

    #region SUBTRAÇÃO
    public static Complexo operator -(Complexo esquerda, Complexo direita)
    {
      return new Complexo(esquerda.re - direita.re, esquerda.im - direita.im);
    }

    public static Complexo operator -(double esquerda, Complexo direita)
    {
      return new Complexo(esquerda - direita.re, -direita.im);
    }

    public static Complexo operator -(Complexo esquerda, double direita)
    {
      return new Complexo(esquerda.re - direita, esquerda.im);
    }

    public static Complexo operator -(int esquerda, Complexo direita)
    {
      return new Complexo(esquerda - direita.re, -direita.im);
    }

    public static Complexo operator -(Complexo esquerda, int direita)
    {
      return new Complexo(esquerda.re - direita, esquerda.im);
    }
    #endregion

    #region MULTIPLICAÇÃO
    public static Complexo operator *(Complexo esquerda, Complexo direita)
    {
      return new Complexo(esquerda.re * direita.re - esquerda.im * direita.im,
                                esquerda.re * direita.im + esquerda.im * direita.re);
    }

    public static Complexo operator *(double esquerda, Complexo direita)
    {
      return new Complexo(esquerda * direita.re, esquerda * direita.im);
    }

    public static Complexo operator *(Complexo esquerda, double direita)
    {
      return new Complexo(esquerda.re * direita, esquerda.im * direita);
    }

    public static Complexo operator *(int esquerda, Complexo direita)
    {
      return new Complexo(esquerda * direita.re, esquerda * direita.im);
    }

    public static Complexo operator *(Complexo esquerda, int direita)
    {
      return new Complexo(esquerda.re * direita, esquerda.im * direita);
    }
    #endregion

    #region DIVISÃO
    public static Complexo operator /(Complexo esquerda, Complexo direita)
    {
      if (eZero(direita))
        return NaN;

      double raio  = esquerda.modulo    / direita.modulo,
             theta = esquerda.argumento - direita.argumento;

      return FormaPolar(raio, theta);
    }

    public static Complexo operator /(double esquerda, Complexo direita)
    {
      if (eZero(direita))
        return NaN;

      double   alfa = esquerda / Math.Pow(direita.modulo, 2);
      Complexo conj = direita.conjugado;

      return new Complexo(alfa * conj.re, alfa * conj.im);
    }

    public static Complexo operator /(Complexo esquerda, double direita)
    {
      return (direita == 0) ? NaN : new Complexo(esquerda.re / direita, esquerda.im / direita);
    }

    public static Complexo operator /(int esquerda, Complexo direita)
    {
      if (eZero(direita))
        return NaN;

      double   alfa = esquerda / Math.Pow(direita.modulo, 2);
      Complexo conj = direita.conjugado;

      return new Complexo(alfa * conj.re, alfa * conj.im);
    }

    public static Complexo operator /(Complexo esquerda, int direita)
    {
      return (direita == 0) ? NaN : new Complexo(esquerda.re / direita, esquerda.im / direita);
    }
    #endregion

    #region CONVERSORES
    public static implicit operator Complexo(double real)
    {
      return new Complexo(real, 0);
    }

    public static implicit operator Complexo(int real)
    {
      return new Complexo(real, 0);
    }
    #endregion
    #endregion

    #region Funções
    public static bool eZero(Complexo valor)
    {
      return (valor.re == 0) && (valor.im == 0);
    }

    public static bool eZero(Complexo valor, double epsilon)
    {
      return valor.modulo < epsilon;
    }

    public static bool eNaN(Complexo valor)
    {
      return double.IsNaN     (valor.re) ||
             double.IsNaN     (valor.im) ||
             double.IsInfinity(valor.re) ||
             double.IsInfinity(valor.im);
    }

    public static double abs(Complexo valor)
    {
      return valor.modulo;
    }

    public static double arg(Complexo valor)
    {
      return valor.argumento;
    }

    public static double dist(Complexo esquerda, Complexo direita)
    {
      return (esquerda - direita).modulo;
    }

    public static Complexo conj(Complexo valor)
    {
      return valor.conjugado;
    }

    public static Complexo exp(Complexo expoente)
    {
      return FormaPolar(Math.Exp(expoente.re), expoente.im);
    }

    public static Complexo exp(Complexo _base, Complexo expoente)
    {
      return exp(expoente * ln(_base));
    }

    public static Complexo ln(Complexo valor)
    {
      return new Complexo(Math.Log(valor.modulo), valor.argumento);
    }

    public static Complexo log(Complexo _base, Complexo valor)
    {
      return ln(valor) / ln(_base);
    }

    public static Complexo gama(Complexo valor)
    {
      return ZERO;
    }

    public static Complexo[] raiz(Complexo valor, uint n)
    {
      Complexo[] result = new Complexo[n];

      for (uint i = 0; i < n; i++)
        result[i] = ZERO;

      return result;
    }

    public static Complexo sen(Complexo valor)
    {
      return new Complexo
      (
        Math.Sin(valor.re) * Math.Cosh(valor.im),
        Math.Cos(valor.re) * Math.Sinh(valor.im)
      );
    }

    public static Complexo cos(Complexo valor)
    {
      return new Complexo
      (
        + Math.Cos(valor.re) * Math.Cosh(valor.im),
        - Math.Sin(valor.re) * Math.Sinh(valor.im)
      );
    }

    public static Complexo tan(Complexo valor)
    {
      Complexo _sen = sen(valor),
               _cos = cos(valor);

      return !eZero(_cos) ? _sen / _cos : NaN;
    }

    public static Complexo csc(Complexo valor)
    {
      Complexo _sen = sen(valor);

      return !eZero(_sen) ? 1 / _sen : NaN;
    }

    public static Complexo sec(Complexo valor)
    {
      Complexo _cos = cos(valor);

      return !eZero(_cos) ? 1 / _cos : NaN;
    }

    public static Complexo cot(Complexo valor)
    {
      Complexo _sen = sen(valor),
               _cos = cos(valor);

      return !eZero(_sen) ? _cos / _sen : NaN;
    }

    public static Complexo senh(Complexo valor)
    {
      return 0.5 * (exp(valor) - exp(-valor));
    }

    public static Complexo cosh(Complexo valor)
    {
      return 0.5 * (exp(valor) + exp(-valor));
    }

    public static Complexo tanh(Complexo valor)
    {
      Complexo _senh = senh(valor),
               _cosh = cosh(valor);

      return !eZero(_cosh) ? _senh / _cosh : NaN;
    }

    public static Complexo csch(Complexo valor)
    {
      Complexo _senh = senh(valor);

      return !eZero(_senh) ? 1 / _senh : NaN;
    }

    public static Complexo sech(Complexo valor)
    {
      Complexo _cosh = cosh(valor);

      return !eZero(_cosh) ? 1 / _cosh : NaN;
    }

    public static Complexo coth(Complexo valor)
    {
      Complexo _senh = senh(valor),
               _cosh = cosh(valor);

      return !eZero(_senh) ? _cosh / _senh : NaN;
    }
    #endregion

    #region Protocolos de Funções
    public static IComparadorIgualdade<Complexo> comparadorIgualdade()
    {
      return comparadorIgualdade(TMatematica.EPSILON);
    }

    public static IComparadorIgualdade<Complexo> comparadorIgualdade(double epsilon)
    {
      if (epsilon <= 0)
        epsilon = TMatematica.EPSILON;

      return new ComparadorIgualdadeComplexo(epsilon);
    }

    public static IRedutor<Complexo, Complexo> redutorSoma()
    {
      return new RedutorSomaComplexo();
    }

    public static IRedutor<Complexo, Complexo> redutorMultiplicacao()
    {
      return new RedutorMultiplicacaoComplexo();
    }

    public static IProjetor<Complexo, string> projetorString()
    {
      return new ProjetorStringComplexo();
    }
    #endregion
  }
}