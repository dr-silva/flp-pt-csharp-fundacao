﻿/*******************************************************************************
 *
 * Arquivo  : matematica.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Classe base para se trabalhar com os tipos de dados definidos nes-
 *            te pacote.
 *
 ******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica
{
  public static class TMatematica
  {
    #region Atributos & Constantes
    public const double EPSILON = 1e-7;

    private static readonly uint fSemente = (uint)Environment.TickCount;
    private static          uint fValor   = fSemente;
    #endregion

    #region Aleatoriedade
    private static uint proximo()
    {
      fValor = fValor * 0x19660D + 0x3C6EF35F;
      return fValor;
    }

    public static int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)limite);
    }

    public static double uniforme()
    {
      return proximo() * (1.0 / 0x10000 / 0x10000); // 2^-32
    }

    public static uint semente
    {
      get { return fSemente; }
    }
    #endregion

    #region Comparadores
    public static IComparador<float> criarComparadorFloat(float epsilon)
    {
      if (epsilon <= 0)
        epsilon = (float)EPSILON;

      return new ComparadorFloat(epsilon);
    }

    public static IComparador<double> criarComparadorDouble(double epsilon)
    {
      if (epsilon <= 0)
        epsilon = EPSILON;

      return new ComparadorDouble(epsilon);
    }

    public static IComparador<decimal> criarComparadorDecimal(decimal epsilon)
    {
      if (epsilon <= 0)
        epsilon = (decimal)EPSILON;

      return new ComparadorDecimal(epsilon);
    }
    #endregion

    #region Comparadores de Igualdade
    public static IComparadorIgualdade<float> criarComparadorIgualdadeFloat(float epsilon)
    {
      if (epsilon <= 0)
        epsilon = (float)EPSILON;

      return new ComparadorIgualdadeFloat(epsilon);
    }

    public static IComparadorIgualdade<double> criarComparadorIgualdadeDouble(double epsilon)
    {
      if (epsilon <= 0)
        epsilon = EPSILON;

      return new ComparadorIgualdadeDouble(epsilon);
    }

    public static IComparadorIgualdade<decimal> criarComparadorIgualdadeDecimal(decimal epsilon)
    {
      if (epsilon <= 0)
        epsilon = (decimal)EPSILON;

      return new ComparadorIgualdadeDecimal(epsilon);
    }
    #endregion
  }
}