﻿/*******************************************************************************
 * 
 * Arquivo  : igualdade.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as implementações protocolos de funções de igualdade para
 *            os tipos pontos-flutuantes instanciados pela classe Matemática.
 * 
 *******************************************************************************/
using System;
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Matematica
{
  struct ComparadorIgualdadeFloat : IComparadorIgualdade<float>
  {
    private readonly float epsilon;

    public ComparadorIgualdadeFloat(float epsilon)
    {
      this.epsilon = epsilon;
    }

    public bool Equals(float x, float y)
    {
      return Math.Abs(x - y) < epsilon;
    }

    public int GetHashCode(float obj)
    {
      return obj.GetHashCode();
    }
  }

  struct ComparadorIgualdadeDouble : IComparadorIgualdade<double>
  {
    private readonly double epsilon;

    public ComparadorIgualdadeDouble(double epsilon)
    {
      this.epsilon = epsilon;
    }

    public bool Equals(double x, double y)
    {
      return Math.Abs(x - y) < epsilon;
    }

    public int GetHashCode(double obj)
    {
      return obj.GetHashCode();
    }
  }

  struct ComparadorIgualdadeDecimal : IComparadorIgualdade<decimal>
  {
    private readonly decimal epsilon;

    public ComparadorIgualdadeDecimal(decimal epsilon)
    {
      this.epsilon = epsilon;
    }

    public bool Equals(decimal x, decimal y)
    {
      return Math.Abs(x - y) < epsilon;
    }

    public int GetHashCode(decimal obj)
    {
      return obj.GetHashCode();
    }
  }
}