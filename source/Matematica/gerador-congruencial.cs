﻿/*******************************************************************************
 *
 * Arquivo  : gerador-congruencial.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-18 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define a interface para geradores de números aleatórios.
 *
 ******************************************************************************/

namespace FractalLotus.Fundacao.Matematica
{
  public interface IGeradorCongruencial
  {
    int    uniforme(int valor);
    double uniforme();

    uint semente { get; }
  }
}