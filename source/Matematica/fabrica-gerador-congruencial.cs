﻿/*******************************************************************************
 *
 * Arquivo  : fabrica-gerador-congruencial.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-23 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Declara a fabrica de geradores congruenciais.
 *
 ******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FractalLotus.Fundacao.Matematica
{
  public static class FabricaGeradorCongruencial
  {
    #region Registros
    private static readonly IDictionary<string, ConstructorInfo> registros = registrar();

    private static IDictionary<string, ConstructorInfo> registrar()
    {
      IDictionary<string, ConstructorInfo> result = new Dictionary<string, ConstructorInfo>();

      Type     tipoBase = typeof(IGeradorCongruencial);
      Assembly assembly = Assembly.GetExecutingAssembly();

      foreach (Type tipo in assembly.GetTypes())
      {
        if (!tipo.IsClass || tipo.IsAbstract || !tipoBase.IsAssignableFrom(tipo))
          continue;

        GeradorCongruencialAttribute atributo   = obterAtributo  (tipo);
        ConstructorInfo              construtor = obterConstrutor(tipo);

        if ((atributo?.validar() ?? false) && (construtor != null))
          result.Add(atributo.chave, construtor);
      }

      return result;
    }

    private static GeradorCongruencialAttribute obterAtributo(Type tipo)
    {
      Attribute atributo = tipo.GetCustomAttribute(typeof(GeradorCongruencialAttribute));

      if (atributo != null && atributo is GeradorCongruencialAttribute)
        return (GeradorCongruencialAttribute)atributo;
      else
        return null;
    }

    private static ConstructorInfo obterConstrutor(Type tipo)
    {
      return tipo.GetConstructor(new Type[] { typeof(uint) });
    }
    #endregion

    #region Assistentes
    private static List<ConstructorInfo> listar()
    {
      lock (registros)
      {
        return new List<ConstructorInfo>(registros.Values);
      }
    }

    private static List<ConstructorInfo> listar(TipoGeradorCongruencial tipo)
    {
      lock (registros)
      {
        string str = tipo.ToString();

        return registros.Where (x => x.Key.StartsWith(str))
                        .Select(x => x.Value)
                        .ToList();
      }
    }

    private static ConstructorInfo obter(string nome, TipoGeradorCongruencial tipo)
    {
      lock (registros)
      {
        string chave = $"{tipo}-{nome}";

        return registros.ContainsKey(chave) ? registros[chave] : null;
      }
    }

    private static IGeradorCongruencial criar(ConstructorInfo info, uint semente)
    {
      return (IGeradorCongruencial) info?.Invoke(new object[] { semente });
    }
    #endregion

    #region Interface
    public static IGeradorCongruencial criar()
    {
      return criar((uint)Environment.TickCount);
    }

    public static IGeradorCongruencial criar(uint semente)
    {
      List<ConstructorInfo> lista = listar();

      return lista.Count > 0 ? criar(lista[ TMatematica.uniforme(lista.Count) ], semente) : null;
    }

    public static IGeradorCongruencial criar(TipoGeradorCongruencial tipo)
    {
      return criar(tipo, (uint)Environment.TickCount);
    }

    public static IGeradorCongruencial criar(TipoGeradorCongruencial tipo, uint semente)
    {
      List<ConstructorInfo> lista = listar(tipo);

      return lista.Count > 0 ? criar(lista[ TMatematica.uniforme(lista.Count) ], semente) : null;
    }

    public static IGeradorCongruencial criar(string nome, TipoGeradorCongruencial tipo)
    {
      return criar(nome, tipo, (uint)Environment.TickCount);
    }

    public static IGeradorCongruencial criar(string nome, TipoGeradorCongruencial tipo, uint semente)
    {
      return criar(obter(nome, tipo), semente);
    }
    #endregion
  }
}