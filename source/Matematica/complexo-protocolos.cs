﻿/*******************************************************************************
 * 
 * Arquivo  : complexo-protocolos.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as implementações de protocolos de funções para o tipo
 *            complexo.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Matematica
{
  struct ComparadorIgualdadeComplexo : IComparadorIgualdade<Complexo>
  {
    private readonly double epsilon;

    public ComparadorIgualdadeComplexo(double epsilon)
    {
      this.epsilon = epsilon;
    }

    public bool Equals(Complexo x, Complexo y)
    {
      return Complexo.dist(x, y) < epsilon;
    }

    public int GetHashCode(Complexo obj)
    {
      return obj.GetHashCode();
    }
  }

  struct RedutorSomaComplexo : IRedutor<Complexo, Complexo>
  {
    public Complexo reduzir(Complexo valor, Complexo item)
    {
      return valor + item;
    }
  }

  struct RedutorMultiplicacaoComplexo : IRedutor<Complexo, Complexo>
  {
    public Complexo reduzir(Complexo valor, Complexo item)
    {
      return valor * item;
    }
  }

  struct ProjetorStringComplexo : IProjetor<Complexo, string>
  {
    public string projetar(Complexo valor)
    {
      return valor.ToString();
    }
  }
}
