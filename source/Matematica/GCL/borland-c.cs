﻿/*******************************************************************************
 *
 * Arquivo  : borland-c.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-24 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Borland C.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("borland-c", TipoGeradorCongruencial.Linear)]
  public class BorlandC : IGeradorCongruencial
  {
    #region Atributos
    private uint fValor,
                 fSemente;
    #endregion

    #region Construtor
    public BorlandC(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = (fValor * 0x015A4E35 + 1);
      return fValor & 0x7FFFFFFF;
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000);
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}