﻿/*******************************************************************************
 *
 * Arquivo  : open-vms.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Open VMS, um sistema operacional.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("open-vms", TipoGeradorCongruencial.Linear)]
  public class OpenVms : IGeradorCongruencial
  {
    #region Atributos
    private uint fValor,
                 fSemente;
    #endregion

    #region Construtor
    public OpenVms(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = fValor * 0x10DCD + 1;
      return fValor;
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)(limite));
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x10000 / 0x10000); // 2^-32
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}