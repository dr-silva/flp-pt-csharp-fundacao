﻿/*******************************************************************************
 *
 * Arquivo  : borland-pascal.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-24 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Borland Pascal.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("borland-pascal", TipoGeradorCongruencial.Linear)]
  public class BorlandPascal : IGeradorCongruencial
  {
    #region Atributos
    private uint fValor,
                 fSemente;
    #endregion

    #region Construtor
    public BorlandPascal(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = fValor * 0x08088405 + 1;
      return fValor;
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      ulong maxixmo = (uint)limite,
            proximo = this.proximo(),
            result  = (maxixmo * proximo) >> 32;

      return (int)result;
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x10000 / 0x10000); // 2^-32
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}
