﻿/*******************************************************************************
 *
 * Arquivo  : posix.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            POSIX (Portable Operating System Interface), uma família de
 *            padrões especificados pela IEEE Computer Society.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("posix", TipoGeradorCongruencial.Linear)]
  public class Posix : IGeradorCongruencial
  {
    #region Atributos
    private ulong fValor;
    private uint  fSemente;
    #endregion

    #region Construtor
    public Posix(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = (fValor * 0x5DEECE66D + 11) & 0xFFFFFFFFFFFF;
      return (uint)(fValor >> 16);
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)(limite));
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x100000000L); // 2^32
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}