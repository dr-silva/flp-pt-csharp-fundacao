﻿/*******************************************************************************
 *
 * Arquivo  : mmix.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            MMIX desenhado por Donald Knuth.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("mmix", TipoGeradorCongruencial.Linear)]
  public class MMIX : IGeradorCongruencial
  {
    #region Atributos
    private ulong fValor;
    private uint  fSemente;

    private const ulong MULTIPLIER = 0x5851F42D4C957F2D; // 6.364.136.223.846.793.005
    private const long  INCREMENT  = 0x14057B7EF767814F; // 1.442.695.040.888.963.407
    #endregion

    #region Construtor
    public MMIX(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private ulong proximo()
    {
      fValor = fValor * MULTIPLIER + INCREMENT;
      return fValor;
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (ulong)(limite));
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x10000 / 0x10000 / 0x10000 / 0x10000); // 2^-64
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}