﻿/*******************************************************************************
 *
 * Arquivo  : numerical-recipes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-24 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            Numerical Recipes.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("numerical-recipes", TipoGeradorCongruencial.Linear)]
  public class NumericalRecipes : IGeradorCongruencial
  {
    #region Atributos
    private uint fValor,
                 fSemente;
    #endregion

    #region Construtor
    public NumericalRecipes(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = fValor * 0x19660D + 0x3C6EF35F;
      return fValor;
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x10000 / 0x10000); // 2^-32
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}
