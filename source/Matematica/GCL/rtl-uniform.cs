﻿/*******************************************************************************
 *
 * Arquivo  : rtl-uniform.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-25 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            RtlUniform da Native API usada pelo Windows NT.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("rtl-uniform", TipoGeradorCongruencial.Linear)]
  public class RtlUniform : IGeradorCongruencial
  {
    #region Atributos
    private uint fValor,
                 fSemente;
    #endregion

    #region Construtor
    public RtlUniform(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = (fValor * 0x7FFFFFED + 0x7FFFFFC3) & 0x7FFFFFFF;
      return fValor;
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)limite);
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x80000000); // 2^-31
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}