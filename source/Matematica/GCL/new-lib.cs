﻿/*******************************************************************************
 *
 * Arquivo  : new-lib.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define o gerador congruencial linear com as especificações do
 *            New Lib, uma biblioteca padrão para o C.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Recursos;

namespace FractalLotus.Fundacao.Matematica.GCL
{
  [GeradorCongruencial("new-lib", TipoGeradorCongruencial.Linear)]
  public class NewLib : IGeradorCongruencial
  {
    #region Atributos
    private ulong fValor;
    private uint  fSemente;
    #endregion

    #region Construtor
    public NewLib(uint semente)
    {
      fValor = fSemente = semente;
    }
    #endregion

    #region Assistentes
    private uint proximo()
    {
      fValor = fValor * 0x5851F42D4C957F2D + 1;
      return (uint)(fValor >> 32);
    }
    #endregion

    #region Interface
    public int uniforme(int limite)
    {
      if (limite <= 0)
        throw Excecoes.ArgumentoNegativo(nameof(limite));

      return (int)(proximo() % (uint)(limite));
    }

    public double uniforme()
    {
      return proximo() * (1.0 / 0x10000 / 0x10000); // 2^-32
    }

    public uint semente
    {
      get { return fSemente; }
    }
    #endregion
  }
}