﻿/*******************************************************************************
 * 
 * Arquivo  : igualdade.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Define as implementações protocolos de funções de comparação para
 *            os tipos pontos-flutuantes instanciados pela classe Matemática.
 * 
 *******************************************************************************/
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Matematica
{
  struct ComparadorFloat : IComparador<float>
  {
    private readonly float epsilon;

    public ComparadorFloat(float epsilon)
    {
      this.epsilon = epsilon;
    }

    public int Compare(float x, float y)
    {
      float diferenca = x - y;

      return (diferenca <= -epsilon) ? -1 :
             (diferenca >= +epsilon) ? +1 : 0;
    }
  }

  struct ComparadorDouble : IComparador<double>
  {
    private readonly double epsilon;

    public ComparadorDouble(double epsilon)
    {
      this.epsilon = epsilon;
    }

    public int Compare(double x, double y)
    {
      double diferenca = x - y;

      return (diferenca <= -epsilon) ? -1 :
             (diferenca >= +epsilon) ? +1 : 0;
    }
  }

  struct ComparadorDecimal : IComparador<decimal>
  {
    private readonly decimal epsilon;

    public ComparadorDecimal(decimal epsilon)
    {
      this.epsilon = epsilon;
    }

    public int Compare(decimal x, decimal y)
    {
      decimal diferenca = x - y;

      return (diferenca <= -epsilon) ? -1 :
             (diferenca >= +epsilon) ? +1 : 0;
    }
  }
}