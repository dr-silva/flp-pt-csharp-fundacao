﻿/*******************************************************************************
 *
 * Arquivo  : registro-gerador.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-18 <yyyy-mm-dd>
 * Licença  : Este código está sob licença Non-Comercial Creative Commons
 * Descrição: Define os tipos de dados necessários para se registrar um gerador
 *            congruencial.
 *
 ******************************************************************************/
using System;

namespace FractalLotus.Fundacao.Matematica
{
  public enum TipoGeradorCongruencial
  {
    Linear,
    NaoLinear
  }

  [AttributeUsage(AttributeTargets.Class)]
  public class GeradorCongruencialAttribute : Attribute
  {
    public readonly string                  nome;
    public readonly TipoGeradorCongruencial tipo;

    public GeradorCongruencialAttribute(string nome, TipoGeradorCongruencial tipo)
    {
      this.nome = nome;
      this.tipo = tipo;
    }

    public bool validar()
    {
      return !string.IsNullOrWhiteSpace(nome);
    }

    public string chave
    {
      get { return $"{tipo}-{nome}"; }
    }
  }
}
