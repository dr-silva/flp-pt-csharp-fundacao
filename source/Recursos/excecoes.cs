﻿using System;

namespace FractalLotus.Fundacao.Recursos
{
  class Excecoes
  {
    public const string S_PERIODO_INVALIDO    = "O parâmetro \"{0}\" não corresponde a um Período";
    public const string S_ARGUMENTO_NULO      = "O parâmetro \"{0}\" não pode ser nulo";
    public const string S_ARGUMENTO_NEGATIVO  = "O parâmetro \"{0}\" deve ser positivo";
    public const string S_INDICE_FORA_LIMITES = "O parâmetro \"{0}\" está fora dos limites: {1} e {2}";
    public const string S_CHAVE_DICIONARIO    = "A chave informada não consta no dicionário";

    public const string S_OP_COLECAO_VAZIA    = "Operação inválida em coleção vazia";
    public const string S_OP_COLECAO_CHEIA    = "Operação inválida em coleção cheia";

    public static Exception PeriodoInvalido(string parametro)
    {
      return new ArgumentException(string.Format(S_PERIODO_INVALIDO, parametro));
    }
    public static Exception ArgumentoNulo(string parametro)
    {
      return new ArgumentNullException(string.Format(S_ARGUMENTO_NULO, parametro), (Exception)null);
    }
    public static Exception ArgumentoNegativo(string parametro)
    {
      return new ArgumentOutOfRangeException(string.Format(S_ARGUMENTO_NEGATIVO, parametro), (Exception)null);
    }
    public static Exception IndiceForaLimites(string parametro, object inferior, object superior)
    {
      return new IndexOutOfRangeException
      (
        string.Format(S_INDICE_FORA_LIMITES, parametro, inferior, superior)
      );
    }
    public static Exception ChaveDicionario()
    {
      return new ArgumentOutOfRangeException(S_CHAVE_DICIONARIO, (Exception)null);
    }
    public static Exception ColecaoVazia()
    {
      return new InvalidOperationException(S_OP_COLECAO_VAZIA);
    }
    public static Exception ColecaoCheia()
    {
      return new InvalidOperationException(S_OP_COLECAO_CHEIA);
    }
  }
}