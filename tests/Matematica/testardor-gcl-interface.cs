﻿using FractalLotus.Fundacao.Matematica;

namespace FractalLotus.Fundacao.Testes.Matematica
{
  public abstract class TestadorGCLInterface : TestadorGCL
  {
    private readonly IGeradorCongruencial gerador;

    public TestadorGCLInterface(string nome)
    {
      gerador = FabricaGeradorCongruencial.criar
      (
        nome, TipoGeradorCongruencial.Linear, 20210927
      );
    }

    protected override int gerarUniforme(int limite)
    {
      return gerador.uniforme(limite);
    }

    protected override double gerarUniforme()
    {
      return gerador.uniforme();
    }
  }
}
