﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FractalLotus.Fundacao.Testes.Matematica
{
  [TestClass]
  public class TestadorGCLAppleCarbon : TestadorGCLInterface
  {
    public TestadorGCLAppleCarbon() : base("apple-carbon")
    {
    }
  }

  [TestClass]
  public class TestadorGCLBorlandC : TestadorGCLInterface
  {
    public TestadorGCLBorlandC() : base("borland-c")
    {
    }
  }

  [TestClass]
  public class TestadorGCLBorlandPascal : TestadorGCLInterface
  {
    public TestadorGCLBorlandPascal() : base("borland-pascal")
    {
    }
  }

  [TestClass]
  public class TestadorGCLCpp11 : TestadorGCLInterface
  {
    public TestadorGCLCpp11() : base("cpp11")
    {
    }
  }

  [TestClass]
  public class TestadorGCLGLibC : TestadorGCLInterface
  {
    public TestadorGCLGLibC() : base("glibc")
    {
    }
  }

  [TestClass]
  public class TestadorGCLMMIX : TestadorGCLInterface
  {
    public TestadorGCLMMIX() : base("mmix")
    {
    }
  }

  [TestClass]
  public class TestadorGCLNewLib : TestadorGCLInterface
  {
    public TestadorGCLNewLib() : base("new-lib")
    {
    }
  }

  [TestClass]
  public class TestadorGCLNumericalRecipes : TestadorGCLInterface
  {
    public TestadorGCLNumericalRecipes() : base("numerical-recipes")
    {
    }
  }

  [TestClass]
  public class TestadorGCLOpenVms : TestadorGCLInterface
  {
    public TestadorGCLOpenVms() : base("open-vms")
    {
    }
  }

  [TestClass]
  public class TestadorGCLPosix : TestadorGCLInterface
  {
    public TestadorGCLPosix() : base("posix")
    {
    }
  }

  [TestClass]
  public class TestadorGCLRtlUniform : TestadorGCLInterface
  {
    public TestadorGCLRtlUniform() : base("rtl-uniform")
    {
    }
  }
}
