﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Matematica;

namespace FractalLotus.Fundacao.Testes.Matematica
{
  [TestClass]
  public class TestadorGCLMatematica : TestadorGCL
  {
    protected override int gerarUniforme(int limite)
    {
      return TMatematica.uniforme(limite);
    }

    protected override double gerarUniforme()
    {
      return TMatematica.uniforme();
    }
  }
}
