﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FractalLotus.Fundacao.Testes.Matematica
{
  public abstract class TestadorGCL
  {
    private const double DELTA_INT    = 10,
                         DELTA_DOUBLE = 1.0 / 12.0;
    private const int    TOTAL        = 1000,
                         LIMITE       = 500;

    protected abstract int    gerarUniforme(int limite);
    protected abstract double gerarUniforme();

    [TestMethod]
    public void testarInteger()
    {
      double soma = 0;

      for (int i = 0; i < TOTAL; i++)
      {
        int valor = gerarUniforme(LIMITE);

        if ((valor < 0) || (valor >= LIMITE))
          Assert.Fail("Valor inteiro fora do limite.");

        soma += valor;
      }

      Assert.AreEqual((LIMITE - 1) / 2.0, soma / TOTAL, DELTA_INT);
    }

    [TestMethod]
    public void testarDouble()
    {
      double soma = 0;

      for (int i = 0; i < TOTAL; i++)
      {
        double valor = gerarUniforme();

        if ((valor < 0) || (valor >= 1))
          Assert.Fail("Valor inteiro fora do limite.");

        soma += valor;
      }

      Assert.AreEqual(0.5, soma / TOTAL, DELTA_DOUBLE);
    }

    [TestMethod]
    public void testarErro()
    {
      try
      {
        gerarUniforme(0);

        Assert.Fail("Era para ter levantado uma exceção.");
      }
      catch (ArgumentOutOfRangeException)
      {
        // do nothing
      }
      catch (Exception e)
      {
        Assert.Fail("Erro desconhecido: " + e.Message);
      }
    }
  }
}
