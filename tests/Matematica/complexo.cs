﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Matematica;

namespace FractalLotus.Fundacao.Testes.Matematica
{
  [TestClass]
  public class TestadorComplexo
  {
    #region Atributos
    private static readonly Complexo ESQUERDA = new Complexo( 2,  3),
                                     DIREITA  = new Complexo(-4, -4);
    private static readonly int      INTEIRO  = 2;
    private static readonly double   REAL     = 3.0;

    private readonly IComparadorIgualdade<Complexo> comparador = Complexo.comparadorIgualdade();
    #endregion

    #region Assistente
    private void validar(Complexo esperado, Complexo calculado)
    {
      Assert.IsTrue
      (
        comparador.Equals(esperado, calculado),
        $"expected <{esperado}>, but calculated <{calculado}>"
      );
    }
    #endregion

    #region Unário
    [TestMethod]
    public void testarPositivo()
    {
      validar(new Complexo(2, 3), +ESQUERDA);
    }

    [TestMethod]
    public void testarNegativo()
    {
      validar(new Complexo(-2, -3), -ESQUERDA);
    }
    #endregion

    #region Adição
    [TestMethod]
    public void testarAdicao_1()
    {
      validar(new Complexo(-2, -4), INTEIRO + DIREITA);
    }

    [TestMethod]
    public void testarAdicao_2()
    {
      validar(new Complexo(-1, -4), REAL + DIREITA);
    }

    [TestMethod]
    public void testarAdicao_3()
    {
      validar(new Complexo(4, 3), ESQUERDA + INTEIRO);
    }

    [TestMethod]
    public void testarAdicao_4()
    {
      validar(new Complexo(5, 3), ESQUERDA + REAL);
    }

    [TestMethod]
    public void testarAdicao_5()
    {
      validar(new Complexo(-2, -1), ESQUERDA + DIREITA);
    }
    #endregion

    #region Subtração
    [TestMethod]
    public void testarSubtracao_1()
    {
      validar(new Complexo(6, 4), INTEIRO - DIREITA);
    }

    [TestMethod]
    public void testarSubtracao_2()
    {
      validar(new Complexo(7, 4), REAL - DIREITA);
    }

    [TestMethod]
    public void testarSubtracao_3()
    {
      validar(new Complexo(0, 3), ESQUERDA - INTEIRO);
    }

    [TestMethod]
    public void testarSubtracao_4()
    {
      validar(new Complexo(-1, 3), ESQUERDA - REAL);
    }

    [TestMethod]
    public void testarSubtracao_5()
    {
      validar(new Complexo(6, 7), ESQUERDA - DIREITA);
    }
    #endregion

    #region Multiplicação
    [TestMethod]
    public void testarMultiplicacao_1()
    {
      validar(new Complexo(-8, -8), INTEIRO * DIREITA);
    }

    [TestMethod]
    public void testarMultiplicacao_2()
    {
      validar(new Complexo(-12, -12), REAL * DIREITA);
    }

    [TestMethod]
    public void testarMultiplicacao_3()
    {
      validar(new Complexo(4, 6), ESQUERDA * INTEIRO);
    }

    [TestMethod]
    public void testarMultiplicacao_4()
    {
      validar(new Complexo(6, 9), ESQUERDA * REAL);
    }

    [TestMethod]
    public void testarMultiplicacao_5()
    {
      validar(new Complexo(4, -20), ESQUERDA * DIREITA);
    }
    #endregion

    #region Divisão
    [TestMethod]
    public void testarDivisao_1()
    {
      validar(new Complexo(-0.25, 0.25), INTEIRO / DIREITA);
    }

    [TestMethod]
    public void testarDivisao_2()
    {
      validar(new Complexo(-0.375, 0.375), REAL / DIREITA);
    }

    [TestMethod]
    public void testarDivisao_3()
    {
      validar(new Complexo(1, 1.5), ESQUERDA / INTEIRO);
    }

    [TestMethod]
    public void testarDivisao_4()
    {
      validar(new Complexo(0.6666666667, 1), ESQUERDA / REAL);
    }

    [TestMethod]
    public void testarDivisao_5()
    {
      validar(new Complexo(-0.625, -0.125), ESQUERDA / DIREITA);
    }

    [TestMethod]
    public void testarDivisaoZero_1()
    {
      int      zero      = 0;
      Complexo calculado = ESQUERDA / zero;

      if (!Complexo.eNaN(calculado))
        Assert.Fail("Divisão por 0 devia retornar um valor não-numérico");
    }

    [TestMethod]
    public void testarDivisaoZero_2()
    {
      double   zero      = 0;
      Complexo calculado = ESQUERDA / zero;

      if (!Complexo.eNaN(calculado))
        Assert.Fail("Divisão por 0 devia retornar um valor não-numérico");
    }

    [TestMethod]
    public void testarDivisaoZero_3()
    {
      Complexo calculado = ESQUERDA / Complexo.ZERO;

      if (!Complexo.eNaN(calculado))
        Assert.Fail("Divisão por 0 devia retornar um valor não-numérico");
    }
    #endregion

    #region Exponenciação & Logartmo
    [TestMethod]
    public void testarExponenciacao_1()
    {
      validar(new Complexo(-7.315110095, 1.042743656), Complexo.exp(ESQUERDA));
    }

    [TestMethod]
    public void testarExponenciacao_2()
    {
      validar(new Complexo(-0.281851193, -0.107283263), Complexo.exp(ESQUERDA, DIREITA));
    }

    [TestMethod]
    public void testarLogaritmo_1()
    {
      validar(new Complexo(1.282474679, 0.982793723), Complexo.ln(ESQUERDA));
    }

    [TestMethod]
    public void testarLogaritmo_2()
    {
      validar(new Complexo(-0.035736229, -1.809839357), Complexo.log(ESQUERDA, DIREITA));
    }
    #endregion

    #region Trigonométrico
    [TestMethod]
    public void testarSeno()
    {
      validar(new Complexo(9.154499147, -4.16890696), Complexo.sen(ESQUERDA));
    }

    [TestMethod]
    public void testarCosseno()
    {
      validar(new Complexo(-4.189625691, -9.109227894), Complexo.cos(ESQUERDA));
    }

    [TestMethod]
    public void testarTangente()
    {
      validar(new Complexo(-0.003764026, 1.003238627), Complexo.tan(ESQUERDA));
    }

    [TestMethod]
    public void testarCossecante()
    {
      validar(new Complexo(0.09047321, 0.041200986), Complexo.csc(ESQUERDA));
    }

    [TestMethod]
    public void testarSecante()
    {
      validar(new Complexo(-0.041674964, 0.090611137), Complexo.sec(ESQUERDA));
    }

    [TestMethod]
    public void testarCotangente()
    {
      validar(new Complexo(-0.00373971, -0.996757797), Complexo.cot(ESQUERDA));
    }
    #endregion

    #region Hiperbólico
    [TestMethod]
    public void testarSenoHiperbolico()
    {
      validar(new Complexo(-3.59056459, 0.530921086), Complexo.senh(ESQUERDA));
    }

    [TestMethod]
    public void testarCossenoHiperbolico()
    {
      validar(new Complexo(-3.724545505, 0.51182257), Complexo.cosh(ESQUERDA));
    }

    [TestMethod]
    public void testarTangenteHiperbolica()
    {
      validar(new Complexo(0.965385879, -0.009884375), Complexo.tanh(ESQUERDA));
    }

    [TestMethod]
    public void testarCossecanteHiperbolica()
    {
      validar(new Complexo(-0.272548661, -0.040300579), Complexo.csch(ESQUERDA));
    }

    [TestMethod]
    public void testarSecanteHiperbolica()
    {
      validar(new Complexo(-0.263512975, -0.036211637), Complexo.sech(ESQUERDA));
    }

    [TestMethod]
    public void testarCotangenteHiperbolica()
    {
      validar(new Complexo(1.035746638, 0.010604783), Complexo.coth(ESQUERDA));
    }
    #endregion
  }
}
