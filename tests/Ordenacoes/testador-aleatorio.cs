﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Ordenacoes;

namespace FractalLotus.Fundacao.Testes.Ordenacoes
{
  [TestClass]
  public class TestadorAleatorio
  {
    #region Atributos
    private readonly IValidadorOrdenacao<int> validador;
    private readonly IOrdenacao         <int> ordenador;
    #endregion

    #region Construtor
    public TestadorAleatorio()
    {
      validador = new ValidadorOrdenacaoAscendente<int>();
      ordenador = new OrdenacaoAleatoria          <int>();
    }
    #endregion

    #region Assistentes
    private int[] criarVetor(int total)
    {
      if (total < 0)
        return null;

      int[] result = new int[total];

      for (int i = 0; i < total; i++)
        result[i] = i + 1;

      return result;
    }

    private bool ordenar(int total)
    {
      int[] vetor = criarVetor(total);

             ordenador.ordenar(vetor);
      return validador.validar(vetor);
    }
    #endregion

    #region Métodos de Testes
    [TestMethod]
    public void testarNulo()
    {
      Assert.IsTrue(ordenar(-1));
    }

    [TestMethod]
    public void testarVazio()
    {
      Assert.IsTrue(ordenar(0));
    }

    [TestMethod]
    public void testarNaoVazio()
    {
      Assert.IsFalse(ordenar(100));
    }
    #endregion
  }
}
