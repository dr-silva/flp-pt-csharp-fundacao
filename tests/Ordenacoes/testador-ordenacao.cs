﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Ordenacoes;

namespace FractalLotus.Fundacao.Testes.Ordenacoes
{
  public abstract class TestadorOrdenacao
  {
    #region Atributos
    private   readonly IComparer  <int> aleatorio;
    protected readonly IComparador<int> comparador;
    #endregion

    #region Construtor
    public TestadorOrdenacao()
    {
      aleatorio  = new ComparadorAleatorio();
      comparador = FabricaComparador.criar<int>();
    }
    #endregion

    #region Assistentes
    private IValidadorOrdenacao<int> criarValidador(bool crescente)
    {
      if (crescente)
        return new ValidadorOrdenacaoAscendente<int>(comparador);
      else
        return new ValidadorOrdenacaoDescendente<int>(comparador);
    }

    private int[] criarVetor(int total)
    {
      if (total < 0)
        return null;

      int[] result = new int[total];

      for (int i = 0; i < total; i++)
        result[i] = i + 1;

      Array.Sort(result, aleatorio);

      return result;
    }

    protected abstract IOrdenacao<int> criarOrdenacao(bool crescente);

    protected bool ordenar(int total, bool crescente)
    {
      IOrdenacao         <int> ordenacao = criarOrdenacao(crescente);
      IValidadorOrdenacao<int> validador = criarValidador(crescente);

      int[] vetor = criarVetor(total);

             ordenacao.ordenar(vetor);
      return validador.validar(vetor);
    }
    #endregion

    #region Métodos de Testes
    [TestMethod]
    public void testarCrescenteNulo()
    {
      Assert.IsTrue(ordenar(-1, true));
    }

    [TestMethod]
    public void testarCrescenteVazio()
    {
      Assert.IsTrue(ordenar(0, true));
    }

    [TestMethod]
    public void testarCrescenteNaoVazio()
    {
      Assert.IsTrue(ordenar(100, true));
    }

    [TestMethod]
    public void testarDecrescenteNulo()
    {
      Assert.IsTrue(ordenar(-1, false));
    }

    [TestMethod]
    public void testarDecrescenteVazio()
    {
      Assert.IsTrue(ordenar(0, false));
    }

    [TestMethod]
    public void testarDecrescenteNaoVazio()
    {
      Assert.IsTrue(ordenar(100, false));
    }
    #endregion
  }

  class ComparadorAleatorio : IComparer<int>
  {
    private readonly Dictionary<int, double> dicionario;
    private readonly Random                  aleatorio;

    public ComparadorAleatorio()
    {
      aleatorio  = new Random(20210910);
      dicionario = new Dictionary<int, double>();
    }

    public int Compare(int x, int y)
    {
      double valorX = dicionario.ContainsKey(x) ? dicionario[x] : aleatorio.NextDouble(),
             valorY = dicionario.ContainsKey(y) ? dicionario[y] : aleatorio.NextDouble();

      if (!dicionario.ContainsKey(x))
        dicionario.Add(x, valorX);

      if (!dicionario.ContainsKey(y))
        dicionario.Add(y, valorY);

      return (valorX < valorY) ? -1 :
             (valorX < valorY) ? +1 : 0;
    }
  }
}
