﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using FractalLotus.Fundacao.Ordenacoes;
using FractalLotus.Fundacao.Cerne;

namespace FractalLotus.Fundacao.Testes.Ordenacoes
{
  [TestClass]
  public class TestadorInsercao : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoPorInsercaoAscendente<int>(comparador);
      else
        return new OrdenacaoPorInsercaoDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorSelecao : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoPorSelecaoAscendente<int>(comparador);
      else
        return new OrdenacaoPorSelecaoDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorShell : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoShellAscendente<int>(comparador);
      else
        return new OrdenacaoShellDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorHeap : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoHeapAscendente<int>(comparador);
      else
        return new OrdenacaoHeapDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorMerge : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoMergeAscendente<int>(comparador);
      else
        return new OrdenacaoMergeDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorQuick : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoQuickAscendente<int>(comparador);
      else
        return new OrdenacaoQuickDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorQuick3Way : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoQuick3WayAscendente<int>(comparador);
      else
        return new OrdenacaoQuick3WayDescendente<int>(comparador);
    }
  }

  [TestClass]
  public class TestadorContagem : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoPorContagemAscendente<int>(x => x);
      else
        return new OrdenacaoPorContagemDescendente<int>(x => x);
    }
  }

  [TestClass]
  public class TestadorBalde : TestadorOrdenacao
  {
    protected override IOrdenacao<int> criarOrdenacao(bool crescente)
    {
      if (crescente)
        return new OrdenacaoPorBaldeAscendente<int>(x => x / 101.0);
      else
        return new OrdenacaoPorBaldeDescendente<int>(x => x / 101.0);
    }
  }
}
