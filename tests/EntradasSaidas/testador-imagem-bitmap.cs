﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.EntradasSaidas;

namespace FractalLotus.Fundacao.Testes.EntradasSaidas
{
  [TestClass]
  public class TestadorImagemBitmap
  {
    private const string RECURSOS   = "..\\..\\..\\recursos\\";
    private const string ORIGINAL_1 = RECURSOS + "original-1.png";
    private const string ORIGINAL_2 = RECURSOS + "original-2.png";
    private const string NEGATIVO   = RECURSOS + "neg.png";
    private const string HORIZONTAL = RECURSOS + "esp-horz.png";
    private const string VERTICAL   = RECURSOS + "esp-vert.png";
    private const string ESQUERDA   = RECURSOS + "rot-esq.png";
    private const string DIREITA    = RECURSOS + "rot-dir.png";

    private void validarImagem(ImagemBitmap ativo, ImagemBitmap passivo)
    {
      if (!ativo.Equals(passivo))
        Assert.Fail();
    }

    [TestMethod]
    public void testarNegativo()
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(NEGATIVO);

      ativo.negativar();
      validarImagem(ativo, passivo);
    }

    [TestMethod]
    public void testarEspelhoHorizontal()
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_2),
                   passivo = new ImagemBitmap(HORIZONTAL);

      ativo.espelharHorizontalmente();
      validarImagem(ativo, passivo);
    }

    [TestMethod]
    public void testarEspelhoVertical()
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(VERTICAL);

      ativo.espelharVerticalmente();
      validarImagem(ativo, passivo);
    }

    [TestMethod]
    public void testarRotacaoEsquerda()
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(ESQUERDA);

      ativo.rotarParaEsquerda();
      validarImagem(ativo, passivo);
    }

    [TestMethod]
    public void testarRotacaoDireita()
    {
      ImagemBitmap ativo   = new ImagemBitmap(ORIGINAL_1),
                   passivo = new ImagemBitmap(DIREITA);

      ativo.rotarParaDireita();
      validarImagem(ativo, passivo);
    }
  }
}
