﻿using System;
using System.Collections.Generic;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Ordenacoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  public class VetorValidacao
  {
    #region Atributos
    private readonly int   fTotal;
    private readonly int[] fValores;
    #endregion

    #region Construtor
    public VetorValidacao(int total)
    {
      fTotal   = total;
      fValores = new int[total];

      for (int i = 0; i < total; i++)
        fValores[i] = i + 1;

      using (var aleatorio = new OrdenacaoAleatoria<int>())
      {
        aleatorio.ordenar(fValores);
      }
    }
    #endregion

    #region Arrays
    public bool validarCrescente(int[] array)
    {
      using (var validador = new ValidadorOrdenacaoAscendente<int>())
      {
        return (array.Length == fTotal) && validador.validar(array);
      }
    }

    public bool validarDecrescente(int[] array)
    {
      using (var validador = new ValidadorOrdenacaoDescendente<int>())
      {
        return (array.Length == fTotal) && validador.validar(array);
      }
    }

    public bool validarValores(int[] array)
    {
      bool result = array.Length == fTotal;

      for (int i = 0; result && (i < fTotal); i++)
        result = array[i] == fValores[i];

      return result;
    }

    public bool validarInverso(int[] array)
    {
      bool result = array.Length == fTotal;

      for (int i = 0; result && (i < fTotal); i++)
        result = array[i] == fValores[fTotal - i - 1];

      return result;
    }

    public bool validarConteudo(int[] array)
    {
      Array.Sort(array);

      bool result = array.Length == fTotal;

      for (int i = 0; result && (i < fTotal); i++)
        result = array[i] == i + 1;

      return result;
    }
    #endregion

    #region Enumeráveis
    public bool validarCrescente(IEnumeravel<int> colecao)
    {
      return validarCrescente(colecao.toArray());
    }

    public bool validarDecrescente(IEnumeravel<int> colecao)
    {
      return validarDecrescente(colecao.toArray());
    }

    public bool validarValores(IEnumeravel<int> colecao)
    {
      return validarValores(colecao.toArray());
    }

    public bool validarInverso(IEnumeravel<int> colecao)
    {
      return validarInverso(colecao.toArray());
    }

    public bool validarConteudo(IEnumeravel<int> colecao)
    {
      return validarConteudo(colecao.toArray());
    }
    #endregion

    #region Coleções
    public IEnumerable<int> valores()
    {
      for (int i = 0; i < fTotal; i++)
        yield return fValores[i];
    }

    public IEnumerable<int> inverso()
    {
      for (int i = fTotal - 1; i >= 0; i--)
        yield return fValores[i];
    }

    public IEnumerable<int> aleatorio()
    {
      int[] indices = new int[fTotal];

      for (int i = 0; i < fTotal; i++)
        indices[i] = i;

      using (var aleatorio = new OrdenacaoAleatoria<int>())
      {
        aleatorio.ordenar(indices);
      }

      for (int i = 0; i < fTotal; i++)
        yield return fValores[indices[i]];
    }

    public int total
    {
      get { return fTotal; }
    }
    #endregion
  }
}