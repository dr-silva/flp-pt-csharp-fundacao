﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  #region Abstrações
  public abstract class TestadorFila : TestadorEnumeravel
  {
    protected IEnfileiravel<int> fila
    {
      get { return (IEnfileiravel<int>)colecao; }
    }

    protected abstract bool validarEnfileiramento(IEnumeravel<int> colecao);
    protected abstract bool validarDesenfileiramento(int[] array);

    [TestMethod]
    public void testarEnfileiramento()
    {
      fila.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        fila.enfileirar(valor);

      Assert.AreEqual(true, validarEnfileiramento(colecao));
    }

    [TestMethod]
    public void testarDesempilhamento()
    {
      fila.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        fila.enfileirar(valor);

      int[] itens  = new int[colecao.total];
      int   indice = 0;

      while (!colecao.vazio)
        itens[indice++] = fila.desenfileirar();

      if (!validarDesenfileiramento(itens))
        Assert.Fail();

      Assert.AreEqual(true, colecao.vazio);
    }
  }

  public abstract class TestadorFilaPrioridadeMinima : TestadorFila
  {
    protected override bool validarEnfileiramento(IEnumeravel<int> colecao)
    {
      return vetor.validarConteudo(colecao);
    }

    protected override bool validarDesenfileiramento(int[] array)
    {
      return vetor.validarCrescente(array);
    }
  }

  public abstract class TestadorFilaPrioridadeMaxima : TestadorFila
  {
    protected override bool validarEnfileiramento(IEnumeravel<int> colecao)
    {
      return vetor.validarConteudo(colecao);
    }

    protected override bool validarDesenfileiramento(int[] array)
    {
      return vetor.validarDecrescente(array);
    }
  }
  #endregion

  #region Filas Normais
  [TestClass]
  public class TestadorFilaVetorial : TestadorFila
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new FilaVetorial<int>(vetor.total + 1);
    }

    protected override bool validarEnfileiramento(IEnumeravel<int> colecao)
    {
      return vetor.validarValores(colecao);
    }

    protected override bool validarDesenfileiramento(int[] array)
    {
      return vetor.validarValores(array);
    }
  }

  [TestClass]
  public class TestadorFilaEncadeada : TestadorFila
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new FilaEncadeada<int>();
    }

    protected override bool validarEnfileiramento(IEnumeravel<int> colecao)
    {
      return vetor.validarValores(colecao);
    }

    protected override bool validarDesenfileiramento(int[] array)
    {
      return vetor.validarValores(array);
    }
  }
  #endregion

  #region Filas Prioridades Mínimas
  [TestClass]
  public class TestadorFilaPrioridadeMinimaVetorial : TestadorFilaPrioridadeMinima
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new FilaPrioridadeMinimaVetorial<int>(vetor.total + 1);
    }
  }

  [TestClass]
  public class TestadorFilaPrioridadeMinimaEncadeada : TestadorFilaPrioridadeMinima
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new FilaPrioridadeMinimaEncadeada<int>();
    }
  }
  #endregion

  #region Filas Prioridades Máximas
  [TestClass]
  public class TestadorFilaPrioridadeMaximaVetorial : TestadorFilaPrioridadeMaxima
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new FilaPrioridadeMaximaVetorial<int>(vetor.total + 1);
    }
  }

  [TestClass]
  public class TestadorFilaPrioridadeMaximaEncadeada : TestadorFilaPrioridadeMaxima
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new FilaPrioridadeMaximaEncadeada<int>();
    }
  }
  #endregion
}
