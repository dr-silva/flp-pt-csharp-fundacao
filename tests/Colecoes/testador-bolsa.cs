﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  #region Abstrações
  public abstract class TestadorBolsa : TestadorEnumeravel
  {
    protected IGuardavel<int> bolsa
    {
      get { return (IGuardavel<int>)colecao; }
    }

    protected abstract bool validar();

    [TestMethod]
    public void testarAdicao()
    {
      if (colecao.total != 0)
        Assert.Fail();

      foreach (var valor in vetor.valores())
        bolsa.guardar(valor);

      Assert.AreEqual(true, validar());
    }
  }

  public abstract class TestadorBolsaNormal : TestadorBolsa
  {
    protected override bool validar()
    {
      return vetor.validarConteudo(colecao);
    }
  }

  public abstract class TestadorBolsaOrdenada : TestadorBolsa
  {
    protected override bool validar()
    {
      return vetor.validarCrescente(colecao);
    }
  }
  #endregion

  #region Bolsas Normais
  [TestClass]
  public class TestadorBolsaVetorial : TestadorBolsaNormal
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new BolsaVetorial<int>(vetor.total);
    }
  }

  [TestClass]
  public class TestadorBolsaEncadeada : TestadorBolsaNormal
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new BolsaEncadeada<int>();
    }
  }
  #endregion

  #region Bolsas Ordenadas
  [TestClass]
  public class TestadorBolsaOrdenadaVetorial : TestadorBolsaOrdenada
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new BolsaOrdenadaVetorial<int>(vetor.total);
    }
  }

  [TestClass]
  public class TestadorBolsaOrdenadaEncadeada : TestadorBolsaOrdenada
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new BolsaOrdenadaEncadeada<int>();
    }
  }
  #endregion
}
