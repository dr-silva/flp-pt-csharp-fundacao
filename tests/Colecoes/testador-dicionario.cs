﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  #region Abstrações
  public abstract class TestadorDicionario : TestadorEnumeravel
  {
    protected IDicionarizavel<int, double> dicionario;

    protected abstract bool validarInsercao();

    [TestMethod]
    public void testarInsercao()
    {
      dicionario.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        dicionario.adicionar(valor, valor / 100.0);

      Assert.AreEqual(true, validarInsercao());
    }

    [TestMethod]
    public void testarExclusao()
    {
      dicionario.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        dicionario.adicionar(valor, valor / 100.0);

      int[] itens  = new int[vetor.total];
      int   indice = 0;

      foreach (int valor in vetor.aleatorio())
        if (dicionario.remover(valor))
          itens[indice++] = valor;

      if (!colecao.vazio)
        Assert.Fail();

      Assert.AreEqual(true, vetor.validarConteudo(itens));
    }
  }

  public abstract class TestadorDicionarioHash : TestadorDicionario
  {
    protected override bool validarInsercao()
    {
      return vetor.validarConteudo(colecao);
    }
  }

  public abstract class TestadorDicionarioOrdenado : TestadorDicionario
  {
    protected override bool validarInsercao()
    {
      return vetor.validarCrescente(colecao);
    }
  }
  #endregion

  #region Dicionários Hash
  [TestClass]
  public class TestadorDicionarioHashEncadeado : TestadorDicionarioHash
  {
    protected override IEnumeravel<int> criarColecao()
    {
      dicionario = new DicionarioHashEncadeado<int, double>(vetor.total);

      return dicionario.chaves;
    }
  }

  [TestClass]
  public class TestadorDicionarioHashSondado : TestadorDicionarioHash
  {
    protected override IEnumeravel<int> criarColecao()
    {
      dicionario = new DicionarioHashSondado<int, double>(vetor.total);

      return dicionario.chaves;
    }
  }
  #endregion

  #region Dicionários Ordenados
  [TestClass]
  public class TestadorDicionarioOrdenadoVetorial : TestadorDicionarioOrdenado
  {
    protected override IEnumeravel<int> criarColecao()
    {
      dicionario = new DicionarioOrdenadoVetorial<int, double>(vetor.total);

      return dicionario.chaves;
    }
  }

  [TestClass]
  public class TestadorDicionarioOrdenadoEncadeado : TestadorDicionarioOrdenado
  {
    protected override IEnumeravel<int> criarColecao()
    {
      dicionario = new DicionarioOrdenadoEncadeado<int, double>();

      return dicionario.chaves;
    }
  }
  #endregion
}
