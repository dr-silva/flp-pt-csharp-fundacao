﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  #region Abstrações
  public abstract class TestadorConjunto : TestadorEnumeravel
  {
    protected IDistinguivel<int> conjunto
    {
      get { return (IDistinguivel<int>)colecao; }
    }

    protected abstract bool validarInsercao();

    protected bool validarConjunto(int[] validacao)
    {
      int[] valores = conjunto.toArray();
      bool  result  = valores.Length == validacao?.Length;

      if (result && validacao != null)
      {
        Array.Sort(valores);

        for (int i = 0; result && i < validacao.Length; i++)
          result = valores[i] == valores[i];
      }

      return result;
    }

    [TestMethod]
    public void testarInsercao()
    {
      conjunto.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        conjunto.adicionar(valor);

      Assert.AreEqual(true, validarInsercao());
    }

    [TestMethod]
    public void testarExclusao()
    {
      conjunto.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        conjunto.adicionar(valor);

      int[] itens  = new int[vetor.total];
      int   indice = 0;

      foreach (int valor in vetor.aleatorio())
        if (conjunto.remover(valor))
          itens[indice++] = valor;

      if (!colecao.vazio)
        Assert.Fail();

      Assert.AreEqual(true, vetor.validarConteudo(itens));
    }

    [TestMethod]
    public void testarUniao_1()
    {
      IEmpilhavel<int> pilha = new PilhaVetorial<int>(10);

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
      {
        conjunto.adicionar(i);
        pilha.empilhar(i * 2);
      }

      conjunto.unir(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18})
      );
    }

    [TestMethod]
    public void testarUniao_2()
    {
      IEmpilhavel<int> pilha = new PilhaVetorial<int>(10);

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
        conjunto.adicionar(i);

      conjunto.unir(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
      );
    }

    [TestMethod]
    public void testarUniao_3()
    {
      IEmpilhavel<int> pilha = null;

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
        conjunto.adicionar(i);

      conjunto.unir(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
      );
    }

    [TestMethod]
    public void testarExcecao_1()
    {
      IEmpilhavel<int> pilha = new PilhaVetorial<int>(10);

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
      {
        conjunto.adicionar(i);
        pilha.empilhar(i * 2);
      }

      conjunto.excetuar(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {1, 3, 5, 7, 9})
      );
    }

    [TestMethod]
    public void testarExcecao_2()
    {
      IEmpilhavel<int> pilha = new PilhaVetorial<int>(10);

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
        conjunto.adicionar(i);

      conjunto.excetuar(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
      );
    }

    [TestMethod]
    public void testarExcecao_3()
    {
      IEmpilhavel<int> pilha = null;

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
        conjunto.adicionar(i);

      conjunto.excetuar(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
      );
    }

    [TestMethod]
    public void testarInterseccao_1()
    {
      IEmpilhavel<int> pilha = new PilhaVetorial<int>(10);

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
      {
        conjunto.adicionar(i);
        pilha.empilhar(i * 2);
      }

      conjunto.interseccionar(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {0, 2, 4, 6, 8})
      );
    }

    [TestMethod]
    public void testarInterseccao_2()
    {
      IEmpilhavel<int> pilha = new PilhaVetorial<int>(10);

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
        conjunto.adicionar(i);

      conjunto.interseccionar(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {})
      );
    }

    [TestMethod]
    public void testarInterseccao_3()
    {
      IEmpilhavel<int> pilha = null;

      conjunto.limpar();

      for (int i = 0; i < 10; i++)
        conjunto.adicionar(i);

      conjunto.interseccionar(pilha);

      Assert.AreEqual
      (
        true, validarConjunto(new int[] {})
      );
    }
  }

  public abstract class TestadorConjuntoHash : TestadorConjunto
  {
    protected override bool validarInsercao()
    {
      return vetor.validarConteudo(colecao);
    }
  }

  public abstract class TestadorConjuntoOrdenado : TestadorConjunto
  {
    protected override bool validarInsercao()
    {
      return vetor.validarCrescente(colecao);
    }
  }
  #endregion

  #region Dicionários Hash
  [TestClass]
  public class TestadorConjuntoHashEncadeado : TestadorConjuntoHash
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new ConjuntoHashEncadeado<int>(vetor.total);
    }
  }

  [TestClass]
  public class TestadorConjuntoHashSondado : TestadorConjuntoHash
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new ConjuntoHashSondado<int>(vetor.total);
    }
  }
  #endregion

  #region Dicionários Ordenados
  [TestClass]
  public class TestadorConjuntoOrdenadoVetorial : TestadorConjuntoOrdenado
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new ConjuntoOrdenadoVetorial<int>(vetor.total);
    }
  }

  [TestClass]
  public class TestadorConjuntoOrdenadoEncadeado : TestadorConjuntoOrdenado
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new ConjuntoOrdenadoEncadeado<int>();
    }
  }
  #endregion
}
