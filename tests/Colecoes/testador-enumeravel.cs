﻿using System;
using System.Collections.Generic;

using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Ordenacoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  public abstract class TestadorEnumeravel
  {
    private const int TOTAL = 100;

    protected readonly VetorValidacao   vetor;
    protected readonly IEnumeravel<int> colecao;

    public TestadorEnumeravel()
    {
      vetor    = new VetorValidacao(TOTAL);
      colecao  = criarColecao();
    }

    protected abstract IEnumeravel<int> criarColecao();
  }
}
