﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  public abstract class TestadorLista : TestadorEnumeravel
  {
    protected IListavel<int> lista
    {
      get { return (IListavel<int>)colecao; }
    }

    [TestMethod]
    public void testarInsercao()
    {
      lista.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        lista.adicionar(valor);

      Assert.AreEqual(true, vetor.validarValores(colecao));
    }

    [TestMethod]
    public void testarRemocao()
    {
      lista.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        lista.adicionar(valor);

      int[] itens  = new int[colecao.total];
      int   indice = 0;

      for (int i = colecao.total; i > 0; i--)
      {
        itens[indice++] = lista[i - 1];

        lista.remover(i - 1);
      }

      if (!vetor.validarInverso(itens))
        Assert.Fail();

      Assert.AreEqual(true, colecao.vazio);
    }
  }

  [TestClass]
  public class TestadorListaVetorial : TestadorLista
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new ListaVetorial<int>(vetor.total);
    }
  }

  [TestClass]
  public class TestadorListaEncadeada : TestadorLista
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new ListaEncadeada<int>();
    }
  }
}
