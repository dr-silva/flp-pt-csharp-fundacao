﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;

namespace FractalLotus.Fundacao.Testes.Colecoes
{
  public abstract class TestadorPilha : TestadorEnumeravel
  {
    protected IEmpilhavel<int> pilha
    {
      get { return (IEmpilhavel<int>)colecao; }
    }

    [TestMethod]
    public void testarEmpilhamento()
    {
      pilha.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        pilha.empilhar(valor);

      Assert.AreEqual(true, vetor.validarConteudo(colecao));
    }

    [TestMethod]
    public void testarDesempilhamento()
    {
      pilha.limpar();

      if (colecao.total != 0)
        Assert.Fail();

      foreach (int valor in vetor.valores())
        pilha.empilhar(valor);

      int[] itens  = new int[colecao.total];
      int   indice = 0;

      while (!colecao.vazio)
        itens[indice++] = pilha.desempilhar();

      if (!vetor.validarInverso(itens))
        Assert.Fail();

      Assert.AreEqual(colecao.vazio, true);
    }
  }

  [TestClass]
  public class TestadorPilhaVetorial : TestadorPilha
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new PilhaVetorial<int>(vetor.total);
    }
  }

  [TestClass]
  public class TestadorPilhaEncadeada : TestadorPilha
  {
    protected override IEnumeravel<int> criarColecao()
    {
      return new PilhaEncadeada<int>();
    }
  }
}